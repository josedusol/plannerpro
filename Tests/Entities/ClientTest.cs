﻿using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Entities.Exceptions;
using System;
using Xunit;

namespace PlannerPro.Entities.API.Tests
{
    public class ClientTest
    {
        [Theory]
        [InlineData("a")]
        [InlineData("ab")]
        [InlineData("abc")]
        [InlineData("ClientA_test")]
        public void ClientWithNonEmptyNameShouldBeValid(string name)
        {
            Client client = new RealClient(name, 1);

            Assert.Equal(name, client.Name);
        }

        [Theory]
        [InlineData("ClientA_test", 1, "ClientA_test", 1)]
        [InlineData("ClientB_test", 1, "ClientB_test", 1)]
        public void ClientEqualsTrueTest(String firstClientName, int firstClientPriority, 
            String secondClientName, int secondClientPriority)
        {
            Client firstClient = new RealClient(firstClientName, firstClientPriority);
            Client secondClient = new RealClient(secondClientName, secondClientPriority);
            Assert.True(firstClient.Equals(secondClient));
        }

        [Theory]
        [InlineData("ClientA_test", 1, "ClientB_test", 1)]
        [InlineData("ClientB_test", 1, "ClientX", 5)]
        public void ClientNotEqualsTest(String firstClientName, int firstClientPriority, 
            String secondClientName, int secondClientPriority)
        {
            Client firstClient = new RealClient(firstClientName, firstClientPriority);
            Client secondClient = new RealClient(secondClientName, secondClientPriority);
            Assert.False(firstClient.Equals(secondClient));
        }

        [Theory]
        [InlineData("a")]
        [InlineData("ClientA_test")]
        public void ClientToStringTest(string name)
        {
            Client client = new RealClient(name, 1);
            String clientName = client.ToString();

            Assert.Equal(name, client.Name);
        }

        [Fact]
        public void ClientWithEmptyNameShouldThrowException() 
        {
            Assert.Throws<ClientException>(
            delegate {
                Client client = new RealClient("", 1);
            });
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        [InlineData(5)]
        [InlineData(6)]
        [InlineData(7)]
        [InlineData(8)]
        [InlineData(9)]
        [InlineData(10)]
        public void ClientWithPreferenceBetweenOneAndTenShouldBeValid(int preference) 
        {
            Client client = new RealClient("ClientA_test", preference);

            Assert.Equal(preference, client.Preference);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(11)]
        public void ClientWithPreferenceLessThanOneOrGreaterThanTenShouldThrowException(int preference) 
        {
            Assert.Throws<ClientException>(
            delegate {
                Client client = new RealClient("ClientA_test", preference);
            });
        }
    }
}
