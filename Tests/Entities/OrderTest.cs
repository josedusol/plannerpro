﻿using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Entities.Exceptions;
using System;
using Xunit;

namespace PlannerPro.Entities.API.Tests
{
    public class OrderTest
    {
        [Fact]
        public void OrderWithNonNullClientShouldBeValid()
        {
            Client client = new RealClient("ClientA_test", 1);
            Product product = new RealProduct("ProductA_test", 1, 1, 1);

            Order order = new RealOrder(client, product, 1, new DateTime(2050, 1, 1));

            Assert.Equal("ClientA_test", order.Client.Name);
        }

        [Fact]
        public void OrderWithNullClientShouldThrowException()
        {
            Product product = new RealProduct("ProductA_test", 1, 1, 1);

            Assert.Throws<OrderException>(
            delegate {
                Order order = new RealOrder(Client.NULL, product, 1, new DateTime(2050, 1, 1));
            });
        }

        [Fact]
        public void OrderWithNonNullProductShouldBeValid()
        {
            Client client = new RealClient("ClientA_test", 1);
            Product product = new RealProduct("ProductA_test", 1, 1, 1);

            Order order = new RealOrder(client, product, 1, new DateTime(2050, 1, 1));

            Assert.Equal("ProductA_test", order.Product.Name);
        }

        [Fact]
        public void OrderWithNullProductShouldThrowException()
        {
            Client client = new RealClient("ClientA_test", 1);

            Assert.Throws<OrderException>(
            delegate {
                Order order = new RealOrder(client, Product.NULL, 1, new DateTime(2050, 1, 1));
            });
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(1000)]
        public void OrderWithGreaterThanZeroQuantityShouldBeValid(int quantity)
        {
            Client client = new RealClient("ClientA_test", 1);
            Product product = new RealProduct("ProductA_test", 1, 1, 1);

            Order order = new RealOrder(client, product, quantity, new DateTime(2050, 1, 1));

            Assert.Equal(quantity, order.Quantity);
        }

        [Fact]
        public void OrderWithLessThanOneQuantityShouldThrowException()
        {
            Client client = new RealClient("ClientA_test", 1);
            Product product = new RealProduct("ProductA_test", 1, 1, 1);

            Assert.Throws<OrderException>(
            delegate {
                Order order = new RealOrder(client, product, 0, new DateTime(2050, 1, 1));
            });
        }

        [Fact]
        public void OrderWithFutureDeliveryDateShouldBeValid()
        {
            Client client = new RealClient("ClientA_test", 1);
            Product product = new RealProduct("ProductA_test", 1, 1, 1);
            DateTime futureDeliveryDate = DateTime.Now.AddDays(5);

            Order order = new RealOrder(client, product, 1, futureDeliveryDate);

            Assert.Equal<DateTime>(futureDeliveryDate, order.DeliveryDate);
        }

        [Fact]
        public void OrderWithActualDeliveryDateShouldThrowException()
        {
            Client client = new RealClient("ClientA_test", 1);
            Product product = new RealProduct("ProductA_test", 1, 1, 1);
            DateTime actualDate = DateTime.Now;

            Assert.Throws<OrderException>(
            delegate {
                Order order = new RealOrder(client, product, 1, actualDate);
            });
        }

        [Fact]
        public void OrderWithPastDeliveryDateShouldThrowException()
        {
            Client client = new RealClient("ClientA_test", 1);
            Product product = new RealProduct("ProductA_test", 1, 1, 1);
            DateTime pastDeliveryDate = new DateTime(2000, 1, 1);

            Assert.Throws<OrderException>(
            delegate {
                Order order = new RealOrder(client, product, 1, pastDeliveryDate);
            });
        }
    }
}
