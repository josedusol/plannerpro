﻿using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Entities.Exceptions;
using Xunit;

namespace PlannerPro.Entities.API.Tests
{
    public class ProductTest
    {
        [Theory]
        [InlineData("a")]
        [InlineData("ab")]
        [InlineData("abc")]
        [InlineData("ProductA_test")]
        public void ProductWithNonEmptyNameShouldBeValid(string name)
        {
            Product product = new RealProduct(name, 1, 1, 1);

            Assert.Equal(name, product.Name);
        }

        [Fact]
        public void ProductWithEmptyNameShouldThrowException()
        {
            Assert.Throws<ProductException>(
            delegate {
                Product product = new RealProduct("", 1, 1, 1);
            });
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(1000)]
        public void ProductWithGreaterThanZeroManufacturingTimeShouldBeValid(int manufacturingTime)
        {
            Product product = new RealProduct("ProductA_test", manufacturingTime, 1, 1);

            Assert.Equal(manufacturingTime, product.ManufacturingTime);
        }

        [Fact]
        public void ProductWithLessThanOneManufacturingTimeShouldThrowException()
        {
            Assert.Throws<ProductException>(
            delegate {
                Product product = new RealProduct("ProductA_test", 0, 1, 1);
            });
        }

        [Theory]
        [InlineData(0)]
        [InlineData(0.99999)]
        [InlineData(10.4)]
        [InlineData(2000000.5)]
        public void ProductWithGreaterOrEqualToZeroProductionCostShouldBeValid(float productionCost)
        {
            Product product = new RealProduct("ProductA_test", 1, 1, productionCost);

            Assert.Equal(productionCost, product.ProductionCost);
        }

        [Fact]
        public void ProductWithLessThanZeroProductionCostShouldThrowException()
        {
            Assert.Throws<ProductException>(
            delegate {
                Product product = new RealProduct("ProductA_test", 1, 1, -1);
            });
        }

        [Theory]
        [InlineData(0)]
        [InlineData(0.99999)]
        [InlineData(10.4)]
        [InlineData(2000000.5)]
        public void ProductWithGreaterOrEqualToZeroSalePriceShouldBeValid(float salePrice)
        {
            Product product = new RealProduct("ProductA_test", 1, salePrice, 1);

            Assert.Equal(salePrice, product.SalePrice);
        }

        [Fact]
        public void ProductWithLessThanZeroSalePriceShouldThrowException()
        {
            Assert.Throws<ProductException>(
            delegate {
                Product product = new RealProduct("ProductA_test", 1, -1, 1);
            });
        }

        [Fact]
        public void TwoProductsWithSameNameShouldBeEqual()
        {
            Product productOne = new RealProduct("ProductA_test", 1, 1, 1);
            Product productTwo = new RealProduct("ProductA_test", 1, 1, 1);

            Assert.Equal<Product>(productOne, productTwo);
        }

        [Fact]
        public void TwoProductsWithDistinctNameShouldBeNotEqual()
        {
            Product productOne = new RealProduct("ProductA_test", 1, 1, 1);
            Product productTwo = new RealProduct("ProductB_test", 1, 1, 1);

            Assert.NotEqual<Product>(productOne, productTwo);
        }
    }
}
