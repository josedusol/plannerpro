﻿using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Entities.Exceptions;
using Xunit;

namespace PlannerPro.Entities.API.Tests
{
    public class PlanTest
    {
        [Theory]
        [InlineData("a")]
        [InlineData("ab")]
        [InlineData("abc")]
        [InlineData("ClientA_test")]
        public void PlanWithNonEmptyNameShouldBeValid(string name)
        {
            Plan plan = new RealPlan(name);

            Assert.Equal(name, plan.Name);
        }

        [Fact]
        public void PlanWithEmptyNameShouldThrowException()
        {
            Assert.Throws<PlanException>(
            delegate {
                Plan plan = new RealPlan("");
            });
        }

        [Theory]
        [InlineData("")]
        [InlineData("a")]
        [InlineData("ab")]
        [InlineData("abc")]
        [InlineData("bla bla")]
        public void PlanWithNonNullDescriptionShouldBeValid(string description)
        {
            Plan plan = new RealPlan("PlanA_test", description, new DeliveryDateCriteria());

            Assert.Equal(description, plan.Description);
        }

        public void PlanWithNullDescriptionShouldThrowException()
        {
            Assert.Throws<PlanException>(
            delegate {
                Plan plan = new RealPlan("PlanA_test", null, new DeliveryDateCriteria());
            });
        }

        [Fact]
        public void NewPlanHasNoPlannedOrders()
        {
            Plan plan = new RealPlan("PlanA_test");

            Assert.True(plan.PendentOrders.Count == 0);
        }
    }
}
