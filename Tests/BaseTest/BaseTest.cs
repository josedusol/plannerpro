﻿using PlannerPro.Data.DataImp;
using System;
using System.Data.Entity;
using System.Reflection;
using Xunit;

namespace PlannerPro.Tests.BaseTest
{
    public abstract class BaseTest : IDisposable
    {
        protected UnitOfWork unitOfWork;
        protected DbContext context;

        protected BaseTest()
        {
            CreateUnitOfWorkInstance();
        }

        private void CreateUnitOfWorkInstance()
        {
            ConstructorInfo ci = typeof(UnitOfWork).GetConstructor(BindingFlags.Instance 
                | BindingFlags.NonPublic, null, Type.EmptyTypes, null);
            Assert.NotNull(ci);
            this.unitOfWork = (UnitOfWork)ci.Invoke(null);

            Type cl = typeof(UnitOfWork);
            FieldInfo field = cl.GetField("context", BindingFlags.NonPublic | BindingFlags.Instance);
            context = (DbContext)field.GetValue(unitOfWork);
        }

        public UnitOfWork UnitOfWorkInstance {
            get { return unitOfWork; } 
        }

        public void Dispose()
        {
            context.Database.
            ExecuteSqlCommand("DELETE o FROM Orders o, Clients c "
                            + "WHERE o.ClientID = c.ClientID AND c.Name LIKE '%_test';");
            context.Database.ExecuteSqlCommand("DELETE FROM Clients WHERE Name LIKE '%_test';");
            context.Database.ExecuteSqlCommand("DELETE FROM Products WHERE Name LIKE '%_test';");
            context.Database.ExecuteSqlCommand("DELETE pc FROM Plans p, PlanningCriterias pc "
                            + "WHERE p.PlanningCriteriaID = pc.PlanningCriteriaID AND p.Name LIKE '%_test';");
            context.Database.ExecuteSqlCommand("DELETE FROM Plans WHERE Name LIKE '%_test';");
            unitOfWork.Dispose();
        }
    }
}
