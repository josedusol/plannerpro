﻿using PlannerPro.Logic.LogicImp;
using System;
using System.Reflection;
using Xunit;

namespace PlannerPro.Logic.BaseTest
{
    public abstract class LogicBaseTest : PlannerPro.Tests.BaseTest.BaseTest
    {
        protected ClientLogic clientLogic;
        protected ProductLogic productLogic;
        protected OrderLogic orderLogic;
        protected PlanLogic planLogic;

        protected LogicBaseTest()
        {
            CreateClientLogicInstance();
            CreateProductLogicInstance();
            CreateOrderLogicInstance();
            CreatePlanLogicInstance();       
        }

        private void CreateClientLogicInstance()
        {
            ConstructorInfo ci = typeof(ClientLogic).GetConstructor(BindingFlags.Instance
                | BindingFlags.NonPublic, null, Type.EmptyTypes, null);
            Assert.NotNull(ci);
            this.clientLogic = (ClientLogic)ci.Invoke(null);

            Type cl = typeof(ClientLogic);
            FieldInfo field = cl.GetField("unitOfWork", BindingFlags.NonPublic | BindingFlags.Instance);
            field.SetValue(clientLogic, base.unitOfWork);
        }

        private void CreateProductLogicInstance()
        {
            ConstructorInfo ci = typeof(ProductLogic).GetConstructor(BindingFlags.Instance
                | BindingFlags.NonPublic, null, Type.EmptyTypes, null);
            Assert.NotNull(ci);
            this.productLogic = (ProductLogic)ci.Invoke(null);

            Type cl = typeof(ProductLogic);
            FieldInfo field = cl.GetField("unitOfWork", BindingFlags.NonPublic | BindingFlags.Instance);
            field.SetValue(productLogic, base.unitOfWork);
        }

        private void CreateOrderLogicInstance()
        {
            ConstructorInfo ci = typeof(OrderLogic).GetConstructor(BindingFlags.Instance
                | BindingFlags.NonPublic, null, Type.EmptyTypes, null);
            Assert.NotNull(ci);
            this.orderLogic = (OrderLogic)ci.Invoke(null);

            Type cl = typeof(OrderLogic);
            FieldInfo field = cl.GetField("unitOfWork", BindingFlags.NonPublic | BindingFlags.Instance);
            field.SetValue(orderLogic, base.unitOfWork);
        }

        private void CreatePlanLogicInstance()
        {
            ConstructorInfo ci = typeof(PlanLogic).GetConstructor(BindingFlags.Instance
                | BindingFlags.NonPublic, null, Type.EmptyTypes, null);
            Assert.NotNull(ci);
            this.planLogic = (PlanLogic)ci.Invoke(null);

            Type cl = typeof(PlanLogic);
            FieldInfo field = cl.GetField("unitOfWork", BindingFlags.NonPublic | BindingFlags.Instance);
            field.SetValue(planLogic, base.unitOfWork);
        }

        public ClientLogic ClientLogicInstance
        {
            get { return clientLogic; }
        }

        public ProductLogic ProductLogicInstance
        {
            get { return productLogic; }
        }

        public OrderLogic OrderLogicInstance
        {
            get { return orderLogic; }
        }

        public PlanLogic PlanLogicInstance 
        { 
            get { return planLogic; } 
        }
    }
}
