﻿using PlannerPro.Data.DataImp;
using PlannerPro.Entities.API;
using PlannerPro.Entities.EntitiesImp;
using System;
using System.Linq;
using Xunit;
using Xunit.Abstractions;

namespace PlannerPro.Tests.EntityFramework
{
    [Collection("Serial Tests")]
    public class LearningTest : PlannerPro.Tests.BaseTest.BaseTest
    {
        private readonly ITestOutputHelper output;

        public LearningTest(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void PersistClientTest()
        {
            using (var db = new PlannerProContext()) {
                Client clientOne = new RealClient("ClientA_test", 1);
                Client clientTwo = new RealClient("ClientB_test", 2);
                db.Clients.Add(clientOne);
                db.Clients.Add(clientTwo);
                db.SaveChanges();

                var query = from c in db.Clients
                            select c;

                Assert.True(query.ToList().Contains<Client>(clientOne));
                Assert.True(query.ToList().Contains<Client>(clientTwo));
            }
        }

        [Fact]
        public void PersistProductTest()
        {
            using (var db = new PlannerProContext()) {
                Product productOne = new RealProduct("ProductA_test", 1, 1, 1);
                Product productTwo = new RealProduct("ProductB_test", 1, 1, 1);

                db.Products.Add(productOne);
                db.Products.Add(productTwo);
                db.SaveChanges();

                var query = from product in db.Products
                            select product;

                Assert.True(query.ToList().Contains<Product>(productOne));
                Assert.True(query.ToList().Contains<Product>(productTwo));

            }
        }

        [Fact]
        public void PersistOrderTest()
        {
            using (var db = new PlannerProContext()) {
                Product productOne = new RealProduct("ProductA_test", 1, 1, 1);
                Product productTwo = new RealProduct("ProductB_test", 1, 1, 1);
                db.Products.Add(productOne);
                db.Products.Add(productTwo);
                Client clientOne = new RealClient("ClientA_test", 1);
                Client clientTwo = new RealClient("ClientB_test", 2);
                db.Clients.Add(clientOne);
                db.Clients.Add(clientTwo);
                Order orderOne = new RealOrder(clientOne, productOne, 5, new DateTime(2050, 1, 1));
                Order orderTwo = new RealOrder(clientTwo, productTwo, 5, new DateTime(2050, 1, 1));
                db.Orders.Add(orderOne);
                db.Orders.Add(orderTwo);
                db.SaveChanges();

                var query = from o in db.Orders
                            select o;

                Assert.True(query.ToList().Contains<Order>(orderOne));
                Assert.True(query.ToList().Contains<Order>(orderTwo));
            }
        }

        [Fact]
        public void PersistPlanTest()
        {
            using (var db = new PlannerProContext()) {
                Product newProduct = new RealProduct("ProductA_test", 1, 1, 1);
                Product newProductTwo = new RealProduct("ProductB_test", 1, 1, 1);
                db.Products.Add(newProduct);
                db.Products.Add(newProductTwo);
                Client newClient = new RealClient("ClientA_test", 1);
                Client newClient2 = new RealClient("ClientB_test", 2);
                db.Clients.Add(newClient);
                db.Clients.Add(newClient2);
                Order newOrder = new RealOrder(newClient, newProduct, 5, new DateTime(2050, 1, 1));
                Order newOrderTwo = new RealOrder(newClient2, newProductTwo, 5, new DateTime(2050, 1, 1));
                db.Orders.Add(newOrder);
                db.Orders.Add(newOrderTwo);
                Plan plan = new RealPlan("PlanA_test", "Test plan A", new DeliveryDateCriteria());

                plan.PendentOrders.Add(newOrder);
                plan.PendentOrders.Add(newOrderTwo);
                db.Plans.Add(plan);
                db.SaveChanges();

                var query = from p in db.Plans
                            select p;

                Assert.True(query.ToList().Contains<Plan>(plan));
            }
        }
    }
}
