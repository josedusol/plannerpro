﻿using PlannerPro.Data.DataListImp;
using PlannerPro.Tests.EntityFramework;
using System;
using System.Reflection;
using Xunit;

namespace PlannerPro.Data.Tests
{
    public abstract class BaseLearningTest : IDisposable
    {     
        protected BaseLearningTest()
        {       
        }   

        public void Dispose()
        {
            using (var db = new TestContext()) {
                //db.Database.ExecuteSqlCommand("DELETE FROM Orders");
                //db.Database.ExecuteSqlCommand("DELETE FROM Plans");
                //db.Database.ExecuteSqlCommand("DELETE FROM Clients");
                //db.Database.ExecuteSqlCommand("DELETE FROM Products");
            }
        }
    }
}
