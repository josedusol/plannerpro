﻿using PlannerPro.Data.API;
using PlannerPro.Entities.API;
using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Tests.BaseTest;
using System.Collections.Generic;
using Xunit;

namespace PlannerPro.Data.Tests
{
    [Collection("Serial Tests")]
    public class ClientRepositoryTest : BaseTest
    {
        [Fact]
        public void CreateNewClient()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Client client = new RealClient("ClientA_test", 1);

            uow.ClientRepository.CreateClient(client);
            uow.Save();
            uow.ClientRepository.AttachClient(client);

            Assert.Equal<Client>(client, uow.ClientRepository.GetClient(client));
        }

        [Fact]
        public void CreateTwoNewClients()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Client clientOne = new RealClient("ClientA_test", 1);
            Client clientTwo = new RealClient("ClientB_test", 1);

            uow.ClientRepository.CreateClient(clientOne);
            uow.ClientRepository.CreateClient(clientTwo);
            uow.Save();
            uow.ClientRepository.AttachClient(clientOne);
            uow.ClientRepository.AttachClient(clientTwo);

            Assert.Equal<Client>(clientOne, uow.ClientRepository.GetClient(clientOne));
            Assert.Equal<Client>(clientTwo, uow.ClientRepository.GetClient(clientTwo));
        }

        [Fact]
        public void GetDeletedClientShouldReturnNullClient()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Client client = new RealClient("ClientA_test", 1);
            uow.ClientRepository.CreateClient(client);
            uow.Save();

            uow.ClientRepository.DeleteClient(client);
            uow.Save();

            Assert.Equal<Client>(Client.NULL, uow.ClientRepository.GetClient(client));
        }

        [Fact]
        public void GetNonExistentClientShouldReturnNullClient()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;

            Client client = uow.ClientRepository.GetClient(new RealClient("ClientX"));

            Assert.Equal<Client>(Client.NULL, client);
        }

        [Fact]
        public void GetAllClientsShouldReturnAllClientsInClientRepository()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Client newClientOne = new RealClient("ClientA_test", 1);
            Client newClientTwo = new RealClient("ClientB_test", 1);
            Client newClientThree = new RealClient("ClientC_test", 1);
            List<Client> expected = new List<Client>() { newClientOne, newClientTwo, newClientThree };

            uow.ClientRepository.CreateClient(newClientOne);
            uow.ClientRepository.CreateClient(newClientTwo);
            uow.ClientRepository.CreateClient(newClientThree);
            uow.Save();

            Assert.Equal(expected, uow.ClientRepository.GetAllClients());
        }
    }
}
