﻿using PlannerPro.Data.API;
using PlannerPro.Entities.API;
using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Tests.BaseTest;
using System;
using System.Collections.Generic;
using Xunit;

namespace PlannerPro.Data.Tests
{
    [Collection("Serial Tests")]
    public class OrderRepositoryTest : BaseTest
    {
        [Fact]
        public void CreateNewOrder()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Order newOrder = new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1));

            uow.OrderRepository.CreateOrder(newOrder);
            uow.Save();
            uow.OrderRepository.AttachOrder(newOrder);

            Assert.Equal<Order>(newOrder, uow.OrderRepository.GetOrder(newOrder));
        }

        [Fact]
        public void CreateTwoNewOrders()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Order newOrderOne = new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1));
            Order newOrderTwo = new RealOrder(new RealClient("ClientB_test"),
                new RealProduct("ProductB_test"), 1, new DateTime(2050, 1, 2));

            uow.OrderRepository.CreateOrder(newOrderOne);
            uow.OrderRepository.CreateOrder(newOrderTwo);
            uow.Save();
            uow.OrderRepository.AttachOrder(newOrderOne);
            uow.OrderRepository.AttachOrder(newOrderTwo);

            Assert.Equal<Order>(newOrderOne, uow.OrderRepository.GetOrder(newOrderOne));
            Assert.Equal<Order>(newOrderTwo, uow.OrderRepository.GetOrder(newOrderTwo));
        }

        [Fact]
        public void GetDeletedOrderShouldReturnNullOrder()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Order order = new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1));
            uow.OrderRepository.CreateOrder(order);
            uow.Save();

            uow.OrderRepository.DeleteOrder(order);
            uow.Save();

            Assert.Equal<Order>(Order.NULL, uow.OrderRepository.GetOrder(order));
        }

        [Fact]
        public void GetNonExistentOrderShouldReturnNullOrder()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Order storedOrder = uow.OrderRepository.GetOrder(new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1)));

            Assert.Equal<Order>(Order.NULL, storedOrder);
        }

        [Fact]
        public void GetAllOrdersShouldReturnAllOrdersInOrderRepository()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Order orderOne = new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1));
            Order orderTwo = new RealOrder(new RealClient("ClientB_test"),
                new RealProduct("ProductB_test"), 1, new DateTime(2050, 1, 1));
            Order orderThree = new RealOrder(new RealClient("ClientC_test"),
                new RealProduct("ProductC_test"), 1, new DateTime(2050, 1, 1));
            IList<Order> expectedOrders = new List<Order>() { orderOne, orderTwo, orderThree };

            uow.OrderRepository.CreateOrder(orderOne);
            uow.OrderRepository.CreateOrder(orderTwo);
            uow.OrderRepository.CreateOrder(orderThree);
            uow.Save();

            Assert.Equal(expectedOrders, uow.OrderRepository.GetAllOrders());
        }
    }
}
