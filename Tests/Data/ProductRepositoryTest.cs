﻿using PlannerPro.Data.API;
using PlannerPro.Entities.API;
using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Tests.BaseTest;
using System.Collections.Generic;
using Xunit;

namespace PlannerPro.Data.Tests
{
    [Collection("Serial Tests")]
    public class ProductRepositoryTest : BaseTest
    {
        [Fact]
        public void CreateNewProduct()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Product product = new RealProduct("ProductA_test", 1, 1, 1);
            uow.ProductRepository.CreateProduct(product);
            uow.Save();
            uow.ProductRepository.AttachProduct(product);

            Assert.Equal<Product>(product, uow.ProductRepository.GetProduct(product));
        }

        [Fact]
        public void CreateTwoNewProducts()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Product newProductOne = new RealProduct("ProductA_test", 1, 1, 1);
            Product newProductTwo = new RealProduct("ProductB_test", 1, 1, 1);
            uow.ProductRepository.CreateProduct(newProductOne);
            uow.ProductRepository.CreateProduct(newProductTwo);
            uow.Save();

            uow.ProductRepository.AttachProduct(newProductOne);
            uow.ProductRepository.AttachProduct(newProductTwo);

            Assert.Equal<Product>(newProductOne, uow.ProductRepository.GetProduct(newProductOne));
            Assert.Equal<Product>(newProductTwo, uow.ProductRepository.GetProduct(newProductTwo));
        }

        [Fact]
        public void GetDeletedProductShouldReturnNullProduct()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Product product = new RealProduct("ProductA_test", 1, 1, 1);
            uow.ProductRepository.CreateProduct(product);
            uow.Save();

            uow.ProductRepository.DeleteProduct(product);
            uow.Save();

            Assert.Equal<Product>(Product.NULL, uow.ProductRepository.GetProduct(product));
        }

        [Fact]
        public void GetNonExistentProductShouldReturnNullProduct()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Product storedProduct = uow.ProductRepository.GetProduct(new RealProduct("ProductX"));

            Assert.Equal<Product>(Product.NULL, storedProduct);
        }

        [Fact]
        public void GetAllProductsShouldReturnAllProductsInProductRepository()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Product newProductOne = new RealProduct("ProductA_test", 1, 1, 1);
            Product newProductTwo = new RealProduct("ProductB_test", 1, 1, 1);
            Product newProductThree = new RealProduct("ProductC_test", 1, 1, 1);
            List<Product> expected = new List<Product>() { newProductOne, newProductTwo, newProductThree };

            uow.ProductRepository.CreateProduct(newProductOne);
            uow.ProductRepository.CreateProduct(newProductTwo);
            uow.ProductRepository.CreateProduct(newProductThree);
            uow.Save();

            Assert.Equal(expected, uow.ProductRepository.GetAllProducts());
        }
    }
}
