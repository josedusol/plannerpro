﻿using PlannerPro.Data.API;
using PlannerPro.Entities.API;
using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Tests.BaseTest;
using Xunit;

namespace PlannerPro.Data.Tests
{
    [Collection("Serial Tests")]
    public class PlanRepositoryTest : BaseTest
    {
        [Fact]
        public void CreateNewPlan()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Plan plan = new RealPlan("PlanA_test");

            uow.PlanRepository.CreatePlan(plan);
            uow.Save();
            uow.PlanRepository.AttachPlan(plan);

            Assert.Equal<Plan>(plan, uow.PlanRepository.GetPlan(plan));
        }

        [Fact]
        public void CreateTwoNewPlans()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Plan planOne = new RealPlan("PlanA_test");
            Plan planTwo = new RealPlan("PlanB_test");

            uow.PlanRepository.CreatePlan(planOne);
            uow.PlanRepository.CreatePlan(planTwo);
            uow.Save();
            uow.PlanRepository.AttachPlan(planOne);
            uow.PlanRepository.AttachPlan(planTwo);

            Assert.Equal<Plan>(planOne, uow.PlanRepository.GetPlan(planOne));
            Assert.Equal<Plan>(planTwo, uow.PlanRepository.GetPlan(planTwo));
        }

        [Fact]
        public void GetDeletedPlanShouldReturnNullPlan()
        {
            IUnitOfWork uow = base.UnitOfWorkInstance;
            Plan plan = new RealPlan("PlanA_test");
            uow.PlanRepository.CreatePlan(plan);
            uow.Save();

            uow.PlanRepository.DeletePlan(plan);
            uow.Save();

            Assert.Equal<Plan>(Plan.NULL, uow.PlanRepository.GetPlan(plan));
        }
    }
}
