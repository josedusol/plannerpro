﻿using PlannerPro.Entities.API;
using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Entities.Exceptions;
using PlannerPro.Logic.API;
using PlannerPro.Logic.BaseTest;
using System;
using System.Collections.Generic;
using Xunit;

namespace PlannerPro.Logic.Tests
{
    [Collection("Serial Tests")]
    public class OrderLogicTest : LogicBaseTest 
    {
        [Fact]
        public void RegisterNewOrder()
        {
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order order = new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1));

            orderLogic.RegisterOrder(order);

            Assert.True(orderLogic.OrderIsRegistered(order));
        }

        [Fact]
        public void RegisterTwoNewOrders()
        {
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order orderOne = new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1));
            Order orderTwo = new RealOrder(new RealClient("ClientB_test"),
                new RealProduct("ProductB_test"), 1, new DateTime(2050, 1, 2));

            orderLogic.RegisterOrder(orderOne);
            orderLogic.RegisterOrder(orderTwo);

            Assert.True(orderLogic.OrderIsRegistered(orderOne));
            Assert.True(orderLogic.OrderIsRegistered(orderTwo));
        }

        [Fact]
        public void UpdatePendentOrderShouldThrowException()
        {
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order order = new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1));
            order.Status = OrderStatus.Pending;
            orderLogic.RegisterOrder(order);

            Order updatedOrder = new RealOrder(new RealClient("ClientA_test"),
               new RealProduct("ProductB_test"), 1, new DateTime(2050, 1, 1));

            Assert.Throws<OrderException>(
            delegate {
                orderLogic.UpdateOrder(order, updatedOrder);
            });    
        }

        [Fact]
        public void UpdateCompletedOrderShouldThrowException()
        {
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order order = new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1));
            order.Status = OrderStatus.Finalized;
            orderLogic.RegisterOrder(order);

            Order updatedOrder = new RealOrder(new RealClient("ClientA_test"),
               new RealProduct("ProductB_test"), 1, new DateTime(2050, 1, 1));

            Assert.Throws<OrderException>(
            delegate {
                orderLogic.UpdateOrder(order, updatedOrder);
            });
        }

        public void UpdateOrderDataWhenTheSameOrderDataIsAlreadyRegisteredShouldThrowException()
        {
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order orderOne = new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1));
            Order orderTwo = new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductB_test"), 1, new DateTime(2050, 1, 1));
            orderLogic.RegisterOrder(orderOne);
            orderLogic.RegisterOrder(orderTwo);

            Order updatedOrder = new RealOrder(new RealClient("ClientA_test"),
               new RealProduct("ProductB_test"), 1, new DateTime(2050, 1, 1));

            Assert.Throws<OrderException>(
            delegate {
                orderLogic.UpdateOrder(orderOne, updatedOrder);
            });
        }

        [Fact]
        public void UpdateOrderDataShouldChangeOrderData()
        {
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order order = new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1));
            orderLogic.RegisterOrder(order);

            Order updatedOrder = new RealOrder(new RealClient("ClientA_test"),
              new RealProduct("ProductB_test"), 1, new DateTime(2050, 1, 1));
            orderLogic.UpdateOrder(order, updatedOrder);

            Assert.True(orderLogic.OrderIsRegistered(updatedOrder));
        }

        [Fact]
        public void UpdateOrderProductShouldChangeOrderProduct()
        {
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order order = new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1));
            orderLogic.RegisterOrder(order);
            
            Order updatedOrder = new RealOrder(new RealClient("ClientA_test"),
               new RealProduct("ProductB_test"), 1, new DateTime(2050, 1, 1));
            orderLogic.UpdateOrder(order, updatedOrder);

            Assert.True(orderLogic.OrderIsRegistered(updatedOrder));
        }

        [Fact]
        public void UnregisterNotFinalizedOrderShouldThrowException()
        {
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order order = new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1));
            order.Status = OrderStatus.Pending;
            orderLogic.RegisterOrder(order);

            Assert.Throws<OrderException>(
            delegate {
                orderLogic.UnregisterOrder(order);
            });
        }

        [Fact]
        public void UnregisterOrderThatIsNotRegisteredShouldThrowException()
        {
            IOrderLogic orderLogic = base.OrderLogicInstance;

            Assert.Throws<OrderException>(
            delegate {
                orderLogic.UnregisterOrder(new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1)));
            });
        }

        [Fact]
        public void GetAllRegisteredOrdersReturnAllOrdersInOrderRepository()
        {
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order orderOne = new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1));
            Order orderTwo = new RealOrder(new RealClient("ClientB_test"),
                new RealProduct("ProductB_test"), 5, new DateTime(2020, 4, 7));
            Order orderThree = new RealOrder(new RealClient("ClientC_test"),
                new RealProduct("ProductC_test"), 3, new DateTime(2055, 5, 1));
            List<Order> expected = new List<Order>() { orderOne, orderTwo, orderThree };

            orderLogic.RegisterOrder(orderOne);
            orderLogic.RegisterOrder(orderTwo);
            orderLogic.RegisterOrder(orderThree);

            Assert.Equal(expected, orderLogic.GetAllRegisteredOrders());
        }
    }
}
