﻿using PlannerPro.Entities.API;
using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Entities.Exceptions;
using PlannerPro.Logic.API;
using PlannerPro.Logic.BaseTest;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace PlannerPro.Logic.Tests
{
    [Collection("Serial Tests")]
    public class ClientLogicTest : LogicBaseTest
    {
        [Fact]
        public void RegisterNewClient()
        {
            IClientLogic clientLogic = base.ClientLogicInstance;
            Client newClient = new RealClient("ClientA_test", 1);

            clientLogic.RegisterClient(newClient);

            Assert.True(clientLogic.GetAllRegisteredClients().ToList().Any(c => c.Equals(newClient)));
        }

        [Fact]
        public void RegisterTwoNewClients()
        {
            IClientLogic clientLogic = base.ClientLogicInstance;
            Client newClientOne = new RealClient("ClientA_test", 1);
            Client newClientTwo = new RealClient("ClientB_test", 1);

            clientLogic.RegisterClient(newClientOne);
            clientLogic.RegisterClient(newClientTwo);

            Assert.True(clientLogic.GetAllRegisteredClients().ToList().Any(c => c.Equals(newClientOne)));
            Assert.True(clientLogic.GetAllRegisteredClients().ToList().Any(c => c.Equals(newClientTwo)));
        }

        [Fact]
        public void UpdateNotRegisteredClientShouldThrowException()
        {
            IClientLogic clientLogic = base.ClientLogicInstance;
            Client updatedClient = new RealClient("ClientA_test", 5);

            Assert.Throws<ClientException>(
            delegate {
                clientLogic.UpdateClient(new RealClient("ClientA_test"), updatedClient);
            });
        }

        [Fact]
        public void UpdateClientThatIsInOrderShouldThrowException()
        {
            IClientLogic clientLogic = base.ClientLogicInstance;
            Client client = new RealClient("ClientA_test", 1);
            clientLogic.RegisterClient(client);
            IOrderLogic orderLogic = base.OrderLogicInstance;
            orderLogic.RegisterOrder(new RealOrder(client,
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1)));
            Client updatedClient = new RealClient("ClientA_test", 5);

            Assert.Throws<ClientException>(
            delegate {
                clientLogic.UpdateClient(new RealClient("ClientA_test"), client);
            });
        }

        [Fact]
        public void UpdateClientNameShouldChangeClientName()
        {
            IClientLogic clientLogic = base.ClientLogicInstance;
            Client client = new RealClient("ClientA_test", 1);
            clientLogic.RegisterClient(client);

            Client updatedClient = new RealClient("ClientB_test", 5);
            clientLogic.UpdateClient(client, updatedClient);

            Assert.True(clientLogic.GetAllRegisteredClients().ToList().Any(c => c.Equals(updatedClient)));
        }

        [Fact]
        public void UpdateClientPreferenceShouldChangeClientPreference()
        {
            IClientLogic clientLogic = base.ClientLogicInstance;
            Client client = new RealClient("ClientA_test", 1);
            clientLogic.RegisterClient(client);

            Client updatedClient = new RealClient("ClientA_test", 5);
            clientLogic.UpdateClient(client, updatedClient);

            Assert.True(clientLogic.GetAllRegisteredClients().ToList().Any(c => c.Equals(updatedClient)));
        }

        [Fact]
        public void UnregisterClientThatIsNotRegisteredShouldThrowException()
        {
            IClientLogic clientLogic = base.ClientLogicInstance;

            Assert.Throws<ClientException>(
            delegate {
                clientLogic.UnregisterClient(new RealClient("ClientX"));
            });
        }

        [Fact]
        public void UnregisterClientThatIsInOrderShouldThrowException()
        {
            IClientLogic clientLogic = base.ClientLogicInstance;
            Client client = new RealClient("ClientA_test", 1);
            clientLogic.RegisterClient(client);
            IOrderLogic orderLogic = base.OrderLogicInstance;
            orderLogic.RegisterOrder(new RealOrder(client,
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1)));

            Assert.Throws<ClientException>(
            delegate {
                clientLogic.UnregisterClient(client);
            });
        }

        [Fact]
        public void CheckIfRegisteredClientIsRegisteredShouldReturnTrue()
        {
            IClientLogic clientLogic = base.ClientLogicInstance;
            Client client = new RealClient("ClientA_test", 1);
            clientLogic.RegisterClient(client);

            bool result = clientLogic.ClientIsRegistered(client);

            Assert.True(result);
        }

        [Fact]
        public void CheckIfNonRegisteredClientIsRegisteredShouldReturnFalse()
        {
            IClientLogic clientLogic = base.ClientLogicInstance;

            bool result = clientLogic.ClientIsRegistered(new RealClient("ClientA_test"));

            Assert.False(result);
        }

        [Fact]
        public void GetAllRegisteredClientsShouldReturnAllClientsInClientRepository()
        {
            IClientLogic clientLogic = base.ClientLogicInstance;
            Client newClientOne = new RealClient("ClientA_test", 1);
            Client newClientTwo = new RealClient("ClientB_test", 1);
            Client newClientThree = new RealClient("ClientC_test", 1);
            List<Client> expected = new List<Client>() { newClientOne, newClientTwo, newClientThree };

            clientLogic.RegisterClient(newClientOne);
            clientLogic.RegisterClient(newClientTwo);
            clientLogic.RegisterClient(newClientThree);

            Assert.Equal(expected, clientLogic.GetAllRegisteredClients().ToList());
        }
    }
}
