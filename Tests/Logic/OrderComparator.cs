﻿using PlannerPro.Entities.API;
using System;
using System.Collections.Generic;

namespace PlannerPro.Tests.Logic
{
    public class OrderComparator : IEqualityComparer<Order>
    {
        public bool Equals(Order x, Order y)
        {
            return object.Equals(x.Client, y.Client) &&
                   object.Equals(x.Product, y.Product) &&   
                   object.Equals(x.Quantity, y.Quantity) &&
                   x.DeliveryDate == y.DeliveryDate;
        }

        public int GetHashCode(Order obj)
        {
            throw new NotImplementedException();
        }
    }
}
