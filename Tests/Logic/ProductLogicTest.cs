﻿using PlannerPro.Entities.API;
using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Entities.Exceptions;
using PlannerPro.Logic.API;
using PlannerPro.Logic.BaseTest;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace PlannerPro.Logic.Tests
{
    [Collection("Serial Tests")]
    public class ProductLogicTest : LogicBaseTest
    {
        [Fact]
        public void RegisterNewProduct()
        {
            IProductLogic productLogic = base.ProductLogicInstance;
            Product newProduct = new RealProduct("ProductA_test", 1, 1, 1);

            productLogic.RegisterProduct(newProduct);

            Assert.True(productLogic.GetAllRegisteredProducts().ToList().Any(p => p.Name == "ProductA_test"));
        }

        [Fact]
        public void RegisterTwoNewProducts()
        {
            IProductLogic productLogic = base.ProductLogicInstance;
            Product newProductOne = new RealProduct("ProductA_test", 1, 1, 1);
            Product newProductTwo = new RealProduct("ProductB_test", 1, 1, 1);

            productLogic.RegisterProduct(newProductOne);
            productLogic.RegisterProduct(newProductTwo);

            Assert.True(productLogic.GetAllRegisteredProducts().ToList().Any(p => p.Equals(newProductOne)));
            Assert.True(productLogic.GetAllRegisteredProducts().ToList().Any(p => p.Equals(newProductTwo)));
        }

        [Fact]
        public void RegisterProductWithNameThatIsAlreadyRegisteredShouldThrowException()
        {
            IProductLogic productLogic = base.ProductLogicInstance;
            Product productOne = new RealProduct("ProductA_test", 1, 1, 1);
            Product productSameName = new RealProduct("ProductA_test", 1, 1, 1);
            productLogic.RegisterProduct(productOne);

            Assert.Throws<ProductException>(
            delegate {
                productLogic.RegisterProduct(productSameName);
            });
        }

        [Fact]
        public void UpdateNotRegisteredProductShouldThrowException()
        {
            IProductLogic productLogic = base.ProductLogicInstance;
            Product updatedProduct = new RealProduct("ProductA_test", 1000, 1, 1);

            Assert.Throws<ProductException>(
            delegate {
                productLogic.UpdateProduct(new RealProduct("ProductA_test"), updatedProduct);
            });
        }

        [Fact]
        public void UpdateProductThatIsInOrderShouldThrowException()
        {
            IProductLogic productLogic = base.ProductLogicInstance;
            productLogic.RegisterProduct(new RealProduct("ProductA_test", 1, 1, 1));
            IOrderLogic orderLogic = base.OrderLogicInstance;
            orderLogic.RegisterOrder(new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2050, 1, 1)));
            Product updatedProduct = new RealProduct("ProductA_test", 1000, 1, 1);

            Assert.Throws<ProductException>(
            delegate {
                productLogic.UpdateProduct(new RealProduct("ProductA_test"), updatedProduct);
            });
        }

        public void UpdateProductNameWhenTheNameIsAlreadyRegisteredShouldThrowException()
        {
            IProductLogic productLogic = base.ProductLogicInstance;
            Product productOne = new RealProduct("ProductA_test", 1, 1, 1);
            Product productTwo = new RealProduct("ProductB_test", 1, 1, 1);
            productLogic.RegisterProduct(productOne);
            productLogic.RegisterProduct(productTwo);

            Product updatedProduct = new RealProduct("ProductB_test", 1, 1, 1);

            Assert.Throws<ProductException>(
            delegate {
                productLogic.UpdateProduct(productOne, updatedProduct);
            });
        }

        [Fact]
        public void UpdateProductNameShouldChangeProductName()
        {
            IProductLogic productLogic = base.ProductLogicInstance;
            Product product = new RealProduct("ProductA_test", 1, 1, 1);
            productLogic.RegisterProduct(product);

            Product updatedProduct = new RealProduct("ProductB_test", 1, 1, 1);
            productLogic.UpdateProduct(product, updatedProduct);

            Assert.True(productLogic.ProductIsRegistered(new RealProduct("ProductB_test")));
        }

        [Fact]
        public void UpdateProductManufacturingTimeShouldChangeClientManufacturingTime()
        {
            IProductLogic productLogic = base.ProductLogicInstance;
            Product product = new RealProduct("ProductA_test", 1, 1, 1);
            productLogic.RegisterProduct(product);

            Product updatedProduct = new RealProduct("ProductA_test", 1000, 1, 1);
            productLogic.UpdateProduct(product, updatedProduct);

            Product storedProduct = productLogic.GetAllRegisteredProducts().Single(p => p.Name == "ProductA_test");
            Assert.Equal(1000, storedProduct.ManufacturingTime);
        }

        [Fact]
        public void UnregisterProductThatIsNotRegisteredShouldThrowException()
        {
            IProductLogic productLogic = base.ProductLogicInstance;

            Assert.Throws<ProductException>(
            delegate {
                productLogic.UnregisterProduct(new RealProduct("ProductX"));
            });
        }

        [Fact]
        public void UnregisterProductThatIsInOrderShouldThrowException()
        {
            IProductLogic productLogic = base.ProductLogicInstance;
            Product product = new RealProduct("ProductA_test", 1, 1, 1);
            productLogic.RegisterProduct(product);
            IOrderLogic orderLogic = base.OrderLogicInstance;
            orderLogic.RegisterOrder(new RealOrder(new RealClient("ClientA_test"),
                product, 1, new DateTime(2050, 1, 1)));

            Assert.Throws<ProductException>(
            delegate {
                productLogic.UnregisterProduct(product);
            });
        }

        [Fact]
        public void CheckIfRegisteredProductIsRegisteredShouldReturnTrue()
        {
            IProductLogic productLogic = base.ProductLogicInstance;
            Product product = new RealProduct("ProductA_test", 1, 1, 1);
            productLogic.RegisterProduct(product);

            Assert.True(productLogic.ProductIsRegistered(product));
        }

        [Fact]
        public void CheckIfNonRegisteredProductIsRegisteredShouldReturnFalse()
        {
            IProductLogic productLogic = base.ProductLogicInstance;

            bool result = productLogic.ProductIsRegistered(new RealProduct("ProductA_test"));

            Assert.False(result);
        }

        [Fact]
        public void GetAllRegisteredProductsShouldReturnAllProductsInProductRepository()
        {
            IProductLogic productLogic = base.ProductLogicInstance;
            Product newProductOne = new RealProduct("ProductA_test", 1, 1, 1);
            Product newProductTwo = new RealProduct("ProductB_test", 1, 1, 1);
            Product newProductThree = new RealProduct("ProductC_test", 1, 1, 1);
            List<Product> expected = new List<Product>() { newProductOne, newProductTwo, newProductThree };

            productLogic.RegisterProduct(newProductOne);
            productLogic.RegisterProduct(newProductTwo);
            productLogic.RegisterProduct(newProductThree);

            Assert.Equal(expected, productLogic.GetAllRegisteredProducts().ToList());
        }
    }
}
