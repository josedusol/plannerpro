﻿using PlannerPro.Entities.API;
using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Entities.Exceptions;
using PlannerPro.Logic.API;
using PlannerPro.Logic.BaseTest;
using PlannerPro.Logic.LogicImp;
using PlannerPro.Tests.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace PlannerPro.Logic.Tests
{
    [Collection("Serial Tests")]
    public class PlanLogicTest : LogicBaseTest
    {
        [Fact]
        public void RunPlanWithSingleOrder()
        {
            IPlanLogic planLogic = base.PlanLogicInstance;
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order order = new RealOrder(new RealClient("ClientA_test"),
                new RealProduct("ProductA_test"), 1, new DateTime(2055, 10, 1));
            orderLogic.RegisterOrder(order);

            Plan plan = new RealPlan("PlanA_test");
            planLogic.RunPlan(plan);

            Plan storedPlan = planLogic.GetAllPlans().Single(p => p.Name == "PlanA_test");
            Assert.True(storedPlan.PendentOrders.Count == 1);
        }

        [Fact]
        public void RunPlanWithTwoOrders()
        {
            IPlanLogic planLogic = base.PlanLogicInstance;
            IOrderLogic orderLogic = base.OrderLogicInstance;
            orderLogic.RegisterOrder(new RealOrder(new RealClient("ClientA_test"),
                    new RealProduct("ProductA_test"), 1, new DateTime(2015, 10, 1)));
            orderLogic.RegisterOrder(new RealOrder(new RealClient("ClientA_test"),
                   new RealProduct("ProductB_test"), 1, new DateTime(2015, 10, 2)));

            Plan plan = new RealPlan("PlanA_test");
            planLogic.RunPlan(plan);

            Plan storedPlan = planLogic.GetAllPlans().Single(p => p.Name == "PlanA_test");
            Assert.True(storedPlan.PendentOrders.Count == 2);
        }

        [Fact]
        public void RunPlanByDeliveryDateCriteria()
        {
            PlanLogic planLogic = base.PlanLogicInstance;
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order orderOne = new RealOrder(new RealClient("ClientA_test"),
               new RealProduct("ProductA_test"), 1, new DateTime(2020, 1, 2));
            Order orderTwo = new RealOrder(new RealClient("ClientB_test"),
                new RealProduct("ProductB_test"), 1, new DateTime(2020, 1, 1));
            Order orderThree = new RealOrder(new RealClient("ClientC_test"),
                new RealProduct("ProductC_test"), 1, new DateTime(2020, 1, 3));
            orderLogic.RegisterOrder(orderOne);
            orderLogic.RegisterOrder(orderTwo);
            orderLogic.RegisterOrder(orderThree);
            IList<Order> expectedOrders = new List<Order>() { orderTwo, orderOne, orderThree };
            
            Plan plan = new RealPlan("PlanA_test", "Test plan named A", new DeliveryDateCriteria());
            planLogic.RunPlan(plan);

            Plan storedPlan = planLogic.GetAllPlans().Single(p => p.Name == "PlanA_test");
            storedPlan.DoPlanning();
            Assert.Equal<Order>(expectedOrders, storedPlan.PendentOrders, new OrderComparator());
        }

        [Fact]
        public void RunPlanByFilteredDeliveryDateCriteria()
        {
            IPlanLogic planLogic = base.PlanLogicInstance;
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order orderOne = new RealOrder(new RealClient("ClientA_test", 5),
               new RealProduct("ProductA_test"), 1001, new DateTime(2020, 1, 2));
            Order orderTwo = new RealOrder(new RealClient("ClientB_test", 5),
                new RealProduct("ProductB_test"), 1001, new DateTime(2020, 1, 1));
            Order orderThree = new RealOrder(new RealClient("ClientC_test", 5),
                new RealProduct("ProductC_test"), 1001, new DateTime(2020, 1, 3));
            Order orderFour = new RealOrder(new RealClient("ClientD_test"),
                new RealProduct("ProductD_test"), 1000, new DateTime(2020, 1, 4));
            Order orderFive = new RealOrder(new RealClient("ClientE_test", 4),
               new RealProduct("ProductE_test"), 2000, new DateTime(2020, 1, 5));
            orderLogic.RegisterOrder(orderOne);
            orderLogic.RegisterOrder(orderTwo);
            orderLogic.RegisterOrder(orderThree);
            orderLogic.RegisterOrder(orderFour);
            orderLogic.RegisterOrder(orderFive);
            IList<Order> expectedOrders = new List<Order>() { orderTwo, orderOne, orderThree };

            Plan plan = new RealPlan("PlanA_test", "Test plan named A", new FilteredDeliveryDateCriteria());
            planLogic.RunPlan(plan);

            Plan storedPlan = planLogic.GetAllPlans().Single(p => p.Name == "PlanA_test");
            storedPlan.DoPlanning();
            Assert.Equal<Order>(expectedOrders, storedPlan.PendentOrders, new OrderComparator());
        }

        [Fact]
        public void RunPlanByClientPeferenceCriteria()
        {
            IPlanLogic planLogic = base.PlanLogicInstance;
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order orderOne = new RealOrder(new RealClient("ClientA_test", 2),
               new RealProduct("ProductA_test"), 1, new DateTime(2020, 1, 1));
            Order orderTwo = new RealOrder(new RealClient("ClientB_test", 1),
                new RealProduct("ProductB_test"), 1, new DateTime(2020, 1, 1));
            Order orderThree = new RealOrder(new RealClient("ClientC_test", 3),
                new RealProduct("ProductC_test"), 1, new DateTime(2020, 1, 1));
            orderLogic.RegisterOrder(orderOne);
            orderLogic.RegisterOrder(orderTwo);
            orderLogic.RegisterOrder(orderThree);
            IList<Order> expectedOrders = new List<Order>() { orderThree, orderOne, orderTwo };
            
            Plan plan = new RealPlan("PlanA_test", "Test plan named A", new ClientPreferenceCriteria());
            planLogic.RunPlan(plan);

            Plan storedPlan = planLogic.GetAllPlans().Single(p => p.Name == "PlanA_test");
            storedPlan.DoPlanning();
            Assert.Equal<Order>(expectedOrders, storedPlan.PendentOrders, new OrderComparator());
        }

        [Fact]
        public void RunPlanByProfitMarginCriteria()
        {
            IPlanLogic planLogic = base.PlanLogicInstance;
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order orderOne = new RealOrder(new RealClient("ClientA_test"),
               new RealProduct("ProductA_test", 1, 100, 30), 1, new DateTime(2020, 1, 1));   //Profit: 70
            Order orderTwo = new RealOrder(new RealClient("ClientB_test"),
                new RealProduct("ProductB_test", 1, 100, 40), 1, new DateTime(2020, 1, 1));  //Profit: 60
            Order orderThree = new RealOrder(new RealClient("ClientC_test"),
                new RealProduct("ProductC_test", 1, 100, 20), 1, new DateTime(2020, 1, 1));  //Profit: 80
            orderLogic.RegisterOrder(orderOne);
            orderLogic.RegisterOrder(orderTwo);
            orderLogic.RegisterOrder(orderThree);
            IList<Order> expectedOrders = new List<Order>() { orderThree, orderOne, orderTwo };

            Plan plan = new RealPlan("PlanA_test", "Test plan named A", new ProfitMarginCriteria());
            planLogic.RunPlan(plan);

            Plan storedPlan = planLogic.GetAllPlans().Single(p => p.Name == "PlanA_test");
            storedPlan.DoPlanning();            
            Assert.Equal<Order>(expectedOrders, storedPlan.PendentOrders, new OrderComparator());
        }

        [Fact]
        public void CancelPlanThatIsNotRunningShouldThrowException()
        {
            IPlanLogic planLogic = base.PlanLogicInstance;
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order order = new RealOrder(new RealClient("ClientA_test"),
               new RealProduct("ProductA_test", 1, 100, 30), 1, new DateTime(2020, 1, 1));
            orderLogic.RegisterOrder(order);
            Plan plan = new RealPlan("PlanA_test");
            planLogic.RunPlan(plan);
            planLogic.CancelPlan(plan);

            Assert.Throws<PlanException>(
              delegate {
                  planLogic.CancelPlan(plan);
              });
        }

        [Fact]
        public void FinalizeNextOrderTest()
        {
            IPlanLogic planLogic = base.PlanLogicInstance;
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order orderOne = new RealOrder(new RealClient("ClientA_test", 1),
               new RealProduct("ProductA_test", 1, 100, 30), 1, new DateTime(2020, 1, 1));
            Order orderTwo = new RealOrder(new RealClient("ClientB_test", 2),
                new RealProduct("ProductB_test", 1, 100, 40), 1, new DateTime(2020, 1, 1));
            Order orderThree = new RealOrder(new RealClient("ClientC_test", 3),
                new RealProduct("ProductC_test", 1, 100, 20), 1, new DateTime(2020, 1, 1));          
            orderLogic.RegisterOrder(orderOne);
            orderLogic.RegisterOrder(orderTwo);
            orderLogic.RegisterOrder(orderThree);
            IList<Order> expectedOrders = new List<Order>() { orderThree, orderOne, orderTwo };

            Plan plan = new RealPlan("PlanA_test", "Test plan named A", new ClientPreferenceCriteria());
            planLogic.RunPlan(plan);
            planLogic.FinalizeNextOrder(plan);

            Plan storedPlan = planLogic.GetAllPlans().Single(p => p.Name == "PlanA_test");
            Assert.False(storedPlan.PendentOrders.Contains(orderThree));
        }

        [Fact]
        public void FinalizeNextOrderForNotRunningPlanShouldThrowException()
        {
            IPlanLogic planLogic = base.PlanLogicInstance;
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order order = new RealOrder(new RealClient("ClientA_test"),
               new RealProduct("ProductA_test", 1, 100, 30), 1, new DateTime(2020, 1, 1));
            orderLogic.RegisterOrder(order);
            Plan plan = new RealPlan("PlanA_test");
            planLogic.RunPlan(plan);
            planLogic.CancelPlan(plan);

            Assert.Throws<PlanException>(
              delegate {
                  planLogic.FinalizeNextOrder(plan);
              });
        }

        [Fact]
        public void CancelPlanTest()
        {
            IPlanLogic planLogic = base.PlanLogicInstance;
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order orderOne = new RealOrder(new RealClient("ClientA_test", 1),
               new RealProduct("ProductA_test", 1, 100, 30), 1, new DateTime(2020, 1, 1));
            Order orderTwo = new RealOrder(new RealClient("ClientB_test", 2),
                new RealProduct("ProductB_test", 1, 100, 40), 1, new DateTime(2020, 1, 1));
            Order orderThree = new RealOrder(new RealClient("ClientC_test", 3),
                new RealProduct("ProductC_test", 1, 100, 20), 1, new DateTime(2020, 1, 1));          
            orderLogic.RegisterOrder(orderOne);
            orderLogic.RegisterOrder(orderTwo);
            orderLogic.RegisterOrder(orderThree);
            IList<Order> expectedOrders = new List<Order>() { orderThree, orderOne, orderTwo };

            Plan plan = new RealPlan("PlanA_test");
            planLogic.RunPlan(plan);
            planLogic.CancelPlan(plan);

            Plan storedPlan = planLogic.GetAllPlans().Single(p => p.Name == "PlanA_test");
            Assert.True(storedPlan.Status == PlanStatus.Cancelled 
                && storedPlan.PendentOrders.Count == 0);
        }

        [Fact]
        public void OrderDeliveryStatusDelayedTest()
        {
            IPlanLogic planLogic = base.PlanLogicInstance;
            IOrderLogic orderLogic = base.OrderLogicInstance;
            Order orderOne = new RealOrder(new RealClient("ClientA_test", 3),
               new RealProduct("ProductA_test", 2880, 100, 30), 2, DateTime.Now.AddDays(1));
            Order orderTwo = new RealOrder(new RealClient("ClientB_test", 2),
                new RealProduct("ProductB_test", 1, 100, 40), 1, DateTime.Now.AddDays(1));
            Order orderThree = new RealOrder(new RealClient("ClientC_test", 1),
                new RealProduct("ProductC_test", 1, 100, 20), 1, DateTime.Now.AddDays(1));
            orderLogic.RegisterOrder(orderOne);
            orderLogic.RegisterOrder(orderTwo);
            orderLogic.RegisterOrder(orderThree);
            IList<Order> expectedOrders = new List<Order>() { orderThree, orderOne, orderTwo };

            Plan plan = new RealPlan("PlanA_test", "Test plan named A", new ClientPreferenceCriteria());
            planLogic.RunPlan(plan);

            Plan storedPlan = planLogic.GetAllPlans().Single(p => p.Name == "PlanA_test");
            storedPlan.DoPlanning();
            Assert.True(storedPlan.PendentOrders.First().DeliveryStatus == DeliveryStatus.Delayed);
        }
    }
}