﻿using PlannerPro.Entities.API;
using System.Collections.Generic;

namespace PlannerPro.Data.API
{
    public interface IOrderRepository
    {
        void CreateOrder(Order order);
        void DeleteOrder(Order order);
        void AttachOrder(Order order);
        Order GetOrder(Order order);
        IEnumerable<Order> GetAllOrders();
    }
}
