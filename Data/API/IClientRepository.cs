﻿using PlannerPro.Entities.API;
using System.Collections.Generic;

namespace PlannerPro.Data.API
{
    public interface IClientRepository
    {
        void CreateClient(Client client);
        void DeleteClient(Client client);
        void AttachClient(Client client);
        Client GetClient(Client client);
        IEnumerable<Client> GetAllClients();
    }
}
