﻿using PlannerPro.Entities.API;
using System.Collections.Generic;

namespace PlannerPro.Data.API
{
    public interface IProductRepository
    {
        void CreateProduct(Product product);
        void DeleteProduct(Product product);
        void AttachProduct(Product product);
        Product GetProduct(Product product);
        IEnumerable<Product> GetAllProducts();
    }
}
