﻿using PlannerPro.Data.DataImp;

namespace PlannerPro.Data.API
{
    public interface IUnitOfWork
    {
        ProductRepository ProductRepository { get; }
        ClientRepository ClientRepository { get; }
        OrderRepository OrderRepository { get; }
        PlanRepository PlanRepository { get; }
        void Save();
    }
}
