﻿using PlannerPro.Entities.API;
using System.Collections.Generic;

namespace PlannerPro.Data.API
{
    public interface IPlanRepository
    {
        void CreatePlan(Plan plan);
        void DeletePlan(Plan plan);
        void AttachPlan(Plan plan);
        Plan GetPlan(Plan plan);
        IEnumerable<Plan> GetAllPlans();
    }
}
