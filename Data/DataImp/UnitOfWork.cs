﻿using PlannerPro.Data.API;
using System;

namespace PlannerPro.Data.DataImp
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private static UnitOfWork unitOfWorkinstance;
        private PlannerProContext context;
        private ProductRepository productRepository;
        private ClientRepository clientRepository;
        private OrderRepository orderRepository;
        private PlanRepository planRepository;
        private bool disposed = false;

        public static UnitOfWork GetInstance()
        {
            if (unitOfWorkinstance == null)
                unitOfWorkinstance = new UnitOfWork();

            return unitOfWorkinstance;
        }

        private UnitOfWork()
        {
            context = new PlannerProContext();
        }

        public ProductRepository ProductRepository
        {
            get
            {
                if (productRepository == null) {
                    productRepository = new ProductRepository(context);
                }
                return productRepository;
            }
        }

        public ClientRepository ClientRepository
        {
            get
            {
                if (clientRepository == null) {
                    clientRepository = new ClientRepository(context);
                }
                return clientRepository;
            }
        }

        public OrderRepository OrderRepository
        {
            get {
                if (orderRepository == null) {
                    orderRepository = new OrderRepository(context);
                }
                return orderRepository;
            }
        }

        public PlanRepository PlanRepository
        {
            get
            {
                if (planRepository == null) {
                    planRepository = new PlanRepository(context);
                }
                return planRepository;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
