﻿using PlannerPro.Data.API;
using PlannerPro.Entities.API;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace PlannerPro.Data.DataImp
{
    public class PlanRepository : IPlanRepository
    {
        private PlannerProContext context;

        public PlanRepository(PlannerProContext context)
        {
            this.context = context;
        }

        public void CreatePlan(Plan plan)
        {
            context.Plans.Add(plan);
        }

        public void DeletePlan(Plan plan)
        {
            context.Plans.Attach(plan);
            context.Plans.Remove(plan);
        }

        public void AttachPlan(Plan plan)
        {
            context.Plans.Attach(plan);
        }

        public Plan GetPlan(Plan plan)
        {
            Plan result = context.Plans.Find(plan.PlanID);
            return (result == null ? Plan.NULL : result);  
        }

        public IEnumerable<Plan> GetAllPlans()
        {
            return context.Plans.Include(p => p.PlanningCriteria)
                .Include(p => p.FinalizedOrders).ToList();   
        }
    }
}
