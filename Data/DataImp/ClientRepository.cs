﻿using PlannerPro.Data.API;
using PlannerPro.Entities.API;
using System.Collections.Generic;
using System.Linq;

namespace PlannerPro.Data.DataImp
{
    public class ClientRepository : IClientRepository
    {
        private PlannerProContext context;

        public ClientRepository(PlannerProContext context)
        {
            this.context = context;
        }

        public void CreateClient(Client client)
        {
            context.Clients.Add(client);
        }

        public void DeleteClient(Client client)
        {
            context.Clients.Attach(client);
            context.Clients.Remove(client);
        }

        public void AttachClient(Client client)
        {
            context.Clients.Attach(client);
        }

        public Client GetClient(Client client)
        {
            Client result = context.Clients.Find(client.ClientID);

            return (result == null ? Client.NULL : result);    
        }

        public IEnumerable<Client> GetAllClients()
        {
            return context.Clients.ToList();   
        }
    }
}
