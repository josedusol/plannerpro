﻿using PlannerPro.Data.API;
using PlannerPro.Entities.API;
using System.Collections.Generic;
using System.Linq;

namespace PlannerPro.Data.DataImp
{
    public class OrderRepository : IOrderRepository
    {
        private PlannerProContext context;

        public OrderRepository(PlannerProContext context)
        {
            this.context = context;
        }

        public void CreateOrder(Order order)
        {
            context.Orders.Add(order);
        }

        public void DeleteOrder(Order order)
        {
            context.Orders.Attach(order);
            context.Orders.Remove(order);
        }

        public void AttachOrder(Order order)
        {
            context.Orders.Attach(order);
        }

        public Order GetOrder(Order order)
        {
            Order result = context.Orders.Find(order.OrderID);
            return (result == null ? Order.NULL : result);
        }

        public IEnumerable<Order> GetAllOrders()
        {
            return context.Orders.ToList();
        }
    }
}
