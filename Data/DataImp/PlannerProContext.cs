﻿using PlannerPro.Entities.API;
using System.Data.Entity;

namespace PlannerPro.Data.DataImp
{
    public class PlannerProContext : DbContext
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Plan> Plans { get; set; }
        public DbSet<PlanningCriteria> PlanningCriterias { get; set; }
    }
}
