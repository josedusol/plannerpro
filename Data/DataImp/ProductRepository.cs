﻿using PlannerPro.Data.API;
using PlannerPro.Entities.API;
using System.Collections.Generic;
using System.Linq;

namespace PlannerPro.Data.DataImp
{
    public class ProductRepository : IProductRepository
    {
        private PlannerProContext context;

        public ProductRepository(PlannerProContext context)
        {
            this.context = context;
        }

        public void CreateProduct(Product product)
        {
            context.Products.Add(product);
        }

        public void DeleteProduct(Product product)
        {
            context.Products.Attach(product);
            context.Products.Remove(product);
        }

        public void AttachProduct(Product product)
        {
            context.Products.Attach(product);
        }

        public Product GetProduct(Product product)
        {
            Product result = context.Products.Find(product.ProductID);
            return (result == null ? Product.NULL : result);   
        }

        public IEnumerable<Product> GetAllProducts()
        {
            return context.Products.ToList(); 
        }
    }
}
