﻿using System.Windows.Forms;

namespace PlannerPro.Utilities
{
    public static class DialogBox
    {
        public static DialogResult DisplayError(string title, string message)
        {
            MessageBoxButtons button = MessageBoxButtons.OK;
            MessageBoxIcon icon = MessageBoxIcon.Error;
            DialogResult result = MessageBox.Show(message, title, button, icon);
            return result;
        }

        public static DialogResult DisplayInfo(string title, string message)
        {
            MessageBoxButtons button = MessageBoxButtons.OK;
            MessageBoxIcon icon = MessageBoxIcon.Information;
            DialogResult result = MessageBox.Show(message, title, button, icon);
            return result;
        }
    }
}
