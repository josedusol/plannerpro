﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace PlannerPro.Utilities
{
    public static class EnumHelpers
    {
        public static string ToDescriptionString(this Enum _enum)
        {
            Type type = _enum.GetType();
            MemberInfo[] memberInfo = type.GetMember(_enum.ToString());

            if (memberInfo != null && memberInfo.Length > 0) {
                object[] attrs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }

            return _enum.ToString();
        }
    }
}
