﻿namespace PlannerPro.GUI
{
    partial class GUIOrder
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxOrder = new System.Windows.Forms.GroupBox();
            this.dataGridViewOrder = new System.Windows.Forms.DataGridView();
            this.ColumnProduct = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnClient = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateTimePickerOrder = new System.Windows.Forms.DateTimePicker();
            this.numericUpDownQuantity = new System.Windows.Forms.NumericUpDown();
            this.comboBoxClient = new System.Windows.Forms.ComboBox();
            this.comboBoxProduct = new System.Windows.Forms.ComboBox();
            this.labelDate = new System.Windows.Forms.Label();
            this.labelQuantity = new System.Windows.Forms.Label();
            this.labelClient = new System.Windows.Forms.Label();
            this.labelProduct = new System.Windows.Forms.Label();
            this.buttonUpdateOrder = new System.Windows.Forms.Button();
            this.buttonUnregisterOrder = new System.Windows.Forms.Button();
            this.buttonRegisterOrder = new System.Windows.Forms.Button();
            this.groupBoxOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQuantity)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxOrder
            // 
            this.groupBoxOrder.Controls.Add(this.dataGridViewOrder);
            this.groupBoxOrder.Controls.Add(this.dateTimePickerOrder);
            this.groupBoxOrder.Controls.Add(this.numericUpDownQuantity);
            this.groupBoxOrder.Controls.Add(this.comboBoxClient);
            this.groupBoxOrder.Controls.Add(this.comboBoxProduct);
            this.groupBoxOrder.Controls.Add(this.labelDate);
            this.groupBoxOrder.Controls.Add(this.labelQuantity);
            this.groupBoxOrder.Controls.Add(this.labelClient);
            this.groupBoxOrder.Controls.Add(this.labelProduct);
            this.groupBoxOrder.Controls.Add(this.buttonUpdateOrder);
            this.groupBoxOrder.Controls.Add(this.buttonUnregisterOrder);
            this.groupBoxOrder.Controls.Add(this.buttonRegisterOrder);
            this.groupBoxOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxOrder.Location = new System.Drawing.Point(0, 0);
            this.groupBoxOrder.Name = "groupBoxOrder";
            this.groupBoxOrder.Size = new System.Drawing.Size(700, 320);
            this.groupBoxOrder.TabIndex = 1;
            this.groupBoxOrder.TabStop = false;
            this.groupBoxOrder.Text = "Pedidos";
            // 
            // dataGridViewOrder
            // 
            this.dataGridViewOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnProduct,
            this.ColumnClient,
            this.ColumnQuantity,
            this.ColumnDate,
            this.ColumnState});
            this.dataGridViewOrder.Location = new System.Drawing.Point(278, 14);
            this.dataGridViewOrder.Name = "dataGridViewOrder";
            this.dataGridViewOrder.Size = new System.Drawing.Size(389, 237);
            this.dataGridViewOrder.TabIndex = 19;
            // 
            // ColumnProduct
            // 
            this.ColumnProduct.HeaderText = "Producto";
            this.ColumnProduct.Name = "ColumnProduct";
            // 
            // ColumnClient
            // 
            this.ColumnClient.HeaderText = "Cliente";
            this.ColumnClient.Name = "ColumnClient";
            // 
            // ColumnQuantity
            // 
            this.ColumnQuantity.HeaderText = "Cantidad";
            this.ColumnQuantity.Name = "ColumnQuantity";
            // 
            // ColumnDate
            // 
            this.ColumnDate.HeaderText = "Fecha de Entrega";
            this.ColumnDate.Name = "ColumnDate";
            // 
            // ColumnState
            // 
            this.ColumnState.HeaderText = "Estado";
            this.ColumnState.Name = "ColumnState";
            // 
            // dateTimePickerOrder
            // 
            this.dateTimePickerOrder.Location = new System.Drawing.Point(125, 178);
            this.dateTimePickerOrder.Name = "dateTimePickerOrder";
            this.dateTimePickerOrder.Size = new System.Drawing.Size(120, 20);
            this.dateTimePickerOrder.TabIndex = 18;
            // 
            // numericUpDownQuantity
            // 
            this.numericUpDownQuantity.Location = new System.Drawing.Point(124, 149);
            this.numericUpDownQuantity.Name = "numericUpDownQuantity";
            this.numericUpDownQuantity.Size = new System.Drawing.Size(121, 20);
            this.numericUpDownQuantity.TabIndex = 17;
            // 
            // comboBoxClient
            // 
            this.comboBoxClient.FormattingEnabled = true;
            this.comboBoxClient.Location = new System.Drawing.Point(124, 118);
            this.comboBoxClient.Name = "comboBoxClient";
            this.comboBoxClient.Size = new System.Drawing.Size(121, 21);
            this.comboBoxClient.TabIndex = 16;
            // 
            // comboBoxProduct
            // 
            this.comboBoxProduct.FormattingEnabled = true;
            this.comboBoxProduct.Location = new System.Drawing.Point(124, 87);
            this.comboBoxProduct.Name = "comboBoxProduct";
            this.comboBoxProduct.Size = new System.Drawing.Size(121, 21);
            this.comboBoxProduct.TabIndex = 15;
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Location = new System.Drawing.Point(23, 178);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(95, 13);
            this.labelDate.TabIndex = 14;
            this.labelDate.Text = "Fecha de Entrega:";
            // 
            // labelQuantity
            // 
            this.labelQuantity.AutoSize = true;
            this.labelQuantity.Location = new System.Drawing.Point(66, 151);
            this.labelQuantity.Name = "labelQuantity";
            this.labelQuantity.Size = new System.Drawing.Size(52, 13);
            this.labelQuantity.TabIndex = 13;
            this.labelQuantity.Text = "Cantidad:";
            // 
            // labelClient
            // 
            this.labelClient.AutoSize = true;
            this.labelClient.Location = new System.Drawing.Point(76, 121);
            this.labelClient.Name = "labelClient";
            this.labelClient.Size = new System.Drawing.Size(42, 13);
            this.labelClient.TabIndex = 12;
            this.labelClient.Text = "Cliente:";
            // 
            // labelProduct
            // 
            this.labelProduct.AutoSize = true;
            this.labelProduct.Location = new System.Drawing.Point(65, 90);
            this.labelProduct.Name = "labelProduct";
            this.labelProduct.Size = new System.Drawing.Size(53, 13);
            this.labelProduct.TabIndex = 11;
            this.labelProduct.Text = "Producto:";
            // 
            // buttonUpdateOrder
            // 
            this.buttonUpdateOrder.Location = new System.Drawing.Point(236, 257);
            this.buttonUpdateOrder.Name = "buttonUpdateOrder";
            this.buttonUpdateOrder.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdateOrder.TabIndex = 10;
            this.buttonUpdateOrder.Text = "Modificar";
            this.buttonUpdateOrder.UseVisualStyleBackColor = true;
            // 
            // buttonUnregisterOrder
            // 
            this.buttonUnregisterOrder.Location = new System.Drawing.Point(131, 257);
            this.buttonUnregisterOrder.Name = "buttonUnregisterOrder";
            this.buttonUnregisterOrder.Size = new System.Drawing.Size(75, 23);
            this.buttonUnregisterOrder.TabIndex = 9;
            this.buttonUnregisterOrder.Text = "Baja";
            this.buttonUnregisterOrder.UseVisualStyleBackColor = true;
            // 
            // buttonRegisterOrder
            // 
            this.buttonRegisterOrder.Location = new System.Drawing.Point(26, 257);
            this.buttonRegisterOrder.Name = "buttonRegisterOrder";
            this.buttonRegisterOrder.Size = new System.Drawing.Size(75, 23);
            this.buttonRegisterOrder.TabIndex = 0;
            this.buttonRegisterOrder.Text = "Alta";
            this.buttonRegisterOrder.UseVisualStyleBackColor = true;
            this.buttonRegisterOrder.Click += new System.EventHandler(this.buttonRegisterOrder_Click);
            // 
            // GUIOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxOrder);
            this.Name = "GUIOrder";
            this.Size = new System.Drawing.Size(700, 320);
            this.groupBoxOrder.ResumeLayout(false);
            this.groupBoxOrder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQuantity)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxOrder;
        private System.Windows.Forms.DataGridView dataGridViewOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnProduct;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnClient;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnState;
        private System.Windows.Forms.DateTimePicker dateTimePickerOrder;
        private System.Windows.Forms.NumericUpDown numericUpDownQuantity;
        private System.Windows.Forms.ComboBox comboBoxClient;
        private System.Windows.Forms.ComboBox comboBoxProduct;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Label labelQuantity;
        private System.Windows.Forms.Label labelClient;
        private System.Windows.Forms.Label labelProduct;
        private System.Windows.Forms.Button buttonUpdateOrder;
        private System.Windows.Forms.Button buttonUnregisterOrder;
        private System.Windows.Forms.Button buttonRegisterOrder;
    }
}
