﻿using PlannerPro.Entities.API;
using PlannerPro.Entities.EntitiesImp;
using System;
using System.Windows.Forms;

namespace PlannerPro.GUI.Forms
{
    public partial class PlanningDialog : Form
    {
        public PlanningDialog(PlanningCriteria defaultPlanningCriteria)
        {
            InitializeComponent();
            comboBoxPlanningCriteria.DataSource = new PlanningCriteria[] { 
                new DeliveryDateCriteria(), 
                new FilteredDeliveryDateCriteria(),
                new ClientPreferenceCriteria(),
                new ProfitMarginCriteria()};
            comboBoxPlanningCriteria.SelectedItem = defaultPlanningCriteria;
        }

        public PlanningCriteria PlanningCriteria { get; set; }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            PlanningCriteria = (PlanningCriteria) comboBoxPlanningCriteria.SelectedItem;
            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
