﻿using PlannerPro.Entities.API;
using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Logic.API;
using PlannerPro.Logic.LogicImp;
using PlannerPro.Utilities;
using System;
using System.Windows.Forms;

namespace PlannerPro.GUI.Forms
{
    public partial class MainScreen : Form
    {
        private IPlanLogic planLogic;

        public MainScreen()
        {
            InitializeComponent();
            planLogic = PlanLogic.GetInstance();
        }

        private void toolStripButtonProduct_Click(object sender, EventArgs e)
        {
            productScreen.RefreshData();
            tabControlScreens.SelectTab(tabPageProduct);
        }

        private void toolStripButtonClient_Click(object sender, EventArgs e)
        {
            clientScreen.RefreshData();
            tabControlScreens.SelectTab(tabPageClient);
        }

        private void toolStripButtonOrder_Click(object sender, EventArgs e)
        {
            orderScreen.RefreshData();
            tabControlScreens.SelectTab(tabPageOrder);
        }

        private void toolStripButtonPlan_Click(object sender, EventArgs e)
        {
            tabControlScreens.SelectTab(tabPagePlan);
        }

        private void toolStripButtonPlanning_Click(object sender, EventArgs e)
        {
            var planningDialog = new PlanningDialog(planLogic.DefaultPlanningCriteria);
            var result = planningDialog.ShowDialog(this);
            
            if (result == DialogResult.OK)
                planLogic.DefaultPlanningCriteria = planningDialog.PlanningCriteria;
        }

        private void toolStripButtonHistory_Click(object sender, EventArgs e)
        {
            historyScreen.RefreshData();
            tabControlScreens.SelectTab(tabPageHistory);
        }

        private void toolStripButtonAbout_Click(object sender, EventArgs e)
        {
            DialogBox.DisplayInfo("Acerca de", "PlannerPro v2.0"
            + Environment.NewLine + "Diseño de Aplicaciones"
            + Environment.NewLine + "Obligatorio 1 - Abril/Mayo 2015"
            + Environment.NewLine + "José Solsona 182285"
            + Environment.NewLine + "Juan Heber 180829");
        }

        private void MainScreen_FormClosing(object sender, FormClosingEventArgs e)
        {
            var result = MessageBox.Show("Seguro que desea salir?", 
                "Confirmación", MessageBoxButtons.YesNo);

            if (result == DialogResult.No) {
                e.Cancel = true;
                this.Activate();
            }   
        }
    }
}
