﻿using PlannerPro.GUI.UserControls.ClientScreen;
using PlannerPro.GUI.UserControls.HistoryScreen;
using PlannerPro.GUI.UserControls.OrderScreen;
using PlannerPro.GUI.UserControls.PlanScreen;
using PlannerPro.GUI.UserControls.ProductScreen;

namespace PlannerPro.GUI.Forms
{
    partial class MainScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainScreen));
            this.toolStripOptions = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonProduct = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonClient = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonOrder = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPlan = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPlanning = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonHistory = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAbout = new System.Windows.Forms.ToolStripButton();
            this.tabControlScreens = new System.Windows.Forms.TabControl();
            this.tabPageProduct = new System.Windows.Forms.TabPage();
            this.productScreen = new PlannerPro.GUI.UserControls.ProductScreen.ProductScreen();
            this.tabPageClient = new System.Windows.Forms.TabPage();
            this.clientScreen = new PlannerPro.GUI.UserControls.ClientScreen.ClientScreen();
            this.tabPageOrder = new System.Windows.Forms.TabPage();
            this.orderScreen = new PlannerPro.GUI.UserControls.OrderScreen.OrderScreen();
            this.tabPagePlan = new System.Windows.Forms.TabPage();
            this.planScreen = new PlannerPro.GUI.UserControls.PlanScreen.PlanScreen();
            this.tabPageHistory = new System.Windows.Forms.TabPage();
            this.historyScreen = new PlannerPro.GUI.UserControls.HistoryScreen.HistoryScreen();
            this.toolStripOptions.SuspendLayout();
            this.tabControlScreens.SuspendLayout();
            this.tabPageProduct.SuspendLayout();
            this.tabPageClient.SuspendLayout();
            this.tabPageOrder.SuspendLayout();
            this.tabPagePlan.SuspendLayout();
            this.tabPageHistory.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripOptions
            // 
            this.toolStripOptions.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonProduct,
            this.toolStripButtonClient,
            this.toolStripButtonOrder,
            this.toolStripButtonPlan,
            this.toolStripButtonPlanning,
            this.toolStripButtonHistory,
            this.toolStripSeparator,
            this.toolStripButtonAbout});
            this.toolStripOptions.Location = new System.Drawing.Point(0, 0);
            this.toolStripOptions.Name = "toolStripOptions";
            this.toolStripOptions.Size = new System.Drawing.Size(929, 71);
            this.toolStripOptions.TabIndex = 0;
            // 
            // toolStripButtonProduct
            // 
            this.toolStripButtonProduct.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonProduct.Image = global::PlannerPro.GUI.Properties.Resources.ProductIcon;
            this.toolStripButtonProduct.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonProduct.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonProduct.Margin = new System.Windows.Forms.Padding(10, 1, 10, 2);
            this.toolStripButtonProduct.Name = "toolStripButtonProduct";
            this.toolStripButtonProduct.Size = new System.Drawing.Size(68, 68);
            this.toolStripButtonProduct.ToolTipText = "Productos";
            this.toolStripButtonProduct.Click += new System.EventHandler(this.toolStripButtonProduct_Click);
            // 
            // toolStripButtonClient
            // 
            this.toolStripButtonClient.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonClient.Image = global::PlannerPro.GUI.Properties.Resources.ClientIcon;
            this.toolStripButtonClient.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonClient.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonClient.Margin = new System.Windows.Forms.Padding(0, 1, 10, 2);
            this.toolStripButtonClient.Name = "toolStripButtonClient";
            this.toolStripButtonClient.Size = new System.Drawing.Size(68, 68);
            this.toolStripButtonClient.ToolTipText = "Clientes";
            this.toolStripButtonClient.Click += new System.EventHandler(this.toolStripButtonClient_Click);
            // 
            // toolStripButtonOrder
            // 
            this.toolStripButtonOrder.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonOrder.Image = global::PlannerPro.GUI.Properties.Resources.OrderIcon;
            this.toolStripButtonOrder.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonOrder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOrder.Margin = new System.Windows.Forms.Padding(0, 1, 10, 2);
            this.toolStripButtonOrder.Name = "toolStripButtonOrder";
            this.toolStripButtonOrder.Size = new System.Drawing.Size(68, 68);
            this.toolStripButtonOrder.ToolTipText = "Pedidos";
            this.toolStripButtonOrder.Click += new System.EventHandler(this.toolStripButtonOrder_Click);
            // 
            // toolStripButtonPlan
            // 
            this.toolStripButtonPlan.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonPlan.Image = global::PlannerPro.GUI.Properties.Resources.PlanIcon;
            this.toolStripButtonPlan.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonPlan.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPlan.Margin = new System.Windows.Forms.Padding(5, 1, 15, 2);
            this.toolStripButtonPlan.Name = "toolStripButtonPlan";
            this.toolStripButtonPlan.Size = new System.Drawing.Size(68, 68);
            this.toolStripButtonPlan.ToolTipText = "Planes de producción";
            this.toolStripButtonPlan.Click += new System.EventHandler(this.toolStripButtonPlan_Click);
            // 
            // toolStripButtonPlanning
            // 
            this.toolStripButtonPlanning.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonPlanning.Image = global::PlannerPro.GUI.Properties.Resources.planningIcon;
            this.toolStripButtonPlanning.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonPlanning.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPlanning.Name = "toolStripButtonPlanning";
            this.toolStripButtonPlanning.Size = new System.Drawing.Size(68, 68);
            this.toolStripButtonPlanning.ToolTipText = "Planificación";
            this.toolStripButtonPlanning.Click += new System.EventHandler(this.toolStripButtonPlanning_Click);
            // 
            // toolStripButtonHistory
            // 
            this.toolStripButtonHistory.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonHistory.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHistory.Image")));
            this.toolStripButtonHistory.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonHistory.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHistory.Margin = new System.Windows.Forms.Padding(5, 1, 15, 2);
            this.toolStripButtonHistory.Name = "toolStripButtonHistory";
            this.toolStripButtonHistory.Size = new System.Drawing.Size(68, 68);
            this.toolStripButtonHistory.Text = "toolStripButton1";
            this.toolStripButtonHistory.ToolTipText = "Historial";
            this.toolStripButtonHistory.Click += new System.EventHandler(this.toolStripButtonHistory_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 71);
            // 
            // toolStripButtonAbout
            // 
            this.toolStripButtonAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonAbout.Image = global::PlannerPro.GUI.Properties.Resources.AboutIcon;
            this.toolStripButtonAbout.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAbout.Name = "toolStripButtonAbout";
            this.toolStripButtonAbout.Size = new System.Drawing.Size(68, 68);
            this.toolStripButtonAbout.ToolTipText = "Acerca de";
            this.toolStripButtonAbout.Click += new System.EventHandler(this.toolStripButtonAbout_Click);
            // 
            // tabControlScreens
            // 
            this.tabControlScreens.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControlScreens.Controls.Add(this.tabPageProduct);
            this.tabControlScreens.Controls.Add(this.tabPageClient);
            this.tabControlScreens.Controls.Add(this.tabPageOrder);
            this.tabControlScreens.Controls.Add(this.tabPagePlan);
            this.tabControlScreens.Controls.Add(this.tabPageHistory);
            this.tabControlScreens.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlScreens.ItemSize = new System.Drawing.Size(0, 1);
            this.tabControlScreens.Location = new System.Drawing.Point(0, 71);
            this.tabControlScreens.Name = "tabControlScreens";
            this.tabControlScreens.SelectedIndex = 0;
            this.tabControlScreens.Size = new System.Drawing.Size(929, 370);
            this.tabControlScreens.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControlScreens.TabIndex = 1;
            // 
            // tabPageProduct
            // 
            this.tabPageProduct.Controls.Add(this.productScreen);
            this.tabPageProduct.Location = new System.Drawing.Point(4, 5);
            this.tabPageProduct.Name = "tabPageProduct";
            this.tabPageProduct.Size = new System.Drawing.Size(921, 361);
            this.tabPageProduct.TabIndex = 1;
            this.tabPageProduct.UseVisualStyleBackColor = true;
            // 
            // productScreen
            // 
            this.productScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.productScreen.Location = new System.Drawing.Point(0, 0);
            this.productScreen.Name = "productScreen";
            this.productScreen.Padding = new System.Windows.Forms.Padding(10);
            this.productScreen.Size = new System.Drawing.Size(921, 361);
            this.productScreen.TabIndex = 0;
            // 
            // tabPageClient
            // 
            this.tabPageClient.Controls.Add(this.clientScreen);
            this.tabPageClient.Location = new System.Drawing.Point(4, 5);
            this.tabPageClient.Name = "tabPageClient";
            this.tabPageClient.Size = new System.Drawing.Size(921, 361);
            this.tabPageClient.TabIndex = 0;
            this.tabPageClient.UseVisualStyleBackColor = true;
            // 
            // clientScreen
            // 
            this.clientScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clientScreen.Location = new System.Drawing.Point(0, 0);
            this.clientScreen.Name = "clientScreen";
            this.clientScreen.Padding = new System.Windows.Forms.Padding(10);
            this.clientScreen.Size = new System.Drawing.Size(921, 361);
            this.clientScreen.TabIndex = 0;
            // 
            // tabPageOrder
            // 
            this.tabPageOrder.Controls.Add(this.orderScreen);
            this.tabPageOrder.Location = new System.Drawing.Point(4, 5);
            this.tabPageOrder.Name = "tabPageOrder";
            this.tabPageOrder.Size = new System.Drawing.Size(921, 361);
            this.tabPageOrder.TabIndex = 2;
            this.tabPageOrder.UseVisualStyleBackColor = true;
            // 
            // orderScreen
            // 
            this.orderScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.orderScreen.Location = new System.Drawing.Point(0, 0);
            this.orderScreen.Name = "orderScreen";
            this.orderScreen.Padding = new System.Windows.Forms.Padding(10);
            this.orderScreen.Size = new System.Drawing.Size(921, 361);
            this.orderScreen.TabIndex = 0;
            // 
            // tabPagePlan
            // 
            this.tabPagePlan.Controls.Add(this.planScreen);
            this.tabPagePlan.Location = new System.Drawing.Point(4, 5);
            this.tabPagePlan.Name = "tabPagePlan";
            this.tabPagePlan.Size = new System.Drawing.Size(921, 361);
            this.tabPagePlan.TabIndex = 3;
            this.tabPagePlan.UseVisualStyleBackColor = true;
            // 
            // planScreen
            // 
            this.planScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.planScreen.Location = new System.Drawing.Point(0, 0);
            this.planScreen.Name = "planScreen";
            this.planScreen.Padding = new System.Windows.Forms.Padding(10);
            this.planScreen.Size = new System.Drawing.Size(921, 361);
            this.planScreen.TabIndex = 0;
            // 
            // tabPageHistory
            // 
            this.tabPageHistory.Controls.Add(this.historyScreen);
            this.tabPageHistory.Location = new System.Drawing.Point(4, 5);
            this.tabPageHistory.Name = "tabPageHistory";
            this.tabPageHistory.Size = new System.Drawing.Size(921, 361);
            this.tabPageHistory.TabIndex = 4;
            this.tabPageHistory.UseVisualStyleBackColor = true;
            // 
            // historyScreen
            // 
            this.historyScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.historyScreen.Location = new System.Drawing.Point(0, 0);
            this.historyScreen.Name = "historyScreen";
            this.historyScreen.Padding = new System.Windows.Forms.Padding(10);
            this.historyScreen.Size = new System.Drawing.Size(921, 361);
            this.historyScreen.TabIndex = 0;
            // 
            // MainScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 441);
            this.Controls.Add(this.tabControlScreens);
            this.Controls.Add(this.toolStripOptions);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PlannerPro";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainScreen_FormClosing);
            this.toolStripOptions.ResumeLayout(false);
            this.toolStripOptions.PerformLayout();
            this.tabControlScreens.ResumeLayout(false);
            this.tabPageProduct.ResumeLayout(false);
            this.tabPageClient.ResumeLayout(false);
            this.tabPageOrder.ResumeLayout(false);
            this.tabPagePlan.ResumeLayout(false);
            this.tabPageHistory.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripOptions;
        private System.Windows.Forms.ToolStripButton toolStripButtonClient;
        private System.Windows.Forms.ToolStripButton toolStripButtonProduct;
        private System.Windows.Forms.ToolStripButton toolStripButtonOrder;
        private System.Windows.Forms.ToolStripButton toolStripButtonPlan;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton toolStripButtonAbout;
        private System.Windows.Forms.TabControl tabControlScreens;
        private System.Windows.Forms.TabPage tabPageClient;
        private System.Windows.Forms.TabPage tabPageProduct;
        private System.Windows.Forms.TabPage tabPageOrder;
        private System.Windows.Forms.TabPage tabPagePlan;
        private System.Windows.Forms.TabPage tabPageHistory;
        private System.Windows.Forms.ToolStripButton toolStripButtonHistory;
        private System.Windows.Forms.ToolStripButton toolStripButtonPlanning;
        private ProductScreen productScreen;
        private ClientScreen clientScreen;
        private OrderScreen orderScreen;
        private PlanScreen planScreen;
        private HistoryScreen historyScreen;
    }
}