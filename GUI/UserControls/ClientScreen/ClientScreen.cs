﻿using PlannerPro.Entities.API;
using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Logic.API;
using PlannerPro.Logic.LogicImp;
using PlannerPro.Utilities;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace PlannerPro.GUI.UserControls.ClientScreen
{
    public partial class ClientScreen : UserControl
    {
        private IClientLogic clientLogic = null;
        private BindingSource clientBindingSource;

        public ClientScreen()
        {
            InitializeComponent();
            clientLogic = ClientLogic.GetInstance();
            clientBindingSource = new BindingSource();
            if (!(DesignMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime))
                RefreshData();
        }

        private void TryClientRegistration()
        {
            clientLogic.RegisterClient(CreateClientFromUserInput());
            RefreshData();
            ResetControls();
            DialogBox.DisplayInfo("Clientes", "El cliente se ha dado de alta.");
        }

        private Client CreateClientFromUserInput()
        {
            return new RealClient(textBoxClientName.Text, trackBarClientPreference.Value);
        }

        public void RefreshData()
        {
            var clientList = clientLogic.GetAllRegisteredClients().ToList();
            clientBindingSource.DataSource = clientList;
            dataGridRegisteredClients.DataSource = clientBindingSource;
        }

        private void ResetControls()
        {
            textBoxClientName.Clear();
            trackBarClientPreference.Value = 1;
        }

        private void TryClientUnregistration()
        {
            Client selectedClient  = GetSelectedClientFromDataGrid();

            if (selectedClient != Client.NULL) {
                clientLogic.UnregisterClient(selectedClient);
                RefreshData();
                DialogBox.DisplayInfo("Clientes", "El cliente se ha dado de baja.");
            } else
                DialogBox.DisplayError("Clientes", "No hay cliente seleccionado.");
        }

        private Client GetSelectedClientFromDataGrid()
        {
            return (dataGridRegisteredClients.Rows.Count > 0 && dataGridRegisteredClients.SelectedRows.Count > 0) ?
                (Client)dataGridRegisteredClients.CurrentRow.DataBoundItem
              : Client.NULL;
        }

        private void TryClientUpdate()
        {
            Client selectedClient = GetSelectedClientFromDataGrid();

            if (selectedClient != Client.NULL) {
                clientLogic.UpdateClient(selectedClient, CreateClientFromUserInput());
                RefreshData();
                DialogBox.DisplayInfo("Clientes", "El cliente se ha modificado.");
            } else
                DialogBox.DisplayError("Clientes", "No hay cliente seleccionado.");
        }

        private void LoadSelectedClientData()
        {
            Client selectedClient = GetSelectedClientFromDataGrid();

            if (selectedClient != Client.NULL) {
                textBoxClientName.Text = selectedClient.Name;
                trackBarClientPreference.Value = selectedClient.Preference;
            }
        }
    }
}
