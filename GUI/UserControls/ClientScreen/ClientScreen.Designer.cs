﻿namespace PlannerPro.GUI.UserControls.ClientScreen
{
    partial class ClientScreen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxClients = new System.Windows.Forms.GroupBox();
            this.dataGridRegisteredClients = new System.Windows.Forms.DataGridView();
            this.clientsNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clientsPreferenceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.trackBarClientPreference = new System.Windows.Forms.TrackBar();
            this.labelRegClients = new System.Windows.Forms.Label();
            this.textBoxClientName = new System.Windows.Forms.TextBox();
            this.buttonClientUpdate = new System.Windows.Forms.Button();
            this.buttonClientUnregister = new System.Windows.Forms.Button();
            this.buttonClientRegister = new System.Windows.Forms.Button();
            this.labelClientPreference = new System.Windows.Forms.Label();
            this.labelClientName = new System.Windows.Forms.Label();
            this.groupBoxClients.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRegisteredClients)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarClientPreference)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxClients
            // 
            this.groupBoxClients.Controls.Add(this.dataGridRegisteredClients);
            this.groupBoxClients.Controls.Add(this.trackBarClientPreference);
            this.groupBoxClients.Controls.Add(this.labelRegClients);
            this.groupBoxClients.Controls.Add(this.textBoxClientName);
            this.groupBoxClients.Controls.Add(this.buttonClientUpdate);
            this.groupBoxClients.Controls.Add(this.buttonClientUnregister);
            this.groupBoxClients.Controls.Add(this.buttonClientRegister);
            this.groupBoxClients.Controls.Add(this.labelClientPreference);
            this.groupBoxClients.Controls.Add(this.labelClientName);
            this.groupBoxClients.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxClients.Location = new System.Drawing.Point(10, 10);
            this.groupBoxClients.Name = "groupBoxClients";
            this.groupBoxClients.Padding = new System.Windows.Forms.Padding(20);
            this.groupBoxClients.Size = new System.Drawing.Size(900, 340);
            this.groupBoxClients.TabIndex = 0;
            this.groupBoxClients.TabStop = false;
            this.groupBoxClients.Text = "Clientes";
            // 
            // dataGridRegisteredClients
            // 
            this.dataGridRegisteredClients.AllowUserToAddRows = false;
            this.dataGridRegisteredClients.AllowUserToDeleteRows = false;
            this.dataGridRegisteredClients.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridRegisteredClients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridRegisteredClients.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clientsNameColumn,
            this.clientsPreferenceColumn});
            this.dataGridRegisteredClients.Location = new System.Drawing.Point(437, 49);
            this.dataGridRegisteredClients.MultiSelect = false;
            this.dataGridRegisteredClients.Name = "dataGridRegisteredClients";
            this.dataGridRegisteredClients.ReadOnly = true;
            this.dataGridRegisteredClients.RowHeadersVisible = false;
            this.dataGridRegisteredClients.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridRegisteredClients.Size = new System.Drawing.Size(440, 268);
            this.dataGridRegisteredClients.TabIndex = 10;
            this.dataGridRegisteredClients.SelectionChanged += new System.EventHandler(this.dataGridRegisteredClients_SelectionChanged);
            // 
            // clientsNameColumn
            // 
            this.clientsNameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clientsNameColumn.DataPropertyName = "Name";
            this.clientsNameColumn.HeaderText = "Nombre";
            this.clientsNameColumn.MinimumWidth = 220;
            this.clientsNameColumn.Name = "clientsNameColumn";
            this.clientsNameColumn.ReadOnly = true;
            // 
            // clientsPreferenceColumn
            // 
            this.clientsPreferenceColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clientsPreferenceColumn.DataPropertyName = "Preference";
            this.clientsPreferenceColumn.HeaderText = "Preferencia";
            this.clientsPreferenceColumn.MinimumWidth = 215;
            this.clientsPreferenceColumn.Name = "clientsPreferenceColumn";
            this.clientsPreferenceColumn.ReadOnly = true;
            // 
            // trackBarClientPreference
            // 
            this.trackBarClientPreference.Location = new System.Drawing.Point(170, 102);
            this.trackBarClientPreference.Margin = new System.Windows.Forms.Padding(3, 3, 3, 30);
            this.trackBarClientPreference.Minimum = 1;
            this.trackBarClientPreference.Name = "trackBarClientPreference";
            this.trackBarClientPreference.Size = new System.Drawing.Size(180, 45);
            this.trackBarClientPreference.TabIndex = 9;
            this.trackBarClientPreference.Value = 1;
            // 
            // labelRegClients
            // 
            this.labelRegClients.AutoSize = true;
            this.labelRegClients.Location = new System.Drawing.Point(434, 33);
            this.labelRegClients.Name = "labelRegClients";
            this.labelRegClients.Size = new System.Drawing.Size(101, 13);
            this.labelRegClients.TabIndex = 7;
            this.labelRegClients.Text = "Clientes registrados:";
            // 
            // textBoxClientName
            // 
            this.textBoxClientName.Location = new System.Drawing.Point(170, 49);
            this.textBoxClientName.Margin = new System.Windows.Forms.Padding(3, 3, 3, 30);
            this.textBoxClientName.MaxLength = 100;
            this.textBoxClientName.Name = "textBoxClientName";
            this.textBoxClientName.Size = new System.Drawing.Size(180, 20);
            this.textBoxClientName.TabIndex = 5;
            // 
            // buttonClientUpdate
            // 
            this.buttonClientUpdate.Location = new System.Drawing.Point(280, 294);
            this.buttonClientUpdate.Name = "buttonClientUpdate";
            this.buttonClientUpdate.Size = new System.Drawing.Size(105, 23);
            this.buttonClientUpdate.TabIndex = 4;
            this.buttonClientUpdate.Text = "Modificar";
            this.buttonClientUpdate.UseVisualStyleBackColor = true;
            this.buttonClientUpdate.Click += new System.EventHandler(this.buttonClientUpdate_Click);
            // 
            // buttonClientUnregister
            // 
            this.buttonClientUnregister.Location = new System.Drawing.Point(150, 294);
            this.buttonClientUnregister.Name = "buttonClientUnregister";
            this.buttonClientUnregister.Size = new System.Drawing.Size(105, 23);
            this.buttonClientUnregister.TabIndex = 3;
            this.buttonClientUnregister.Text = "Baja";
            this.buttonClientUnregister.UseVisualStyleBackColor = true;
            this.buttonClientUnregister.Click += new System.EventHandler(this.buttonClientUnregister_Click);
            // 
            // buttonClientRegister
            // 
            this.buttonClientRegister.Location = new System.Drawing.Point(23, 294);
            this.buttonClientRegister.Name = "buttonClientRegister";
            this.buttonClientRegister.Size = new System.Drawing.Size(105, 23);
            this.buttonClientRegister.TabIndex = 2;
            this.buttonClientRegister.Text = "Alta";
            this.buttonClientRegister.UseVisualStyleBackColor = true;
            this.buttonClientRegister.Click += new System.EventHandler(this.buttonClientRegister_Click);
            // 
            // labelClientPreference
            // 
            this.labelClientPreference.AutoSize = true;
            this.labelClientPreference.Location = new System.Drawing.Point(100, 106);
            this.labelClientPreference.Name = "labelClientPreference";
            this.labelClientPreference.Size = new System.Drawing.Size(64, 13);
            this.labelClientPreference.TabIndex = 1;
            this.labelClientPreference.Text = "Preferencia:";
            // 
            // labelClientName
            // 
            this.labelClientName.AutoSize = true;
            this.labelClientName.Location = new System.Drawing.Point(117, 52);
            this.labelClientName.Name = "labelClientName";
            this.labelClientName.Size = new System.Drawing.Size(47, 13);
            this.labelClientName.TabIndex = 0;
            this.labelClientName.Text = "Nombre:";
            // 
            // ClientScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxClients);
            this.Name = "ClientScreen";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Size = new System.Drawing.Size(920, 360);
            this.groupBoxClients.ResumeLayout(false);
            this.groupBoxClients.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRegisteredClients)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarClientPreference)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxClients;
        private System.Windows.Forms.Label labelRegClients;
        private System.Windows.Forms.TextBox textBoxClientName;
        private System.Windows.Forms.Button buttonClientUpdate;
        private System.Windows.Forms.Button buttonClientUnregister;
        private System.Windows.Forms.Button buttonClientRegister;
        private System.Windows.Forms.Label labelClientPreference;
        private System.Windows.Forms.Label labelClientName;
        private System.Windows.Forms.TrackBar trackBarClientPreference;
        private System.Windows.Forms.DataGridView dataGridRegisteredClients;
        private System.Windows.Forms.DataGridViewTextBoxColumn clientsNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn clientsPreferenceColumn;
    }
}
