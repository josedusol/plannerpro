﻿using PlannerPro.Entities.Exceptions;
using PlannerPro.Utilities;
using System;

namespace PlannerPro.GUI.UserControls.ClientScreen
{
    public partial class ClientScreen
    {
        private void buttonClientUnregister_Click(object sender, EventArgs e)
        {
            try {
                TryClientUnregistration();
            } catch (ClientException ex) {
                DialogBox.DisplayError("Clientes", ex.Message);
            }
        }

        private void buttonClientRegister_Click(object sender, EventArgs e)
        {
            try {
                TryClientRegistration();
            } catch (ClientException ex) {
                DialogBox.DisplayError("Clientes", ex.Message);
            }
        }

        private void buttonClientUpdate_Click(object sender, EventArgs e)
        {
            try {
                TryClientUpdate();
            } catch (ClientException ex) {
                DialogBox.DisplayError("Clientes", ex.Message);
            }
        }

        private void dataGridRegisteredClients_SelectionChanged(object sender, EventArgs e)
        {
            LoadSelectedClientData();
        }
    }
}
