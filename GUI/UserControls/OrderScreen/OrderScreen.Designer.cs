﻿namespace PlannerPro.GUI.UserControls.OrderScreen
{
    partial class OrderScreen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxOrders = new System.Windows.Forms.GroupBox();
            this.labelRegisteredOrders = new System.Windows.Forms.Label();
            this.dataGridRegisteredOrders = new System.Windows.Forms.DataGridView();
            this.dataGridClientColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridProductColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridQuantityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridDeliveryDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridStatusColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateTimePickerOrderDeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.numUpDownOrderQuantity = new System.Windows.Forms.NumericUpDown();
            this.comboBoxOrderClient = new System.Windows.Forms.ComboBox();
            this.comboBoxOrderProduct = new System.Windows.Forms.ComboBox();
            this.buttonOrderUpdate = new System.Windows.Forms.Button();
            this.buttonOrderUnregister = new System.Windows.Forms.Button();
            this.buttonOrderRegister = new System.Windows.Forms.Button();
            this.labelOrderDeliveryDate = new System.Windows.Forms.Label();
            this.labelOrderQuantity = new System.Windows.Forms.Label();
            this.labelOrderClient = new System.Windows.Forms.Label();
            this.labelOrderProduct = new System.Windows.Forms.Label();
            this.groupBoxOrders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRegisteredOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownOrderQuantity)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxOrders
            // 
            this.groupBoxOrders.Controls.Add(this.labelRegisteredOrders);
            this.groupBoxOrders.Controls.Add(this.dataGridRegisteredOrders);
            this.groupBoxOrders.Controls.Add(this.dateTimePickerOrderDeliveryDate);
            this.groupBoxOrders.Controls.Add(this.numUpDownOrderQuantity);
            this.groupBoxOrders.Controls.Add(this.comboBoxOrderClient);
            this.groupBoxOrders.Controls.Add(this.comboBoxOrderProduct);
            this.groupBoxOrders.Controls.Add(this.buttonOrderUpdate);
            this.groupBoxOrders.Controls.Add(this.buttonOrderUnregister);
            this.groupBoxOrders.Controls.Add(this.buttonOrderRegister);
            this.groupBoxOrders.Controls.Add(this.labelOrderDeliveryDate);
            this.groupBoxOrders.Controls.Add(this.labelOrderQuantity);
            this.groupBoxOrders.Controls.Add(this.labelOrderClient);
            this.groupBoxOrders.Controls.Add(this.labelOrderProduct);
            this.groupBoxOrders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxOrders.Location = new System.Drawing.Point(10, 10);
            this.groupBoxOrders.Name = "groupBoxOrders";
            this.groupBoxOrders.Padding = new System.Windows.Forms.Padding(20);
            this.groupBoxOrders.Size = new System.Drawing.Size(900, 340);
            this.groupBoxOrders.TabIndex = 0;
            this.groupBoxOrders.TabStop = false;
            this.groupBoxOrders.Text = "Pedidos";
            // 
            // labelRegisteredOrders
            // 
            this.labelRegisteredOrders.AutoSize = true;
            this.labelRegisteredOrders.Location = new System.Drawing.Point(434, 33);
            this.labelRegisteredOrders.Name = "labelRegisteredOrders";
            this.labelRegisteredOrders.Size = new System.Drawing.Size(102, 13);
            this.labelRegisteredOrders.TabIndex = 12;
            this.labelRegisteredOrders.Text = "Pedidos registrados:";
            // 
            // dataGridRegisteredOrders
            // 
            this.dataGridRegisteredOrders.AllowUserToAddRows = false;
            this.dataGridRegisteredOrders.AllowUserToDeleteRows = false;
            this.dataGridRegisteredOrders.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridRegisteredOrders.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dataGridRegisteredOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridRegisteredOrders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridClientColumn,
            this.dataGridProductColumn,
            this.dataGridQuantityColumn,
            this.dataGridDeliveryDateColumn,
            this.dataGridStatusColumn});
            this.dataGridRegisteredOrders.Location = new System.Drawing.Point(437, 49);
            this.dataGridRegisteredOrders.MultiSelect = false;
            this.dataGridRegisteredOrders.Name = "dataGridRegisteredOrders";
            this.dataGridRegisteredOrders.ReadOnly = true;
            this.dataGridRegisteredOrders.RowHeadersVisible = false;
            this.dataGridRegisteredOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridRegisteredOrders.Size = new System.Drawing.Size(440, 268);
            this.dataGridRegisteredOrders.TabIndex = 11;
            this.dataGridRegisteredOrders.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridRegisteredOrders_CellFormatting);
            this.dataGridRegisteredOrders.SelectionChanged += new System.EventHandler(this.dataGridRegisteredOrders_SelectionChanged);
            // 
            // dataGridClientColumn
            // 
            this.dataGridClientColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridClientColumn.DataPropertyName = "Client";
            this.dataGridClientColumn.HeaderText = "Cliente";
            this.dataGridClientColumn.MinimumWidth = 80;
            this.dataGridClientColumn.Name = "dataGridClientColumn";
            this.dataGridClientColumn.ReadOnly = true;
            // 
            // dataGridProductColumn
            // 
            this.dataGridProductColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridProductColumn.DataPropertyName = "Product";
            this.dataGridProductColumn.HeaderText = "Producto";
            this.dataGridProductColumn.MinimumWidth = 80;
            this.dataGridProductColumn.Name = "dataGridProductColumn";
            this.dataGridProductColumn.ReadOnly = true;
            // 
            // dataGridQuantityColumn
            // 
            this.dataGridQuantityColumn.DataPropertyName = "Quantity";
            this.dataGridQuantityColumn.HeaderText = "Cantidad";
            this.dataGridQuantityColumn.MinimumWidth = 60;
            this.dataGridQuantityColumn.Name = "dataGridQuantityColumn";
            this.dataGridQuantityColumn.ReadOnly = true;
            this.dataGridQuantityColumn.Width = 60;
            // 
            // dataGridDeliveryDateColumn
            // 
            this.dataGridDeliveryDateColumn.DataPropertyName = "DeliveryDate";
            this.dataGridDeliveryDateColumn.HeaderText = "Fecha de entrega";
            this.dataGridDeliveryDateColumn.MinimumWidth = 115;
            this.dataGridDeliveryDateColumn.Name = "dataGridDeliveryDateColumn";
            this.dataGridDeliveryDateColumn.ReadOnly = true;
            this.dataGridDeliveryDateColumn.Width = 115;
            // 
            // dataGridStatusColumn
            // 
            this.dataGridStatusColumn.DataPropertyName = "Status";
            this.dataGridStatusColumn.HeaderText = "Estado";
            this.dataGridStatusColumn.MinimumWidth = 80;
            this.dataGridStatusColumn.Name = "dataGridStatusColumn";
            this.dataGridStatusColumn.ReadOnly = true;
            this.dataGridStatusColumn.Width = 80;
            // 
            // dateTimePickerOrderDeliveryDate
            // 
            this.dateTimePickerOrderDeliveryDate.CustomFormat = "dd/MM/yyyy hh:mm";
            this.dateTimePickerOrderDeliveryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerOrderDeliveryDate.Location = new System.Drawing.Point(170, 208);
            this.dateTimePickerOrderDeliveryDate.Name = "dateTimePickerOrderDeliveryDate";
            this.dateTimePickerOrderDeliveryDate.Size = new System.Drawing.Size(180, 20);
            this.dateTimePickerOrderDeliveryDate.TabIndex = 10;
            // 
            // numUpDownOrderQuantity
            // 
            this.numUpDownOrderQuantity.Location = new System.Drawing.Point(170, 157);
            this.numUpDownOrderQuantity.Margin = new System.Windows.Forms.Padding(3, 3, 3, 30);
            this.numUpDownOrderQuantity.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numUpDownOrderQuantity.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUpDownOrderQuantity.Name = "numUpDownOrderQuantity";
            this.numUpDownOrderQuantity.Size = new System.Drawing.Size(180, 20);
            this.numUpDownOrderQuantity.TabIndex = 9;
            this.numUpDownOrderQuantity.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // comboBoxOrderClient
            // 
            this.comboBoxOrderClient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOrderClient.FormattingEnabled = true;
            this.comboBoxOrderClient.Location = new System.Drawing.Point(170, 49);
            this.comboBoxOrderClient.Margin = new System.Windows.Forms.Padding(3, 3, 3, 30);
            this.comboBoxOrderClient.Name = "comboBoxOrderClient";
            this.comboBoxOrderClient.Size = new System.Drawing.Size(180, 21);
            this.comboBoxOrderClient.TabIndex = 8;
            // 
            // comboBoxOrderProduct
            // 
            this.comboBoxOrderProduct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOrderProduct.FormattingEnabled = true;
            this.comboBoxOrderProduct.Location = new System.Drawing.Point(170, 102);
            this.comboBoxOrderProduct.Margin = new System.Windows.Forms.Padding(3, 3, 3, 30);
            this.comboBoxOrderProduct.Name = "comboBoxOrderProduct";
            this.comboBoxOrderProduct.Size = new System.Drawing.Size(180, 21);
            this.comboBoxOrderProduct.TabIndex = 7;
            // 
            // buttonOrderUpdate
            // 
            this.buttonOrderUpdate.Location = new System.Drawing.Point(280, 294);
            this.buttonOrderUpdate.Name = "buttonOrderUpdate";
            this.buttonOrderUpdate.Size = new System.Drawing.Size(105, 23);
            this.buttonOrderUpdate.TabIndex = 6;
            this.buttonOrderUpdate.Text = "Modificar";
            this.buttonOrderUpdate.UseVisualStyleBackColor = true;
            this.buttonOrderUpdate.Click += new System.EventHandler(this.buttonOrderUpdate_Click);
            // 
            // buttonOrderUnregister
            // 
            this.buttonOrderUnregister.Location = new System.Drawing.Point(150, 294);
            this.buttonOrderUnregister.Name = "buttonOrderUnregister";
            this.buttonOrderUnregister.Size = new System.Drawing.Size(105, 23);
            this.buttonOrderUnregister.TabIndex = 5;
            this.buttonOrderUnregister.Text = "Baja";
            this.buttonOrderUnregister.UseVisualStyleBackColor = true;
            this.buttonOrderUnregister.Click += new System.EventHandler(this.buttonOrderUnregister_Click);
            // 
            // buttonOrderRegister
            // 
            this.buttonOrderRegister.Location = new System.Drawing.Point(23, 294);
            this.buttonOrderRegister.Name = "buttonOrderRegister";
            this.buttonOrderRegister.Size = new System.Drawing.Size(105, 23);
            this.buttonOrderRegister.TabIndex = 4;
            this.buttonOrderRegister.Text = "Alta";
            this.buttonOrderRegister.UseVisualStyleBackColor = true;
            this.buttonOrderRegister.Click += new System.EventHandler(this.buttonOrderRegister_Click);
            // 
            // labelOrderDeliveryDate
            // 
            this.labelOrderDeliveryDate.AutoSize = true;
            this.labelOrderDeliveryDate.Location = new System.Drawing.Point(70, 210);
            this.labelOrderDeliveryDate.Name = "labelOrderDeliveryDate";
            this.labelOrderDeliveryDate.Size = new System.Drawing.Size(94, 13);
            this.labelOrderDeliveryDate.TabIndex = 3;
            this.labelOrderDeliveryDate.Text = "Fecha de entrega:";
            // 
            // labelOrderQuantity
            // 
            this.labelOrderQuantity.AutoSize = true;
            this.labelOrderQuantity.Location = new System.Drawing.Point(112, 159);
            this.labelOrderQuantity.Name = "labelOrderQuantity";
            this.labelOrderQuantity.Size = new System.Drawing.Size(52, 13);
            this.labelOrderQuantity.TabIndex = 2;
            this.labelOrderQuantity.Text = "Cantidad:";
            // 
            // labelOrderClient
            // 
            this.labelOrderClient.AutoSize = true;
            this.labelOrderClient.Location = new System.Drawing.Point(122, 52);
            this.labelOrderClient.Name = "labelOrderClient";
            this.labelOrderClient.Size = new System.Drawing.Size(42, 13);
            this.labelOrderClient.TabIndex = 1;
            this.labelOrderClient.Text = "Cliente:";
            // 
            // labelOrderProduct
            // 
            this.labelOrderProduct.AutoSize = true;
            this.labelOrderProduct.Location = new System.Drawing.Point(111, 106);
            this.labelOrderProduct.Name = "labelOrderProduct";
            this.labelOrderProduct.Size = new System.Drawing.Size(53, 13);
            this.labelOrderProduct.TabIndex = 0;
            this.labelOrderProduct.Text = "Producto:";
            // 
            // OrderScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxOrders);
            this.Name = "OrderScreen";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Size = new System.Drawing.Size(920, 360);
            this.groupBoxOrders.ResumeLayout(false);
            this.groupBoxOrders.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRegisteredOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownOrderQuantity)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxOrders;
        private System.Windows.Forms.ComboBox comboBoxOrderClient;
        private System.Windows.Forms.ComboBox comboBoxOrderProduct;
        private System.Windows.Forms.Button buttonOrderUpdate;
        private System.Windows.Forms.Button buttonOrderUnregister;
        private System.Windows.Forms.Button buttonOrderRegister;
        private System.Windows.Forms.Label labelOrderDeliveryDate;
        private System.Windows.Forms.Label labelOrderQuantity;
        private System.Windows.Forms.Label labelOrderClient;
        private System.Windows.Forms.Label labelOrderProduct;
        private System.Windows.Forms.DateTimePicker dateTimePickerOrderDeliveryDate;
        private System.Windows.Forms.NumericUpDown numUpDownOrderQuantity;
        private System.Windows.Forms.Label labelRegisteredOrders;
        private System.Windows.Forms.DataGridView dataGridRegisteredOrders;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridClientColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridProductColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridQuantityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridDeliveryDateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridStatusColumn;
    }
}
