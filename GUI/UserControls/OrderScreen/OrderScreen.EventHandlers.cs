﻿using PlannerPro.Entities.API;
using PlannerPro.Entities.Exceptions;
using PlannerPro.Utilities;
using System;
using System.Windows.Forms;

namespace PlannerPro.GUI.UserControls.OrderScreen
{
    public partial class OrderScreen
    {
        private void buttonOrderRegister_Click(object sender, EventArgs e)
        {
            try {
                TryOrderRegistration();
            } catch (OrderException ex) {
                DialogBox.DisplayError("Pedidos", ex.Message);
            }
        }

        private void buttonOrderUnregister_Click(object sender, EventArgs e)
        {
            try {
                TryOrderUnregistration();
            } catch (OrderException ex) {
                DialogBox.DisplayError("Pedidos", ex.Message);
            }
        }

        private void buttonOrderUpdate_Click(object sender, EventArgs e)
        {
            try {
                TryOrderUpdate();
            } catch (OrderException ex) {
                DialogBox.DisplayError("Pedidos", ex.Message);
            }
        }

        private void dataGridRegisteredOrders_CellFormatting(object sender,
    DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 4) {
                e.Value = ((OrderStatus)e.Value).ToDescriptionString();
                e.FormattingApplied = true;
            }
        }

        private void dataGridRegisteredOrders_SelectionChanged(object sender, EventArgs e)
        {
            LoadSelectedOrderData();
        }
    }
}
