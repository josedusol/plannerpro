﻿using PlannerPro.Entities.API;
using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Logic.API;
using PlannerPro.Logic.LogicImp;
using PlannerPro.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace PlannerPro.GUI.UserControls.OrderScreen
{
    public partial class OrderScreen : UserControl
    {
        private IOrderLogic orderLogic = null;
        private IClientLogic clientLogic = null;
        private IProductLogic productLogic = null;
        private BindingSource orderBindingSource = null;

        public OrderScreen()
        {
            InitializeComponent();
            dataGridRegisteredOrders.AutoGenerateColumns = false;            
            orderLogic = OrderLogic.GetInstance();
            clientLogic = ClientLogic.GetInstance();
            productLogic = ProductLogic.GetInstance();
            orderBindingSource = new BindingSource();
            if (!(DesignMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime))
                RefreshData();
        }

        private void TryOrderRegistration()
        {
            orderLogic.RegisterOrder(CreateOrderFromUserInput());
            RefreshRegisteredOrders();
            ResetControls();
            DialogBox.DisplayInfo("Pedidos", "El pedido se ha dado de alta.");
        }

        private Order CreateOrderFromUserInput()
        {          
            return new RealOrder(GetSelectedClientFromComboBox(),
                    GetSelectedProductFromComboBox(),
                    (int)numUpDownOrderQuantity.Value,
                    (DateTime)dateTimePickerOrderDeliveryDate.Value);         
        }

        private Client GetSelectedClientFromComboBox()
        {
            Client orderClient = (Client)comboBoxOrderClient.SelectedItem;
            return orderClient != null ? orderClient: Client.NULL;
        }

        private Product GetSelectedProductFromComboBox()
        {
            Product orderProduct = (Product)comboBoxOrderProduct.SelectedItem;
            return orderProduct != null ? orderProduct: Product.NULL;
        }

        private void TryOrderUnregistration()
        {
            Order selectedOrder = GetSelectedOrderFromDataGrid();

            if (selectedOrder != Order.NULL) {
                orderLogic.UnregisterOrder(selectedOrder);
                RefreshRegisteredOrders();
                DialogBox.DisplayInfo("Pedidos", "El pedido se ha dado de baja.");
            } else
                DialogBox.DisplayError("Pedidos", "No hay pedido seleccionado.");
        }

        private void TryOrderUpdate()
        {
            Order selectedOrder = GetSelectedOrderFromDataGrid();

            if (selectedOrder != Order.NULL) {
                orderLogic.UpdateOrder(selectedOrder, CreateOrderFromUserInput());
                RefreshRegisteredOrders();
                DialogBox.DisplayInfo("Pedidos", "El pedido se ha modificado.");
            } else
                DialogBox.DisplayError("Pedidos", "No hay pedido seleccionado.");
        }

        public void RefreshData()
        {
            RefreshRegisteredClients();
            RefreshRegisteredProducts();
            RefreshRegisteredOrders();
        }

        private void RefreshRegisteredClients()
        {
            comboBoxOrderClient.DataSource = null;
            comboBoxOrderClient.DisplayMember = "Name";
            comboBoxOrderClient.ValueMember = "ClientID";
            comboBoxOrderClient.DataSource = clientLogic.GetAllRegisteredClients().ToList();
        }

        private void RefreshRegisteredProducts()
        {
            comboBoxOrderProduct.DataSource = null;
            comboBoxOrderProduct.DisplayMember = "Name";
            comboBoxOrderProduct.ValueMember = "ProductID";
            comboBoxOrderProduct.DataSource = productLogic.GetAllRegisteredProducts().ToList();
        }

        private void RefreshRegisteredOrders()
        {
            var orderList = GetNonFinalizedOrders(orderLogic.GetAllRegisteredOrders());
            orderBindingSource.DataSource = orderList;
            dataGridRegisteredOrders.DataSource = orderBindingSource;
        }

        private IList<Order> GetNonFinalizedOrders(IEnumerable<Order> orders)
        {
            return orders.Where(order => order.Status != OrderStatus.Finalized).ToList();
        }

        private Order GetSelectedOrderFromDataGrid()
        {
            return (dataGridRegisteredOrders.Rows.Count > 0 
                && dataGridRegisteredOrders.SelectedRows.Count > 0) ?
                (Order)dataGridRegisteredOrders.CurrentRow.DataBoundItem
              : Order.NULL;
        }

        private void ResetControls()
        {           
            comboBoxOrderClient.SelectedIndex = 0;
            comboBoxOrderProduct.SelectedIndex = 0;
            numUpDownOrderQuantity.Value = 1;
            dateTimePickerOrderDeliveryDate.Value = DateTime.Now;
        }

        private void LoadSelectedOrderData()
        {
            Order selectedOrder = GetSelectedOrderFromDataGrid();

            if (selectedOrder != Order.NULL) {
                comboBoxOrderClient.SelectedItem = selectedOrder.Client;
                comboBoxOrderProduct.SelectedItem = selectedOrder.Product;
                numUpDownOrderQuantity.Value = selectedOrder.Quantity;
                dateTimePickerOrderDeliveryDate.Value = selectedOrder.DeliveryDate;
            }
        }
    }
}