﻿using PlannerPro.Entities.Exceptions;
using PlannerPro.Utilities;
using System;

namespace PlannerPro.GUI.UserControls.ProductScreen
{
    public partial class ProductScreen
    {
        private void buttonProductRegister_Click(object sender, EventArgs e)
        {
            try {
                TryProductRegistration();
            } catch (ProductException ex) {
                DialogBox.DisplayError("Productos", ex.Message);
            }
        }

        private void buttonProductUnregister_Click(object sender, EventArgs e)
        {
            try {
                TryProductUnregistration();
            } catch (ProductException ex) {
                DialogBox.DisplayError("Productos", ex.Message);
            }
        }

        private void buttonProductUpdate_Click(object sender, EventArgs e)
        {
            try {
                TryProductUpdate();
            } catch (ProductException ex) {
                DialogBox.DisplayError("Productos", ex.Message);
            }
        }

        private void dataGridRegisteredProducts_SelectionChanged(object sender, EventArgs e)
        {
            LoadSelectedProductData();
        }
    }
}
