﻿using PlannerPro.Entities.API;
using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Entities.Exceptions;
using PlannerPro.Logic.API;
using PlannerPro.Logic.LogicImp;
using PlannerPro.Utilities;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace PlannerPro.GUI.UserControls.ProductScreen
{
    public partial class ProductScreen : UserControl
    {
        private IProductLogic productLogic = null;
        private BindingSource productbindingSource;

        public ProductScreen()
        {
            InitializeComponent();
            productLogic = ProductLogic.GetInstance();
            productbindingSource = new BindingSource();
            if (!(DesignMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime))
                RefreshData();   
        }

        private void TryProductRegistration()
        {
            productLogic.RegisterProduct(CreateProductFromUserInput());
            RefreshData();
            ResetControls();
            DialogBox.DisplayInfo("Clientes", "El producto se ha dado de alta.");
        }

        private Product CreateProductFromUserInput()
        {
            if (String.IsNullOrEmpty(numUpDownProductSalePrice.Text))
                throw new ProductException("El precio de venta no puede ser vacío.");              
            if (String.IsNullOrEmpty(numUpDownProductionCost.Text))
                throw new ProductException("El costo de producción no puede ser vacío.");
                
             return new RealProduct(textBoxProductName.Text,
                    (int) numUpDownProductManufacturingTime.Value,
                    (float) numUpDownProductSalePrice.Value,
                    (float) numUpDownProductionCost.Value);
        }

        public void RefreshData()
        {
            var productList = productLogic.GetAllRegisteredProducts().ToList();
            productbindingSource.DataSource = productList;
            dataGridRegisteredProducts.DataSource = productbindingSource;
        }

        private void ResetControls()
        {
            textBoxProductName.Clear();
            numUpDownProductManufacturingTime.Value = 1;
            numUpDownProductSalePrice.Value = 0;
            numUpDownProductionCost.Value = 0;
        }

        private void TryProductUnregistration()
        {
            Product selectedProduct = GetSelectedProductFromDataGrid();

            if (selectedProduct != Product.NULL) {
                productLogic.UnregisterProduct(selectedProduct);
                RefreshData();
                DialogBox.DisplayInfo("Productos", "El producto se ha dado de baja.");
            } else
                DialogBox.DisplayError("Productos", "No hay producto seleccionado.");
        }

        private Product GetSelectedProductFromDataGrid()
        {
            return (dataGridRegisteredProducts.Rows.Count > 0 
                && dataGridRegisteredProducts.SelectedRows.Count > 0) ?
                (Product)dataGridRegisteredProducts.CurrentRow.DataBoundItem
              : Product.NULL;
        }

        private void TryProductUpdate()
        {
            Product selectedProduct = GetSelectedProductFromDataGrid();

            if (selectedProduct != Product.NULL) {
                productLogic.UpdateProduct(selectedProduct, CreateProductFromUserInput());
                RefreshData();
                DialogBox.DisplayInfo("Productos", "El producto se ha modificado.");
            } else
                DialogBox.DisplayError("Productos", "No hay producto seleccionado.");
        }

        private void LoadSelectedProductData()
        {
            Product selectedProduct = GetSelectedProductFromDataGrid();

            if (selectedProduct != Product.NULL) {
                textBoxProductName.Text = selectedProduct.Name;
                numUpDownProductionCost.Value = Convert.ToDecimal(selectedProduct.ProductionCost);
                numUpDownProductSalePrice.Value = Convert.ToDecimal(selectedProduct.SalePrice);
                numUpDownProductManufacturingTime.Value = selectedProduct.ManufacturingTime;
            }
        }
    }
}
