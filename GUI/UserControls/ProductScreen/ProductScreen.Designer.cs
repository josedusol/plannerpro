﻿namespace PlannerPro.GUI.UserControls.ProductScreen
{
    partial class ProductScreen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxProducts = new System.Windows.Forms.GroupBox();
            this.dataGridRegisteredProducts = new System.Windows.Forms.DataGridView();
            this.productsNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productsSalePriceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productsManufacturingTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productsProductionCostColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelRegisteredProducts = new System.Windows.Forms.Label();
            this.buttonProductUpdate = new System.Windows.Forms.Button();
            this.buttonProductUnregister = new System.Windows.Forms.Button();
            this.numUpDownProductManufacturingTime = new System.Windows.Forms.NumericUpDown();
            this.numUpDownProductSalePrice = new System.Windows.Forms.NumericUpDown();
            this.numUpDownProductionCost = new System.Windows.Forms.NumericUpDown();
            this.textBoxProductName = new System.Windows.Forms.TextBox();
            this.labelProductManufacturingTime = new System.Windows.Forms.Label();
            this.labelProductSalePrice = new System.Windows.Forms.Label();
            this.labelProductionCost = new System.Windows.Forms.Label();
            this.labelProductName = new System.Windows.Forms.Label();
            this.buttonProductRegister = new System.Windows.Forms.Button();
            this.groupBoxProducts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRegisteredProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownProductManufacturingTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownProductSalePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownProductionCost)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxProducts
            // 
            this.groupBoxProducts.Controls.Add(this.dataGridRegisteredProducts);
            this.groupBoxProducts.Controls.Add(this.labelRegisteredProducts);
            this.groupBoxProducts.Controls.Add(this.buttonProductUpdate);
            this.groupBoxProducts.Controls.Add(this.buttonProductUnregister);
            this.groupBoxProducts.Controls.Add(this.numUpDownProductManufacturingTime);
            this.groupBoxProducts.Controls.Add(this.numUpDownProductSalePrice);
            this.groupBoxProducts.Controls.Add(this.numUpDownProductionCost);
            this.groupBoxProducts.Controls.Add(this.textBoxProductName);
            this.groupBoxProducts.Controls.Add(this.labelProductManufacturingTime);
            this.groupBoxProducts.Controls.Add(this.labelProductSalePrice);
            this.groupBoxProducts.Controls.Add(this.labelProductionCost);
            this.groupBoxProducts.Controls.Add(this.labelProductName);
            this.groupBoxProducts.Controls.Add(this.buttonProductRegister);
            this.groupBoxProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxProducts.Location = new System.Drawing.Point(10, 10);
            this.groupBoxProducts.Name = "groupBoxProducts";
            this.groupBoxProducts.Padding = new System.Windows.Forms.Padding(20);
            this.groupBoxProducts.Size = new System.Drawing.Size(900, 340);
            this.groupBoxProducts.TabIndex = 0;
            this.groupBoxProducts.TabStop = false;
            this.groupBoxProducts.Text = "Productos";
            // 
            // dataGridRegisteredProducts
            // 
            this.dataGridRegisteredProducts.AllowUserToAddRows = false;
            this.dataGridRegisteredProducts.AllowUserToDeleteRows = false;
            this.dataGridRegisteredProducts.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridRegisteredProducts.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dataGridRegisteredProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridRegisteredProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.productsNameColumn,
            this.productsSalePriceColumn,
            this.productsManufacturingTimeColumn,
            this.productsProductionCostColumn});
            this.dataGridRegisteredProducts.Location = new System.Drawing.Point(437, 49);
            this.dataGridRegisteredProducts.MultiSelect = false;
            this.dataGridRegisteredProducts.Name = "dataGridRegisteredProducts";
            this.dataGridRegisteredProducts.ReadOnly = true;
            this.dataGridRegisteredProducts.RowHeadersVisible = false;
            this.dataGridRegisteredProducts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridRegisteredProducts.Size = new System.Drawing.Size(440, 268);
            this.dataGridRegisteredProducts.TabIndex = 13;
            this.dataGridRegisteredProducts.SelectionChanged += new System.EventHandler(this.dataGridRegisteredProducts_SelectionChanged);
            // 
            // productsNameColumn
            // 
            this.productsNameColumn.DataPropertyName = "Name";
            this.productsNameColumn.HeaderText = "Nombre";
            this.productsNameColumn.MinimumWidth = 90;
            this.productsNameColumn.Name = "productsNameColumn";
            this.productsNameColumn.ReadOnly = true;
            this.productsNameColumn.Width = 90;
            // 
            // productsSalePriceColumn
            // 
            this.productsSalePriceColumn.DataPropertyName = "SalePrice";
            this.productsSalePriceColumn.HeaderText = "Precio de venta";
            this.productsSalePriceColumn.MinimumWidth = 105;
            this.productsSalePriceColumn.Name = "productsSalePriceColumn";
            this.productsSalePriceColumn.ReadOnly = true;
            this.productsSalePriceColumn.Width = 105;
            // 
            // productsManufacturingTimeColumn
            // 
            this.productsManufacturingTimeColumn.DataPropertyName = "ManufacturingTime";
            this.productsManufacturingTimeColumn.HeaderText = "Tiempo fabricación";
            this.productsManufacturingTimeColumn.MinimumWidth = 125;
            this.productsManufacturingTimeColumn.Name = "productsManufacturingTimeColumn";
            this.productsManufacturingTimeColumn.ReadOnly = true;
            this.productsManufacturingTimeColumn.Width = 125;
            // 
            // productsProductionCostColumn
            // 
            this.productsProductionCostColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.productsProductionCostColumn.DataPropertyName = "ProductionCost";
            this.productsProductionCostColumn.HeaderText = "Costo producción";
            this.productsProductionCostColumn.MinimumWidth = 115;
            this.productsProductionCostColumn.Name = "productsProductionCostColumn";
            this.productsProductionCostColumn.ReadOnly = true;
            // 
            // labelRegisteredProducts
            // 
            this.labelRegisteredProducts.AutoSize = true;
            this.labelRegisteredProducts.Location = new System.Drawing.Point(434, 33);
            this.labelRegisteredProducts.Name = "labelRegisteredProducts";
            this.labelRegisteredProducts.Size = new System.Drawing.Size(112, 13);
            this.labelRegisteredProducts.TabIndex = 12;
            this.labelRegisteredProducts.Text = "Productos registrados:";
            // 
            // buttonProductUpdate
            // 
            this.buttonProductUpdate.Location = new System.Drawing.Point(280, 294);
            this.buttonProductUpdate.Name = "buttonProductUpdate";
            this.buttonProductUpdate.Size = new System.Drawing.Size(105, 23);
            this.buttonProductUpdate.TabIndex = 10;
            this.buttonProductUpdate.Text = "Modificar";
            this.buttonProductUpdate.UseVisualStyleBackColor = true;
            this.buttonProductUpdate.Click += new System.EventHandler(this.buttonProductUpdate_Click);
            // 
            // buttonProductUnregister
            // 
            this.buttonProductUnregister.Location = new System.Drawing.Point(150, 294);
            this.buttonProductUnregister.Name = "buttonProductUnregister";
            this.buttonProductUnregister.Size = new System.Drawing.Size(105, 23);
            this.buttonProductUnregister.TabIndex = 9;
            this.buttonProductUnregister.Text = "Baja";
            this.buttonProductUnregister.UseVisualStyleBackColor = true;
            this.buttonProductUnregister.Click += new System.EventHandler(this.buttonProductUnregister_Click);
            // 
            // numUpDownProductManufacturingTime
            // 
            this.numUpDownProductManufacturingTime.Location = new System.Drawing.Point(170, 208);
            this.numUpDownProductManufacturingTime.Margin = new System.Windows.Forms.Padding(3, 3, 3, 30);
            this.numUpDownProductManufacturingTime.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numUpDownProductManufacturingTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUpDownProductManufacturingTime.Name = "numUpDownProductManufacturingTime";
            this.numUpDownProductManufacturingTime.Size = new System.Drawing.Size(180, 20);
            this.numUpDownProductManufacturingTime.TabIndex = 8;
            this.numUpDownProductManufacturingTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numUpDownProductSalePrice
            // 
            this.numUpDownProductSalePrice.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.numUpDownProductSalePrice.DecimalPlaces = 2;
            this.numUpDownProductSalePrice.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numUpDownProductSalePrice.Location = new System.Drawing.Point(170, 157);
            this.numUpDownProductSalePrice.Margin = new System.Windows.Forms.Padding(3, 3, 3, 30);
            this.numUpDownProductSalePrice.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numUpDownProductSalePrice.Name = "numUpDownProductSalePrice";
            this.numUpDownProductSalePrice.Size = new System.Drawing.Size(180, 20);
            this.numUpDownProductSalePrice.TabIndex = 7;
            // 
            // numUpDownProductionCost
            // 
            this.numUpDownProductionCost.DecimalPlaces = 2;
            this.numUpDownProductionCost.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numUpDownProductionCost.Location = new System.Drawing.Point(170, 102);
            this.numUpDownProductionCost.Margin = new System.Windows.Forms.Padding(3, 3, 3, 30);
            this.numUpDownProductionCost.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numUpDownProductionCost.Name = "numUpDownProductionCost";
            this.numUpDownProductionCost.Size = new System.Drawing.Size(180, 20);
            this.numUpDownProductionCost.TabIndex = 6;
            // 
            // textBoxProductName
            // 
            this.textBoxProductName.Location = new System.Drawing.Point(170, 49);
            this.textBoxProductName.Margin = new System.Windows.Forms.Padding(3, 3, 3, 30);
            this.textBoxProductName.MaxLength = 1000;
            this.textBoxProductName.Name = "textBoxProductName";
            this.textBoxProductName.Size = new System.Drawing.Size(180, 20);
            this.textBoxProductName.TabIndex = 5;
            // 
            // labelProductManufacturingTime
            // 
            this.labelProductManufacturingTime.AutoSize = true;
            this.labelProductManufacturingTime.Location = new System.Drawing.Point(23, 210);
            this.labelProductManufacturingTime.Name = "labelProductManufacturingTime";
            this.labelProductManufacturingTime.Size = new System.Drawing.Size(141, 13);
            this.labelProductManufacturingTime.TabIndex = 4;
            this.labelProductManufacturingTime.Text = "Tiempo de fabricación (c/u):";
            // 
            // labelProductSalePrice
            // 
            this.labelProductSalePrice.AutoSize = true;
            this.labelProductSalePrice.Location = new System.Drawing.Point(53, 159);
            this.labelProductSalePrice.Name = "labelProductSalePrice";
            this.labelProductSalePrice.Size = new System.Drawing.Size(111, 13);
            this.labelProductSalePrice.TabIndex = 3;
            this.labelProductSalePrice.Text = "Precio de venta (c/u):";
            // 
            // labelProductionCost
            // 
            this.labelProductionCost.AutoSize = true;
            this.labelProductionCost.Location = new System.Drawing.Point(30, 106);
            this.labelProductionCost.Name = "labelProductionCost";
            this.labelProductionCost.Size = new System.Drawing.Size(134, 13);
            this.labelProductionCost.TabIndex = 2;
            this.labelProductionCost.Text = "Costo de producción (c/u):";
            // 
            // labelProductName
            // 
            this.labelProductName.AutoSize = true;
            this.labelProductName.Location = new System.Drawing.Point(117, 52);
            this.labelProductName.Name = "labelProductName";
            this.labelProductName.Size = new System.Drawing.Size(47, 13);
            this.labelProductName.TabIndex = 1;
            this.labelProductName.Text = "Nombre:";
            // 
            // buttonProductRegister
            // 
            this.buttonProductRegister.Location = new System.Drawing.Point(23, 294);
            this.buttonProductRegister.Name = "buttonProductRegister";
            this.buttonProductRegister.Size = new System.Drawing.Size(105, 23);
            this.buttonProductRegister.TabIndex = 0;
            this.buttonProductRegister.Text = "Alta";
            this.buttonProductRegister.UseVisualStyleBackColor = true;
            this.buttonProductRegister.Click += new System.EventHandler(this.buttonProductRegister_Click);
            // 
            // ProductScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxProducts);
            this.Name = "ProductScreen";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Size = new System.Drawing.Size(920, 360);
            this.groupBoxProducts.ResumeLayout(false);
            this.groupBoxProducts.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRegisteredProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownProductManufacturingTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownProductSalePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownProductionCost)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxProducts;
        private System.Windows.Forms.Button buttonProductRegister;
        private System.Windows.Forms.Label labelProductName;
        private System.Windows.Forms.Button buttonProductUpdate;
        private System.Windows.Forms.Button buttonProductUnregister;
        private System.Windows.Forms.NumericUpDown numUpDownProductManufacturingTime;
        private System.Windows.Forms.NumericUpDown numUpDownProductSalePrice;
        private System.Windows.Forms.NumericUpDown numUpDownProductionCost;
        private System.Windows.Forms.TextBox textBoxProductName;
        private System.Windows.Forms.Label labelProductManufacturingTime;
        private System.Windows.Forms.Label labelProductSalePrice;
        private System.Windows.Forms.Label labelProductionCost;
        private System.Windows.Forms.Label labelRegisteredProducts;
        private System.Windows.Forms.DataGridView dataGridRegisteredProducts;
        private System.Windows.Forms.DataGridViewTextBoxColumn productsNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productsSalePriceColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productsManufacturingTimeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productsProductionCostColumn;

    }
}
