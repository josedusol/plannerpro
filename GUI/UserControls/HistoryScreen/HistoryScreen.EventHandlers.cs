﻿using PlannerPro.Entities.API;
using PlannerPro.Utilities;
using System;
using System.Windows.Forms;

namespace PlannerPro.GUI.UserControls.HistoryScreen
{
    public partial class HistoryScreen
    {
        private void dataGridAllFinalizedOrders_CellFormatting(object sender, 
            DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 4) {
                e.Value = ((DeliveryStatus)e.Value).ToDescriptionString();
                e.FormattingApplied = true;
            }
        }
        
        private void dateTimePickerFinalizedStartRange_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePickerFinalizedStartRange.Value > dateTimePickerFinalizedEndRange.Value)
                dateTimePickerFinalizedStartRange.Value = this.previousStartRange;

            RefreshOrderHistory();
        }

        private void dateTimePickerFinalizedEndRange_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePickerFinalizedStartRange.Value > dateTimePickerFinalizedEndRange.Value)
                dateTimePickerFinalizedEndRange.Value = this.previousEndRange;

            RefreshOrderHistory();
        }

        private void dateTimePickerPlanInitDate_ValueChanged(object sender, EventArgs e)
        {
            RefreshPlanHistory();
        }

        private void dataGridOlderPlans_CellFormatting(object sender, 
            DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 3) {
                e.Value = ((PlanStatus)e.Value).ToDescriptionString();
                e.FormattingApplied = true;
            }
        }

        private void dataGridOlderPlans_SelectionChanged(object sender, EventArgs e)
        {
            RefreshPlanFinalizedOrders();
        }

        private void textBoxPlanName_TextChanged(object sender, EventArgs e)
        {
            RefreshPlanHistory();
        }
    }
}
