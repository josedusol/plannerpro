﻿namespace PlannerPro.GUI.UserControls.HistoryScreen
{
    partial class HistoryScreen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxHistory = new System.Windows.Forms.GroupBox();
            this.tabControlHistorys = new System.Windows.Forms.TabControl();
            this.tabPageHistoryOrders = new System.Windows.Forms.TabPage();
            this.dateTimePickerFinalizedEndRange = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerFinalizedStartRange = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridAllFinalizedOrders = new System.Windows.Forms.DataGridView();
            this.finalizedOrdersClientColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finalizedOrdersProductColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finalizedOrdersQuantityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finalizedOrdersDeliveryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finalizedOrdersDeliveryColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finalizedOrdersFinishDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageHistoryPlans = new System.Windows.Forms.TabPage();
            this.labelInitDateFilter = new System.Windows.Forms.Label();
            this.labelPlanNameFilter = new System.Windows.Forms.Label();
            this.dateTimePickerPlanInitDate = new System.Windows.Forms.DateTimePicker();
            this.textBoxPlanName = new System.Windows.Forms.TextBox();
            this.labelHistoryPlanOrders = new System.Windows.Forms.Label();
            this.labelHistoryPlans = new System.Windows.Forms.Label();
            this.dataGridOlderPlans = new System.Windows.Forms.DataGridView();
            this.olderPlansNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.olderPlansInitTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.olderPlansCriteryColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.olderPlansResultColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.olderPlansDescriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridPlanFinalizedOrders = new System.Windows.Forms.DataGridView();
            this.planFinalizedOrdersClientColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.planFinalizedOrdersProductColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.planFinalizedOrdersQuantityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.planFinalizedOrdersDeliveryDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.planFinalizedOrdersDelayColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxHistory.SuspendLayout();
            this.tabControlHistorys.SuspendLayout();
            this.tabPageHistoryOrders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllFinalizedOrders)).BeginInit();
            this.tabPageHistoryPlans.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridOlderPlans)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPlanFinalizedOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxHistory
            // 
            this.groupBoxHistory.Controls.Add(this.tabControlHistorys);
            this.groupBoxHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxHistory.Location = new System.Drawing.Point(10, 10);
            this.groupBoxHistory.Name = "groupBoxHistory";
            this.groupBoxHistory.Padding = new System.Windows.Forms.Padding(5);
            this.groupBoxHistory.Size = new System.Drawing.Size(900, 340);
            this.groupBoxHistory.TabIndex = 0;
            this.groupBoxHistory.TabStop = false;
            this.groupBoxHistory.Text = "Historial";
            // 
            // tabControlHistorys
            // 
            this.tabControlHistorys.Controls.Add(this.tabPageHistoryOrders);
            this.tabControlHistorys.Controls.Add(this.tabPageHistoryPlans);
            this.tabControlHistorys.Location = new System.Drawing.Point(8, 21);
            this.tabControlHistorys.Name = "tabControlHistorys";
            this.tabControlHistorys.SelectedIndex = 0;
            this.tabControlHistorys.Size = new System.Drawing.Size(884, 311);
            this.tabControlHistorys.TabIndex = 14;
            // 
            // tabPageHistoryOrders
            // 
            this.tabPageHistoryOrders.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPageHistoryOrders.Controls.Add(this.dateTimePickerFinalizedEndRange);
            this.tabPageHistoryOrders.Controls.Add(this.dateTimePickerFinalizedStartRange);
            this.tabPageHistoryOrders.Controls.Add(this.label3);
            this.tabPageHistoryOrders.Controls.Add(this.label2);
            this.tabPageHistoryOrders.Controls.Add(this.label1);
            this.tabPageHistoryOrders.Controls.Add(this.dataGridAllFinalizedOrders);
            this.tabPageHistoryOrders.Location = new System.Drawing.Point(4, 22);
            this.tabPageHistoryOrders.Name = "tabPageHistoryOrders";
            this.tabPageHistoryOrders.Padding = new System.Windows.Forms.Padding(5);
            this.tabPageHistoryOrders.Size = new System.Drawing.Size(876, 285);
            this.tabPageHistoryOrders.TabIndex = 1;
            this.tabPageHistoryOrders.Text = "Pedidos";
            // 
            // dateTimePickerFinalizedEndRange
            // 
            this.dateTimePickerFinalizedEndRange.CustomFormat = "dd/MM/yyyy";
            this.dateTimePickerFinalizedEndRange.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerFinalizedEndRange.Location = new System.Drawing.Point(302, 248);
            this.dateTimePickerFinalizedEndRange.Name = "dateTimePickerFinalizedEndRange";
            this.dateTimePickerFinalizedEndRange.Size = new System.Drawing.Size(140, 20);
            this.dateTimePickerFinalizedEndRange.TabIndex = 5;
            this.dateTimePickerFinalizedEndRange.ValueChanged += new System.EventHandler(this.dateTimePickerFinalizedEndRange_ValueChanged);
            // 
            // dateTimePickerFinalizedStartRange
            // 
            this.dateTimePickerFinalizedStartRange.CustomFormat = "dd/MM/yyyy";
            this.dateTimePickerFinalizedStartRange.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerFinalizedStartRange.Location = new System.Drawing.Point(125, 248);
            this.dateTimePickerFinalizedStartRange.Name = "dateTimePickerFinalizedStartRange";
            this.dateTimePickerFinalizedStartRange.Size = new System.Drawing.Size(140, 20);
            this.dateTimePickerFinalizedStartRange.TabIndex = 4;
            this.dateTimePickerFinalizedStartRange.ValueChanged += new System.EventHandler(this.dateTimePickerFinalizedStartRange_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(281, 252);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "y:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 252);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Finalizados entre:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pedidos finalizados:";
            // 
            // dataGridAllFinalizedOrders
            // 
            this.dataGridAllFinalizedOrders.AllowUserToAddRows = false;
            this.dataGridAllFinalizedOrders.AllowUserToDeleteRows = false;
            this.dataGridAllFinalizedOrders.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridAllFinalizedOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAllFinalizedOrders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.finalizedOrdersClientColumn,
            this.finalizedOrdersProductColumn,
            this.finalizedOrdersQuantityColumn,
            this.finalizedOrdersDeliveryDate,
            this.finalizedOrdersDeliveryColumn,
            this.finalizedOrdersFinishDate});
            this.dataGridAllFinalizedOrders.Location = new System.Drawing.Point(33, 36);
            this.dataGridAllFinalizedOrders.MultiSelect = false;
            this.dataGridAllFinalizedOrders.Name = "dataGridAllFinalizedOrders";
            this.dataGridAllFinalizedOrders.ReadOnly = true;
            this.dataGridAllFinalizedOrders.RowHeadersVisible = false;
            this.dataGridAllFinalizedOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridAllFinalizedOrders.Size = new System.Drawing.Size(800, 189);
            this.dataGridAllFinalizedOrders.TabIndex = 0;
            this.dataGridAllFinalizedOrders.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridAllFinalizedOrders_CellFormatting);
            // 
            // finalizedOrdersClientColumn
            // 
            this.finalizedOrdersClientColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.finalizedOrdersClientColumn.DataPropertyName = "Client";
            this.finalizedOrdersClientColumn.HeaderText = "Cliente";
            this.finalizedOrdersClientColumn.MinimumWidth = 130;
            this.finalizedOrdersClientColumn.Name = "finalizedOrdersClientColumn";
            this.finalizedOrdersClientColumn.ReadOnly = true;
            // 
            // finalizedOrdersProductColumn
            // 
            this.finalizedOrdersProductColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.finalizedOrdersProductColumn.DataPropertyName = "Product";
            this.finalizedOrdersProductColumn.HeaderText = "Producto";
            this.finalizedOrdersProductColumn.MinimumWidth = 130;
            this.finalizedOrdersProductColumn.Name = "finalizedOrdersProductColumn";
            this.finalizedOrdersProductColumn.ReadOnly = true;
            // 
            // finalizedOrdersQuantityColumn
            // 
            this.finalizedOrdersQuantityColumn.DataPropertyName = "Quantity";
            this.finalizedOrdersQuantityColumn.HeaderText = "Cantidad";
            this.finalizedOrdersQuantityColumn.MinimumWidth = 110;
            this.finalizedOrdersQuantityColumn.Name = "finalizedOrdersQuantityColumn";
            this.finalizedOrdersQuantityColumn.ReadOnly = true;
            this.finalizedOrdersQuantityColumn.Width = 110;
            // 
            // finalizedOrdersDeliveryDate
            // 
            this.finalizedOrdersDeliveryDate.DataPropertyName = "DeliveryDate";
            this.finalizedOrdersDeliveryDate.HeaderText = "Fecha entrega";
            this.finalizedOrdersDeliveryDate.MinimumWidth = 140;
            this.finalizedOrdersDeliveryDate.Name = "finalizedOrdersDeliveryDate";
            this.finalizedOrdersDeliveryDate.ReadOnly = true;
            this.finalizedOrdersDeliveryDate.Width = 140;
            // 
            // finalizedOrdersDeliveryColumn
            // 
            this.finalizedOrdersDeliveryColumn.DataPropertyName = "DeliveryStatus";
            this.finalizedOrdersDeliveryColumn.HeaderText = "Entrega";
            this.finalizedOrdersDeliveryColumn.MinimumWidth = 110;
            this.finalizedOrdersDeliveryColumn.Name = "finalizedOrdersDeliveryColumn";
            this.finalizedOrdersDeliveryColumn.ReadOnly = true;
            this.finalizedOrdersDeliveryColumn.Width = 110;
            // 
            // finalizedOrdersFinishDate
            // 
            this.finalizedOrdersFinishDate.DataPropertyName = "FinalizeDate";
            this.finalizedOrdersFinishDate.HeaderText = "Fecha fin";
            this.finalizedOrdersFinishDate.MinimumWidth = 140;
            this.finalizedOrdersFinishDate.Name = "finalizedOrdersFinishDate";
            this.finalizedOrdersFinishDate.ReadOnly = true;
            this.finalizedOrdersFinishDate.Width = 140;
            // 
            // tabPageHistoryPlans
            // 
            this.tabPageHistoryPlans.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPageHistoryPlans.Controls.Add(this.labelInitDateFilter);
            this.tabPageHistoryPlans.Controls.Add(this.labelPlanNameFilter);
            this.tabPageHistoryPlans.Controls.Add(this.dateTimePickerPlanInitDate);
            this.tabPageHistoryPlans.Controls.Add(this.textBoxPlanName);
            this.tabPageHistoryPlans.Controls.Add(this.labelHistoryPlanOrders);
            this.tabPageHistoryPlans.Controls.Add(this.labelHistoryPlans);
            this.tabPageHistoryPlans.Controls.Add(this.dataGridOlderPlans);
            this.tabPageHistoryPlans.Controls.Add(this.dataGridPlanFinalizedOrders);
            this.tabPageHistoryPlans.Location = new System.Drawing.Point(4, 22);
            this.tabPageHistoryPlans.Name = "tabPageHistoryPlans";
            this.tabPageHistoryPlans.Padding = new System.Windows.Forms.Padding(5);
            this.tabPageHistoryPlans.Size = new System.Drawing.Size(876, 285);
            this.tabPageHistoryPlans.TabIndex = 0;
            this.tabPageHistoryPlans.Text = "Planes";
            // 
            // labelInitDateFilter
            // 
            this.labelInitDateFilter.AutoSize = true;
            this.labelInitDateFilter.Location = new System.Drawing.Point(8, 245);
            this.labelInitDateFilter.Name = "labelInitDateFilter";
            this.labelInitDateFilter.Size = new System.Drawing.Size(82, 13);
            this.labelInitDateFilter.TabIndex = 19;
            this.labelInitDateFilter.Text = "Fecha de inicio:";
            // 
            // labelPlanNameFilter
            // 
            this.labelPlanNameFilter.AutoSize = true;
            this.labelPlanNameFilter.Location = new System.Drawing.Point(43, 213);
            this.labelPlanNameFilter.Name = "labelPlanNameFilter";
            this.labelPlanNameFilter.Size = new System.Drawing.Size(47, 13);
            this.labelPlanNameFilter.TabIndex = 18;
            this.labelPlanNameFilter.Text = "Nombre:";
            // 
            // dateTimePickerPlanInitDate
            // 
            this.dateTimePickerPlanInitDate.CustomFormat = "dd/MM/yyyy";
            this.dateTimePickerPlanInitDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerPlanInitDate.Location = new System.Drawing.Point(96, 241);
            this.dateTimePickerPlanInitDate.Name = "dateTimePickerPlanInitDate";
            this.dateTimePickerPlanInitDate.Size = new System.Drawing.Size(140, 20);
            this.dateTimePickerPlanInitDate.TabIndex = 17;
            this.dateTimePickerPlanInitDate.ValueChanged += new System.EventHandler(this.dateTimePickerPlanInitDate_ValueChanged);
            // 
            // textBoxPlanName
            // 
            this.textBoxPlanName.Location = new System.Drawing.Point(96, 210);
            this.textBoxPlanName.Name = "textBoxPlanName";
            this.textBoxPlanName.Size = new System.Drawing.Size(140, 20);
            this.textBoxPlanName.TabIndex = 16;
            this.textBoxPlanName.TextChanged += new System.EventHandler(this.textBoxPlanName_TextChanged);
            // 
            // labelHistoryPlanOrders
            // 
            this.labelHistoryPlanOrders.AutoSize = true;
            this.labelHistoryPlanOrders.Location = new System.Drawing.Point(427, 15);
            this.labelHistoryPlanOrders.Name = "labelHistoryPlanOrders";
            this.labelHistoryPlanOrders.Size = new System.Drawing.Size(100, 13);
            this.labelHistoryPlanOrders.TabIndex = 15;
            this.labelHistoryPlanOrders.Text = "Pedidos finalizados:";
            // 
            // labelHistoryPlans
            // 
            this.labelHistoryPlans.AutoSize = true;
            this.labelHistoryPlans.Location = new System.Drawing.Point(5, 15);
            this.labelHistoryPlans.Name = "labelHistoryPlans";
            this.labelHistoryPlans.Size = new System.Drawing.Size(96, 13);
            this.labelHistoryPlans.TabIndex = 14;
            this.labelHistoryPlans.Text = "Planes registrados:";
            // 
            // dataGridOlderPlans
            // 
            this.dataGridOlderPlans.AllowUserToAddRows = false;
            this.dataGridOlderPlans.AllowUserToDeleteRows = false;
            this.dataGridOlderPlans.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridOlderPlans.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridOlderPlans.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.olderPlansNameColumn,
            this.olderPlansInitTimeColumn,
            this.olderPlansCriteryColumn,
            this.olderPlansResultColumn,
            this.olderPlansDescriptionColumn});
            this.dataGridOlderPlans.Location = new System.Drawing.Point(8, 31);
            this.dataGridOlderPlans.MultiSelect = false;
            this.dataGridOlderPlans.Name = "dataGridOlderPlans";
            this.dataGridOlderPlans.ReadOnly = true;
            this.dataGridOlderPlans.RowHeadersVisible = false;
            this.dataGridOlderPlans.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridOlderPlans.Size = new System.Drawing.Size(414, 161);
            this.dataGridOlderPlans.TabIndex = 12;
            this.dataGridOlderPlans.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridOlderPlans_CellFormatting);
            this.dataGridOlderPlans.SelectionChanged += new System.EventHandler(this.dataGridOlderPlans_SelectionChanged);
            // 
            // olderPlansNameColumn
            // 
            this.olderPlansNameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.olderPlansNameColumn.DataPropertyName = "Name";
            this.olderPlansNameColumn.HeaderText = "Nombre";
            this.olderPlansNameColumn.MinimumWidth = 65;
            this.olderPlansNameColumn.Name = "olderPlansNameColumn";
            this.olderPlansNameColumn.ReadOnly = true;
            // 
            // olderPlansInitTimeColumn
            // 
            this.olderPlansInitTimeColumn.DataPropertyName = "StartDate";
            this.olderPlansInitTimeColumn.HeaderText = "Fecha inicio";
            this.olderPlansInitTimeColumn.MinimumWidth = 90;
            this.olderPlansInitTimeColumn.Name = "olderPlansInitTimeColumn";
            this.olderPlansInitTimeColumn.ReadOnly = true;
            this.olderPlansInitTimeColumn.Width = 90;
            // 
            // olderPlansCriteryColumn
            // 
            this.olderPlansCriteryColumn.DataPropertyName = "PlanningCriteria";
            this.olderPlansCriteryColumn.HeaderText = "Criterio";
            this.olderPlansCriteryColumn.MinimumWidth = 70;
            this.olderPlansCriteryColumn.Name = "olderPlansCriteryColumn";
            this.olderPlansCriteryColumn.ReadOnly = true;
            this.olderPlansCriteryColumn.Width = 70;
            // 
            // olderPlansResultColumn
            // 
            this.olderPlansResultColumn.DataPropertyName = "Status";
            this.olderPlansResultColumn.HeaderText = "Resultado";
            this.olderPlansResultColumn.MinimumWidth = 65;
            this.olderPlansResultColumn.Name = "olderPlansResultColumn";
            this.olderPlansResultColumn.ReadOnly = true;
            this.olderPlansResultColumn.Width = 65;
            // 
            // olderPlansDescriptionColumn
            // 
            this.olderPlansDescriptionColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.olderPlansDescriptionColumn.DataPropertyName = "Description";
            this.olderPlansDescriptionColumn.HeaderText = "Descripción";
            this.olderPlansDescriptionColumn.MinimumWidth = 100;
            this.olderPlansDescriptionColumn.Name = "olderPlansDescriptionColumn";
            this.olderPlansDescriptionColumn.ReadOnly = true;
            // 
            // dataGridPlanFinalizedOrders
            // 
            this.dataGridPlanFinalizedOrders.AllowUserToAddRows = false;
            this.dataGridPlanFinalizedOrders.AllowUserToDeleteRows = false;
            this.dataGridPlanFinalizedOrders.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridPlanFinalizedOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridPlanFinalizedOrders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.planFinalizedOrdersClientColumn,
            this.planFinalizedOrdersProductColumn,
            this.planFinalizedOrdersQuantityColumn,
            this.planFinalizedOrdersDeliveryDateColumn,
            this.planFinalizedOrdersDelayColumn});
            this.dataGridPlanFinalizedOrders.Location = new System.Drawing.Point(430, 31);
            this.dataGridPlanFinalizedOrders.MultiSelect = false;
            this.dataGridPlanFinalizedOrders.Name = "dataGridPlanFinalizedOrders";
            this.dataGridPlanFinalizedOrders.ReadOnly = true;
            this.dataGridPlanFinalizedOrders.RowHeadersVisible = false;
            this.dataGridPlanFinalizedOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridPlanFinalizedOrders.Size = new System.Drawing.Size(438, 161);
            this.dataGridPlanFinalizedOrders.TabIndex = 13;
            // 
            // planFinalizedOrdersClientColumn
            // 
            this.planFinalizedOrdersClientColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.planFinalizedOrdersClientColumn.DataPropertyName = "Client";
            this.planFinalizedOrdersClientColumn.HeaderText = "Cliente";
            this.planFinalizedOrdersClientColumn.MinimumWidth = 80;
            this.planFinalizedOrdersClientColumn.Name = "planFinalizedOrdersClientColumn";
            this.planFinalizedOrdersClientColumn.ReadOnly = true;
            // 
            // planFinalizedOrdersProductColumn
            // 
            this.planFinalizedOrdersProductColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.planFinalizedOrdersProductColumn.DataPropertyName = "Product";
            this.planFinalizedOrdersProductColumn.HeaderText = "Producto";
            this.planFinalizedOrdersProductColumn.MinimumWidth = 80;
            this.planFinalizedOrdersProductColumn.Name = "planFinalizedOrdersProductColumn";
            this.planFinalizedOrdersProductColumn.ReadOnly = true;
            // 
            // planFinalizedOrdersQuantityColumn
            // 
            this.planFinalizedOrdersQuantityColumn.DataPropertyName = "Quantity";
            this.planFinalizedOrdersQuantityColumn.HeaderText = "Cantidad";
            this.planFinalizedOrdersQuantityColumn.MinimumWidth = 60;
            this.planFinalizedOrdersQuantityColumn.Name = "planFinalizedOrdersQuantityColumn";
            this.planFinalizedOrdersQuantityColumn.ReadOnly = true;
            this.planFinalizedOrdersQuantityColumn.Width = 60;
            // 
            // planFinalizedOrdersDeliveryDateColumn
            // 
            this.planFinalizedOrdersDeliveryDateColumn.DataPropertyName = "DeliveryDate";
            this.planFinalizedOrdersDeliveryDateColumn.HeaderText = "Fecha entrega";
            this.planFinalizedOrdersDeliveryDateColumn.MinimumWidth = 100;
            this.planFinalizedOrdersDeliveryDateColumn.Name = "planFinalizedOrdersDeliveryDateColumn";
            this.planFinalizedOrdersDeliveryDateColumn.ReadOnly = true;
            // 
            // planFinalizedOrdersDelayColumn
            // 
            this.planFinalizedOrdersDelayColumn.DataPropertyName = "FinalizeDate";
            this.planFinalizedOrdersDelayColumn.HeaderText = "Fecha fin";
            this.planFinalizedOrdersDelayColumn.MinimumWidth = 95;
            this.planFinalizedOrdersDelayColumn.Name = "planFinalizedOrdersDelayColumn";
            this.planFinalizedOrdersDelayColumn.ReadOnly = true;
            this.planFinalizedOrdersDelayColumn.Width = 95;
            // 
            // HistoryScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxHistory);
            this.Name = "HistoryScreen";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Size = new System.Drawing.Size(920, 360);
            this.groupBoxHistory.ResumeLayout(false);
            this.tabControlHistorys.ResumeLayout(false);
            this.tabPageHistoryOrders.ResumeLayout(false);
            this.tabPageHistoryOrders.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllFinalizedOrders)).EndInit();
            this.tabPageHistoryPlans.ResumeLayout(false);
            this.tabPageHistoryPlans.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridOlderPlans)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPlanFinalizedOrders)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxHistory;
        private System.Windows.Forms.DataGridView dataGridOlderPlans;
        private System.Windows.Forms.DataGridView dataGridPlanFinalizedOrders;
        private System.Windows.Forms.TabControl tabControlHistorys;
        private System.Windows.Forms.TabPage tabPageHistoryPlans;
        private System.Windows.Forms.Label labelHistoryPlanOrders;
        private System.Windows.Forms.Label labelHistoryPlans;
        private System.Windows.Forms.TabPage tabPageHistoryOrders;
        private System.Windows.Forms.TextBox textBoxPlanName;
        private System.Windows.Forms.DateTimePicker dateTimePickerPlanInitDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridAllFinalizedOrders;
        private System.Windows.Forms.Label labelInitDateFilter;
        private System.Windows.Forms.Label labelPlanNameFilter;
        private System.Windows.Forms.DateTimePicker dateTimePickerFinalizedEndRange;
        private System.Windows.Forms.DateTimePicker dateTimePickerFinalizedStartRange;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn finalizedOrdersClientColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn finalizedOrdersProductColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn finalizedOrdersQuantityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn finalizedOrdersDeliveryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn finalizedOrdersDeliveryColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn finalizedOrdersFinishDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn planFinalizedOrdersClientColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn planFinalizedOrdersProductColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn planFinalizedOrdersQuantityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn planFinalizedOrdersDeliveryDateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn planFinalizedOrdersDelayColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn olderPlansNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn olderPlansInitTimeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn olderPlansCriteryColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn olderPlansResultColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn olderPlansDescriptionColumn;
    }
}
