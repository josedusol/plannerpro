﻿using PlannerPro.Entities.API;
using PlannerPro.Logic.API;
using PlannerPro.Logic.LogicImp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace PlannerPro.GUI.UserControls.HistoryScreen
{
    public partial class HistoryScreen : UserControl
    {
        private IOrderLogic orderLogic;
        private IPlanLogic planLogic;
        private BindingSource orderBindingSource;
        private BindingSource planBindingSource;
        private DateTime previousStartRange;
        private DateTime previousEndRange;

        public HistoryScreen()
        {
            InitializeComponent();
            dataGridAllFinalizedOrders.AutoGenerateColumns = false;
            dataGridOlderPlans.AutoGenerateColumns = false;
            dataGridPlanFinalizedOrders.AutoGenerateColumns = false;         
            previousStartRange = dateTimePickerFinalizedStartRange.Value;
            previousEndRange = dateTimePickerFinalizedEndRange.Value;
            orderLogic = OrderLogic.GetInstance();
            planLogic = PlanLogic.GetInstance();
            orderBindingSource = new BindingSource();
            planBindingSource = new BindingSource();
            if (!(DesignMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime))
                RefreshData();
        }

        public void RefreshData()
        {
            RefreshOrderHistory();
            RefreshPlanHistory();
        }

        private void RefreshOrderHistory()
        {
            RefreshFinalizedOrders();
        }

        private void RefreshPlanHistory()
        {
            RefreshOlderPlans();
            RefreshPlanFinalizedOrders();
        }

        private void RefreshFinalizedOrders()
        {
            var orderList = GetFinalizedOrdersInDateRange(orderLogic.GetAllRegisteredOrders());
            orderBindingSource.DataSource = orderList;
            dataGridAllFinalizedOrders.DataSource = orderBindingSource;
        }

        private IList<Order> GetFinalizedOrdersInDateRange(IEnumerable<Order> orders)
        {
            DateTime startRange = dateTimePickerFinalizedStartRange.Value;
            DateTime endRange = dateTimePickerFinalizedEndRange.Value;

            return orders.Where(order => order.Status == OrderStatus.Finalized
                 && order.FinalizeDate.Date >= startRange.Date
                 && order.FinalizeDate.Date <= endRange.Date).ToList();
        }

        private void RefreshOlderPlans()
        {
            var planList = GetOlderAndFilteredPlans(planLogic.GetAllPlans());
            planBindingSource.DataSource = planList;
            dataGridOlderPlans.DataSource = planBindingSource;
        }

        private IList<Plan> GetOlderAndFilteredPlans(IEnumerable<Plan> plans)
        {
            string name = textBoxPlanName.Text;
            string pattern = (String.IsNullOrEmpty(name) == false ? "^" + name : "^.*");
            var nameRegex = new Regex(pattern);
            DateTime initDate = dateTimePickerPlanInitDate.Value;

            return plans.Where(plan => plan.Status != PlanStatus.Runnning
                 && nameRegex.IsMatch(plan.Name) 
                 && plan.StartDate.Date == initDate.Date).ToList();
        }

        private void RefreshPlanFinalizedOrders()
        {
            Plan selectedPlan = GetSelectedPlanFromDataGrid();

            if (selectedPlan != Plan.NULL)
                LoadPlanFinalizedOrders(selectedPlan);
            else
                dataGridPlanFinalizedOrders.DataSource = null;
        }

        private Plan GetSelectedPlanFromDataGrid()
        {
            return (dataGridOlderPlans.Rows.Count > 0 
                && dataGridOlderPlans.SelectedRows.Count > 0) ?
                (Plan)dataGridOlderPlans.CurrentRow.DataBoundItem
                : 
                Plan.NULL;
        }

        private void LoadPlanFinalizedOrders(Plan selectedPlan)
        {
            dataGridPlanFinalizedOrders.DataSource = null;
            var finalizedOrders = selectedPlan.FinalizedOrders;
            dataGridPlanFinalizedOrders.DataSource = finalizedOrders;
        }
    }
}
