﻿namespace PlannerPro.GUI.UserControls.PlanScreen
{
    partial class PlanScreen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxPlans = new System.Windows.Forms.GroupBox();
            this.labelOrders = new System.Windows.Forms.Label();
            this.dataGridRunningPlans = new System.Windows.Forms.DataGridView();
            this.registeredPlansNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.registeredPlansInitTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.registeredPlansCriteryColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.registeredPlansDescriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.labelDescription = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelExecutedPlans = new System.Windows.Forms.Label();
            this.dataGridPendentOrders = new System.Windows.Forms.DataGridView();
            this.dataGridClientColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridProductColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridQuantityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridDeliveryDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridDelayColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonFinalizeOrder = new System.Windows.Forms.Button();
            this.buttonCancelPlan = new System.Windows.Forms.Button();
            this.buttonExecutePlan = new System.Windows.Forms.Button();
            this.groupBoxPlans.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRunningPlans)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPendentOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxPlans
            // 
            this.groupBoxPlans.Controls.Add(this.labelOrders);
            this.groupBoxPlans.Controls.Add(this.dataGridRunningPlans);
            this.groupBoxPlans.Controls.Add(this.textBoxDescription);
            this.groupBoxPlans.Controls.Add(this.labelDescription);
            this.groupBoxPlans.Controls.Add(this.labelName);
            this.groupBoxPlans.Controls.Add(this.textBoxName);
            this.groupBoxPlans.Controls.Add(this.labelExecutedPlans);
            this.groupBoxPlans.Controls.Add(this.dataGridPendentOrders);
            this.groupBoxPlans.Controls.Add(this.buttonFinalizeOrder);
            this.groupBoxPlans.Controls.Add(this.buttonCancelPlan);
            this.groupBoxPlans.Controls.Add(this.buttonExecutePlan);
            this.groupBoxPlans.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxPlans.Location = new System.Drawing.Point(10, 10);
            this.groupBoxPlans.Name = "groupBoxPlans";
            this.groupBoxPlans.Padding = new System.Windows.Forms.Padding(20);
            this.groupBoxPlans.Size = new System.Drawing.Size(900, 340);
            this.groupBoxPlans.TabIndex = 0;
            this.groupBoxPlans.TabStop = false;
            this.groupBoxPlans.Text = "Planes de producción";
            // 
            // labelOrders
            // 
            this.labelOrders.AutoSize = true;
            this.labelOrders.Location = new System.Drawing.Point(434, 186);
            this.labelOrders.Name = "labelOrders";
            this.labelOrders.Size = new System.Drawing.Size(103, 13);
            this.labelOrders.TabIndex = 12;
            this.labelOrders.Text = "Pedidos pendientes:";
            // 
            // dataGridRunningPlans
            // 
            this.dataGridRunningPlans.AllowUserToAddRows = false;
            this.dataGridRunningPlans.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridRunningPlans.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridRunningPlans.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.registeredPlansNameColumn,
            this.registeredPlansInitTimeColumn,
            this.registeredPlansCriteryColumn,
            this.registeredPlansDescriptionColumn});
            this.dataGridRunningPlans.Location = new System.Drawing.Point(437, 49);
            this.dataGridRunningPlans.MultiSelect = false;
            this.dataGridRunningPlans.Name = "dataGridRunningPlans";
            this.dataGridRunningPlans.RowHeadersVisible = false;
            this.dataGridRunningPlans.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridRunningPlans.Size = new System.Drawing.Size(440, 115);
            this.dataGridRunningPlans.TabIndex = 11;
            this.dataGridRunningPlans.SelectionChanged += new System.EventHandler(this.dataGridRunningPlans_SelectionChanged);
            // 
            // registeredPlansNameColumn
            // 
            this.registeredPlansNameColumn.DataPropertyName = "Name";
            this.registeredPlansNameColumn.HeaderText = "Nombre";
            this.registeredPlansNameColumn.MinimumWidth = 100;
            this.registeredPlansNameColumn.Name = "registeredPlansNameColumn";
            this.registeredPlansNameColumn.ReadOnly = true;
            // 
            // registeredPlansInitTimeColumn
            // 
            this.registeredPlansInitTimeColumn.DataPropertyName = "StartDate";
            this.registeredPlansInitTimeColumn.HeaderText = "Fecha de inicio";
            this.registeredPlansInitTimeColumn.MinimumWidth = 120;
            this.registeredPlansInitTimeColumn.Name = "registeredPlansInitTimeColumn";
            this.registeredPlansInitTimeColumn.ReadOnly = true;
            this.registeredPlansInitTimeColumn.Width = 120;
            // 
            // registeredPlansCriteryColumn
            // 
            this.registeredPlansCriteryColumn.DataPropertyName = "PlanningCriteria";
            this.registeredPlansCriteryColumn.HeaderText = "Criterio";
            this.registeredPlansCriteryColumn.MinimumWidth = 105;
            this.registeredPlansCriteryColumn.Name = "registeredPlansCriteryColumn";
            this.registeredPlansCriteryColumn.ReadOnly = true;
            this.registeredPlansCriteryColumn.Width = 105;
            // 
            // registeredPlansDescriptionColumn
            // 
            this.registeredPlansDescriptionColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.registeredPlansDescriptionColumn.DataPropertyName = "Description";
            this.registeredPlansDescriptionColumn.HeaderText = "Descripción";
            this.registeredPlansDescriptionColumn.MinimumWidth = 110;
            this.registeredPlansDescriptionColumn.Name = "registeredPlansDescriptionColumn";
            this.registeredPlansDescriptionColumn.ReadOnly = true;
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Location = new System.Drawing.Point(170, 102);
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(180, 20);
            this.textBoxDescription.TabIndex = 10;
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Location = new System.Drawing.Point(98, 105);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(66, 13);
            this.labelDescription.TabIndex = 9;
            this.labelDescription.Text = "Descripción:";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(117, 52);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(47, 13);
            this.labelName.TabIndex = 8;
            this.labelName.Text = "Nombre:";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(170, 49);
            this.textBoxName.Margin = new System.Windows.Forms.Padding(3, 3, 3, 30);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(180, 20);
            this.textBoxName.TabIndex = 7;
            // 
            // labelExecutedPlans
            // 
            this.labelExecutedPlans.AutoSize = true;
            this.labelExecutedPlans.Location = new System.Drawing.Point(434, 33);
            this.labelExecutedPlans.Name = "labelExecutedPlans";
            this.labelExecutedPlans.Size = new System.Drawing.Size(106, 13);
            this.labelExecutedPlans.TabIndex = 6;
            this.labelExecutedPlans.Text = "Planes en ejecución:";
            // 
            // dataGridPendentOrders
            // 
            this.dataGridPendentOrders.AllowUserToAddRows = false;
            this.dataGridPendentOrders.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridPendentOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridPendentOrders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridClientColumn,
            this.dataGridProductColumn,
            this.dataGridQuantityColumn,
            this.dataGridDeliveryDateColumn,
            this.dataGridDelayColumn});
            this.dataGridPendentOrders.Location = new System.Drawing.Point(437, 202);
            this.dataGridPendentOrders.MultiSelect = false;
            this.dataGridPendentOrders.Name = "dataGridPendentOrders";
            this.dataGridPendentOrders.ReadOnly = true;
            this.dataGridPendentOrders.RowHeadersVisible = false;
            this.dataGridPendentOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridPendentOrders.Size = new System.Drawing.Size(440, 115);
            this.dataGridPendentOrders.TabIndex = 5;
            this.dataGridPendentOrders.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridPendentOrders_CellFormatting);
            this.dataGridPendentOrders.SelectionChanged += new System.EventHandler(this.dataGridPlanification_SelectionChanged);
            // 
            // dataGridClientColumn
            // 
            this.dataGridClientColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridClientColumn.DataPropertyName = "Client";
            this.dataGridClientColumn.HeaderText = "Cliente";
            this.dataGridClientColumn.MinimumWidth = 80;
            this.dataGridClientColumn.Name = "dataGridClientColumn";
            this.dataGridClientColumn.ReadOnly = true;
            // 
            // dataGridProductColumn
            // 
            this.dataGridProductColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridProductColumn.DataPropertyName = "Product";
            this.dataGridProductColumn.HeaderText = "Producto";
            this.dataGridProductColumn.MinimumWidth = 80;
            this.dataGridProductColumn.Name = "dataGridProductColumn";
            this.dataGridProductColumn.ReadOnly = true;
            // 
            // dataGridQuantityColumn
            // 
            this.dataGridQuantityColumn.DataPropertyName = "Quantity";
            this.dataGridQuantityColumn.HeaderText = "Cantidad";
            this.dataGridQuantityColumn.MinimumWidth = 60;
            this.dataGridQuantityColumn.Name = "dataGridQuantityColumn";
            this.dataGridQuantityColumn.ReadOnly = true;
            this.dataGridQuantityColumn.Width = 60;
            // 
            // dataGridDeliveryDateColumn
            // 
            this.dataGridDeliveryDateColumn.DataPropertyName = "DeliveryDate";
            this.dataGridDeliveryDateColumn.HeaderText = "Fecha de entrega";
            this.dataGridDeliveryDateColumn.MinimumWidth = 115;
            this.dataGridDeliveryDateColumn.Name = "dataGridDeliveryDateColumn";
            this.dataGridDeliveryDateColumn.ReadOnly = true;
            this.dataGridDeliveryDateColumn.Width = 115;
            // 
            // dataGridDelayColumn
            // 
            this.dataGridDelayColumn.DataPropertyName = "DeliveryStatus";
            this.dataGridDelayColumn.HeaderText = "Entrega";
            this.dataGridDelayColumn.MinimumWidth = 80;
            this.dataGridDelayColumn.Name = "dataGridDelayColumn";
            this.dataGridDelayColumn.ReadOnly = true;
            this.dataGridDelayColumn.Width = 80;
            // 
            // buttonFinalizeOrder
            // 
            this.buttonFinalizeOrder.Location = new System.Drawing.Point(280, 294);
            this.buttonFinalizeOrder.Name = "buttonFinalizeOrder";
            this.buttonFinalizeOrder.Size = new System.Drawing.Size(105, 23);
            this.buttonFinalizeOrder.TabIndex = 4;
            this.buttonFinalizeOrder.Text = "Finalizar pedido";
            this.buttonFinalizeOrder.UseVisualStyleBackColor = true;
            this.buttonFinalizeOrder.Click += new System.EventHandler(this.buttonFinalizeOrder_Click);
            // 
            // buttonCancelPlan
            // 
            this.buttonCancelPlan.Location = new System.Drawing.Point(150, 294);
            this.buttonCancelPlan.Name = "buttonCancelPlan";
            this.buttonCancelPlan.Size = new System.Drawing.Size(105, 23);
            this.buttonCancelPlan.TabIndex = 3;
            this.buttonCancelPlan.Text = "Cancelar plan";
            this.buttonCancelPlan.UseVisualStyleBackColor = true;
            this.buttonCancelPlan.Click += new System.EventHandler(this.buttonCancelPlan_Click);
            // 
            // buttonExecutePlan
            // 
            this.buttonExecutePlan.Location = new System.Drawing.Point(23, 294);
            this.buttonExecutePlan.Name = "buttonExecutePlan";
            this.buttonExecutePlan.Size = new System.Drawing.Size(105, 23);
            this.buttonExecutePlan.TabIndex = 2;
            this.buttonExecutePlan.Text = "Ejecutar plan";
            this.buttonExecutePlan.UseVisualStyleBackColor = true;
            this.buttonExecutePlan.Click += new System.EventHandler(this.buttonRunPlan_Click);
            // 
            // PlanScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxPlans);
            this.Name = "PlanScreen";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Size = new System.Drawing.Size(920, 360);
            this.groupBoxPlans.ResumeLayout(false);
            this.groupBoxPlans.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRunningPlans)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPendentOrders)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxPlans;
        private System.Windows.Forms.Label labelExecutedPlans;
        private System.Windows.Forms.Button buttonFinalizeOrder;
        private System.Windows.Forms.Button buttonCancelPlan;
        private System.Windows.Forms.Button buttonExecutePlan;
        private System.Windows.Forms.DataGridView dataGridPendentOrders;
        private System.Windows.Forms.DataGridView dataGridRunningPlans;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label labelOrders;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridClientColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridProductColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridQuantityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridDeliveryDateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridDelayColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn registeredPlansNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn registeredPlansInitTimeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn registeredPlansCriteryColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn registeredPlansDescriptionColumn;
    }
}
