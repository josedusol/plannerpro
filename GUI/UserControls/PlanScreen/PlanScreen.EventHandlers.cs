﻿using PlannerPro.Entities.API;
using PlannerPro.Entities.Exceptions;
using PlannerPro.Utilities;
using System;
using System.Windows.Forms;

namespace PlannerPro.GUI.UserControls.PlanScreen
{
    public partial class PlanScreen
    {
        private void buttonRunPlan_Click(object sender, EventArgs e)
        {
            try {
                TryRunPlan();
            } catch (PlanException ex) {
                DialogBox.DisplayError("Planes", ex.Message);
            }
        }

        private void buttonCancelPlan_Click(object sender, EventArgs e)
        {
            try {
                TryCancelPlan();
            } catch (PlanException ex) {
                DialogBox.DisplayError("Planes", ex.Message);
            }
        }

        private void buttonFinalizeOrder_Click(object sender, EventArgs e)
        {
            try {
                TryFinalizeOrder();
            } catch (PlanException ex) {
                DialogBox.DisplayError("Planes", ex.Message);
            }
        }

        private void dataGridPendentOrders_CellFormatting(object sender, 
            DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 4) {
                e.Value = ((DeliveryStatus)e.Value).ToDescriptionString();
                e.FormattingApplied = true;
            }
        }

        private void dataGridRunningPlans_SelectionChanged(object sender, EventArgs e)
        {
            RefreshPendentOrders();
        }

        private void dataGridPlanification_SelectionChanged(object sender, System.EventArgs e)
        {
            dataGridPendentOrders.CurrentRow.Selected = false;
        }
    }
}
