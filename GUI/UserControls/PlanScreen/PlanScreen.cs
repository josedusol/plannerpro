﻿using PlannerPro.Entities.API;
using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Logic.API;
using PlannerPro.Logic.LogicImp;
using PlannerPro.Utilities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace PlannerPro.GUI.UserControls.PlanScreen
{
    public partial class PlanScreen : UserControl
    {
        private IPlanLogic planLogic;
        private BindingSource planBindingSource;
        private BindingSource orderBindingSource;

        public PlanScreen()
        {
            InitializeComponent();
            dataGridRunningPlans.AutoGenerateColumns = false;
            dataGridPendentOrders.AutoGenerateColumns = false;
            planLogic = PlanLogic.GetInstance();
            planBindingSource = new BindingSource();
            orderBindingSource = new BindingSource();
            if (!(DesignMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime))
                RefreshData();
        }

        private void TryRunPlan()
        {
            planLogic.RunPlan(CreatePlanFromUserInput());
            RefreshRunningPlans();
            ResetControls();
            DialogBox.DisplayInfo("Planes", "El plan se ha ejecutado.");
        }

        private Plan CreatePlanFromUserInput()
        {
            return new RealPlan(textBoxName.Text, 
                textBoxDescription.Text, 
                planLogic.DefaultPlanningCriteria);
        }

        public void RefreshData()
        {
            RefreshRunningPlans();
            RefreshPendentOrders();
        }

        public void RefreshRunningPlans()
        {
            var runningPlans = GetRunningPlans(planLogic.GetAllPlans());
            runningPlans.ForEach(plan => plan.DoPlanning());
            planBindingSource.DataSource = runningPlans;
            dataGridRunningPlans.DataSource = planBindingSource;
        }

        private IList<Plan> GetRunningPlans(IEnumerable<Plan> plans)
        {
            return plans.Where(plan => plan.Status == PlanStatus.Runnning).ToList();
        }

        private void ResetControls()
        {
            textBoxName.Clear();
            textBoxDescription.Clear();
        }

        private void TryCancelPlan()
        {
            Plan selectedPlan = GetSelectedPlanFromDataGrid();

            if (selectedPlan != Plan.NULL) {
                planLogic.CancelPlan(selectedPlan);
                RefreshData();
                DialogBox.DisplayInfo("Planes", "El plan se ha cancelado.");
            } else
                DialogBox.DisplayError("Planes", "No hay plan seleccionado.");
        }

        private Plan GetSelectedPlanFromDataGrid()
        {
            return (dataGridRunningPlans.Rows.Count > 0 
                && dataGridRunningPlans.SelectedRows.Count > 0) ?
                (Plan)dataGridRunningPlans.CurrentRow.DataBoundItem
              : Plan.NULL;  
        }

        private void TryFinalizeOrder()
        {
            Plan selectedPlan = GetSelectedPlanFromDataGrid();

            if (selectedPlan != Plan.NULL) {
                planLogic.FinalizeNextOrder(selectedPlan);
                RefreshData();
            } else
                DialogBox.DisplayError("Planes", "No hay plan seleccionado.");
        }

        public void RefreshPendentOrders()
        {
            Plan selectedPlan = GetSelectedPlanFromDataGrid();

            if (selectedPlan != Plan.NULL)
                LoadPendentOrders(selectedPlan);
            else
                dataGridPendentOrders.DataSource = null;
        }

        private void LoadPendentOrders(Plan selectedPlan)
        {
            dataGridPendentOrders.DataSource = null;
            var pendentOrders = selectedPlan.PendentOrders;
            orderBindingSource.DataSource = pendentOrders;
            dataGridPendentOrders.DataSource = orderBindingSource;
        }
    }
}
