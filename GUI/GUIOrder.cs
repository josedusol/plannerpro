﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PlannerPro.Logic;
using PlannerPro.Logic.Exceptions;
using PlannerPro.Entities;
using PlannerPro.Entities.Exceptions;

namespace PlannerPro.GUI
{
    public partial class GUIOrder : UserControl
    {
        IOrderLogic orderLogic = OrderLogic.GetInstance();
        IClientLogic clientLogic = ClientLogic.GetInstance();

        public GUIOrder()
        {
            InitializeComponent();
        }

        private void buttonRegisterOrder_Click(object sender, EventArgs e)
        {
            try
            {
                Order order = TryCreateOrder();
                TryRegisterOrder(order);
                LoadAllRegisteredOrders();
            }
            catch (InvalidOrderException ipe)
            {
                MainWindow.DisplayError("Productos", ipe.Message);
            }
        }

        private Order TryCreateOrder()
        {
            return new RealOrder((Client)comboBoxClient.SelectedItem,
                (Product)comboBoxProduct.SelectedItem, (int)numericUpDownQuantity.Value,
                (DateTime)dateTimePickerOrder.Value);
        }

        private void TryRegisterOrder(Order order)
        {
            orderLogic.RegisterOrder(order);
            MainWindow.DisplayMessage("Pedidos", "El pedido se ha dado de alta.");
        }

        private static DataTable ConvertListToDataTable(List<Order> list)
        {
            DataTable table = new DataTable();
            int columns = 5;

            for (int i = 0; i < columns; i++)
            {
                table.Columns.Add();
            }

            foreach (var array in list)
            {
                table.Rows.Add(array);
            }

            return table;
        }

        private void LoadAllRegisteredOrders()
        {
            this.dataGridViewOrder.DataSource = orderLogic.GetAllRegisteredOrders();
            
        }
    }
}
