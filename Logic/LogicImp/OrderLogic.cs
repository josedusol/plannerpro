﻿using PlannerPro.Data.DataImp;
using PlannerPro.Entities.API;
using PlannerPro.Entities.Exceptions;
using PlannerPro.Logic.API;
using System.Collections.Generic;
using System.Linq;

namespace PlannerPro.Logic.LogicImp
{
    public class OrderLogic : IOrderLogic
    {
        private static OrderLogic orderLogicInstance;
        private UnitOfWork unitOfWork;

        public static OrderLogic GetInstance()
        {
            if (orderLogicInstance == null)
                orderLogicInstance = new OrderLogic();

            return orderLogicInstance;
        }

        private OrderLogic()
        {
            unitOfWork = UnitOfWork.GetInstance();
        }

        public void RegisterOrder(Order order)
        {
            if (OrderIsRegistered(order))
                throw new OrderException("Ya existe un pedido con" +
                   " los mismos datos.");

             unitOfWork.OrderRepository.CreateOrder(order);
             unitOfWork.Save();
        }

        public void UpdateOrder(Order oldOrder, Order newOrder)
        {
            if (!OrderIsRegistered(oldOrder))
                throw new OrderException("No existe el pedido.");

            if (!OrderIsNew(oldOrder))
                throw new OrderException("Solo se pueden modificar pedidos nuevos.");

            if (!oldOrder.Equals(newOrder) && OrderIsRegistered(newOrder))
                throw new OrderException("Ya existe un pedido con" +
                    " los mismos datos.");

            oldOrder.Product = newOrder.Product;
            oldOrder.Client = newOrder.Client;
            oldOrder.Quantity = newOrder.Quantity;
            oldOrder.DeliveryDate = newOrder.DeliveryDate;
            oldOrder.Status = newOrder.Status;

            unitOfWork.Save();
        }

        public void UnregisterOrder(Order order)
        {
            if (!OrderIsRegistered(order))
                throw new OrderException("No existe el pedido.");

            if (OrderIsPending(order))
                throw new OrderException("No se puede dar de baja un pedido pendiente.");

            unitOfWork.OrderRepository.DeleteOrder(order);
            unitOfWork.Save();
        }

        public bool OrderIsRegistered(Order order)
        {
            return unitOfWork.OrderRepository.GetAllOrders().Any(o => order.Equals(o));
        }

        public IEnumerable<Order> GetAllRegisteredOrders()
        {
            return unitOfWork.OrderRepository.GetAllOrders();
        }

        private bool OrderIsNew(Order order)
        {
            return (order.Status == OrderStatus.New ? true : false);
        }

        private bool OrderIsPending(Order order)
        {
            return (order.Status == OrderStatus.Pending ? true : false);
        }
    }
}
