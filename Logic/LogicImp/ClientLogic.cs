﻿using PlannerPro.Data.DataImp;
using PlannerPro.Entities.API;
using PlannerPro.Entities.Exceptions;
using PlannerPro.Logic.API;
using System.Collections.Generic;
using System.Linq;

namespace PlannerPro.Logic.LogicImp
{
    public class ClientLogic : IClientLogic
    {
        private static ClientLogic clientLogicInstance;
        private UnitOfWork unitOfWork;

        public static ClientLogic GetInstance()
        {
            if (clientLogicInstance == null)
                clientLogicInstance = new ClientLogic();

            return clientLogicInstance;
        }

        private ClientLogic()
        {
            unitOfWork = UnitOfWork.GetInstance();
        }

        public void RegisterClient(Client client)
        {
            if (ClientIsRegistered(client))
                throw new ClientException("Ya hay un cliente dado de alta con" +
                   " los mismos datos.");

            unitOfWork.ClientRepository.CreateClient(client);
            unitOfWork.Save();
        }

        public void UpdateClient(Client oldClient, Client newClient)
        {
            if (!ClientIsRegistered(oldClient))
                throw new ClientException("No existe el cliente.");

            if (ClientIsInOrder(oldClient))
                throw new ClientException("No se puede modificar el cliente" +
                   " porque se encuentra asociado a un pedido.");

            oldClient.Name = newClient.Name;
            oldClient.Preference = newClient.Preference;
            
            unitOfWork.Save();
        }

        public void UnregisterClient(Client client)
        {
            if (!ClientIsRegistered(client))
                throw new ClientException("No hay cliente dado de alta con" +
                   " el nombre: " + client.Name);

            if (ClientIsInOrder(client))
                throw new ClientException("No se puede dar de baja el cliente" +
                   " porque se encuentra asociado a un pedido.");

            unitOfWork.ClientRepository.DeleteClient(client);
            unitOfWork.Save();
        }

        public bool ClientIsRegistered(Client client)
        {
            return unitOfWork.ClientRepository.GetAllClients().Any(c => client.Equals(c));
        }

        public IEnumerable<Client> GetAllRegisteredClients()
        {
            return unitOfWork.ClientRepository.GetAllClients();
        }

        private bool ClientIsInOrder(Client client)
        {
            foreach (Order o in unitOfWork.OrderRepository.GetAllOrders())
                if (o.Client.Equals(client))
                    return true;

            return false;
        }      
    }
}
