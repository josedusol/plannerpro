﻿using PlannerPro.Data.API;
using PlannerPro.Data.DataImp;
using PlannerPro.Entities.API;
using PlannerPro.Entities.Exceptions;
using PlannerPro.Logic.API;
using System.Collections.Generic;
using System.Linq;

namespace PlannerPro.Logic.LogicImp
{
    public class ProductLogic : IProductLogic
    {
        private static ProductLogic productLogicInstance;
        private IUnitOfWork unitOfWork;

        public static ProductLogic GetInstance()
        {
            if (productLogicInstance == null)
                productLogicInstance = new ProductLogic();

            return productLogicInstance;
        }

        private ProductLogic()
        {
            unitOfWork = UnitOfWork.GetInstance();
        }

        public void RegisterProduct(Product product)
        {
            if (ProductIsRegistered(product))
                throw new ProductException("Ya existe un producto" +
                    " con el nombre: " + product.Name);

            unitOfWork.ProductRepository.CreateProduct(product);
            unitOfWork.Save();
        }

        public void UpdateProduct(Product oldProduct, Product newProduct)
        {
            if (!ProductIsRegistered(oldProduct))
                throw new ProductException("No hay producto dado de alta con" +
                   " el nombre: " + oldProduct.Name);

            if (ProductIsInOrder(oldProduct))
                throw new ProductException("No se puede modificar el producto" +
                   " porque se encuentra asociado a un pedido.");

            if (ProductNameExists(oldProduct, newProduct))
                throw new ProductException("Ya existe un producto" +
                    " con el nombre: " + newProduct.Name);

            oldProduct.Name = newProduct.Name;
            oldProduct.ManufacturingTime = newProduct.ManufacturingTime;
            oldProduct.ProductionCost = newProduct.ProductionCost;
            oldProduct.SalePrice = newProduct.SalePrice;
      
            unitOfWork.Save();
        }

        private bool ProductNameExists(Product oldProduct, Product newProduct)
        {
            return !oldProduct.Equals(newProduct) && ProductIsRegistered(newProduct);
        }

        public void UnregisterProduct(Product product)
        {
            if (!ProductIsRegistered(product))
                throw new ProductException("No hay producto dado de alta con" +
                   " el nombre: " + product.Name);

            if (ProductIsInOrder(product))
                throw new ProductException("No se puede dar de baja el producto" + 
                   " porque se encuentra asociado a un pedido.");

            unitOfWork.ProductRepository.DeleteProduct(product);
            unitOfWork.Save();
        }

        public bool ProductIsRegistered(Product product)
        {
            return unitOfWork.ProductRepository.GetAllProducts()
                .Any(p => p.Name.Equals(product.Name));
        }

        public IEnumerable<Product> GetAllRegisteredProducts()
        {
            return unitOfWork.ProductRepository.GetAllProducts();
        }

        private bool ProductIsInOrder(Product product)
        {
            foreach (Order o in unitOfWork.OrderRepository.GetAllOrders())
                if (o.Product.Equals(product))
                    return true;

                return false;
        }       
    }
}
