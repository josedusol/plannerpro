﻿using PlannerPro.Data.DataImp;
using PlannerPro.Entities.API;
using PlannerPro.Entities.EntitiesImp;
using PlannerPro.Entities.Exceptions;
using PlannerPro.Logic.API;
using System.Collections.Generic;
using System.Linq;

namespace PlannerPro.Logic.LogicImp
{
    public class PlanLogic : IPlanLogic
    {
       private static PlanLogic planLogicInstance;
       private PlanningCriteria defaultPlanningCriteria;
       private UnitOfWork unitOfWork;

       public static PlanLogic GetInstance()
        {
            if (planLogicInstance == null)
                planLogicInstance = new PlanLogic();

            return planLogicInstance;
        }

        private PlanLogic()
        {
            unitOfWork = UnitOfWork.GetInstance();
            defaultPlanningCriteria = new DeliveryDateCriteria();          
        }

        public PlanningCriteria DefaultPlanningCriteria
        {
            get { return defaultPlanningCriteria; }
            set { defaultPlanningCriteria = value; }          
        }

        public void RunPlan(Plan plan)
        {
            if (ExistsNewOrders())
                throw new PlanException("No hay pedidos nuevos para poder generar un plan de producción.");
          
            plan.PendentOrders = GetNewOrders(unitOfWork.OrderRepository.GetAllOrders());
            plan.DoPlanning();
            unitOfWork.PlanRepository.CreatePlan(plan);
            unitOfWork.Save();    
        }

        private bool ExistsNewOrders()
        {
            return GetNewOrders(unitOfWork.OrderRepository.GetAllOrders()).Count == 0;
        }

        private IList<Order> GetNewOrders(IEnumerable<Order> orders)
        {
            return orders.Where(order => order.Status == OrderStatus.New).ToList();
        }

        public void CancelPlan(Plan plan)
        {
            if (!PlanIsRunning(plan))
                throw new PlanException("El plan no esta en ejecución.");

            plan.Cancel();
            unitOfWork.Save();
        }

        private bool PlanIsRunning(Plan plan)
        {
            return (plan.Status == PlanStatus.Runnning ? true : false);
        }

        public IEnumerable<Plan> GetAllPlans()
        {
            return unitOfWork.PlanRepository.GetAllPlans();
        }

        public void FinalizeNextOrder(Plan plan)
        {
            if (!PlanIsRunning(plan))
                throw new PlanException("El plan no esta en ejecución.");

            plan.FinalizeNextOrder();
            unitOfWork.Save();
        }
    }
}