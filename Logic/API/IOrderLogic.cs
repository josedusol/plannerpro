﻿using PlannerPro.Entities.API;
using System.Collections.Generic;

namespace PlannerPro.Logic.API
{
    public interface IOrderLogic
    {
        void RegisterOrder(Order order);
        void UpdateOrder(Order oldOrder, Order newOrder);
        void UnregisterOrder(Order order);
        bool OrderIsRegistered(Order order);
        IEnumerable<Order> GetAllRegisteredOrders();       
    }
}
