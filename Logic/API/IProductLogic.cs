﻿using PlannerPro.Entities.API;
using System.Collections.Generic;

namespace PlannerPro.Logic.API
{
    public interface IProductLogic
    {
        void RegisterProduct(Product newProduct);
        void UpdateProduct(Product oldProduct, Product newProduct);
        void UnregisterProduct(Product product);
        bool ProductIsRegistered(Product product);
        IEnumerable<Product> GetAllRegisteredProducts();
    }
}
