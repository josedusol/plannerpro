﻿using PlannerPro.Entities.API;
using System.Collections.Generic;

namespace PlannerPro.Logic.API
{
    public interface IPlanLogic
    {
        PlanningCriteria DefaultPlanningCriteria { get; set; }
        void RunPlan(Plan plan);
        void CancelPlan(Plan plan);
        IEnumerable<Plan> GetAllPlans();
        void FinalizeNextOrder(Plan plan);
    }
}
