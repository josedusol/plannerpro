﻿using PlannerPro.Entities.API;
using System.Collections.Generic;

namespace PlannerPro.Logic.API
{
    public interface IClientLogic
    {
        void RegisterClient(Client client);
        void UpdateClient(Client oldClient, Client newClient);
        void UnregisterClient(Client client);
        bool ClientIsRegistered(Client client);
        IEnumerable<Client> GetAllRegisteredClients();
    }
}
