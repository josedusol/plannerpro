﻿using System.Collections.Generic;

namespace PlannerPro.Entities.API
{
    public abstract class PlanningCriteria
    {
        public int PlanningCriteriaID { get; set; }
        public abstract IList<Order> Planning(IEnumerable<Order> orders);

        public override bool Equals(object obj) 
        {
            return (this.GetType() == obj.GetType());
        }
    }
}
