﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace PlannerPro.Entities.API 
{
    public abstract class Client
    {
        [Browsable(false)]
        public int ClientID { get; set; }
        public virtual string Name { get; set; }
        public virtual int Preference { get; set; }
        public static readonly Client NULL = new NullClient();

        [NotMapped]
        private class NullClient : Client
        {
            public override string Name
            {
                get { return String.Empty; }
                set { }
            }

            public override int Preference
            {
                get { return 0; }
                set { }
            }
        }
    }
}
