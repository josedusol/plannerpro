﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace PlannerPro.Entities.API 
{
    public abstract class Plan
    {
        [Browsable(false)]
        public int PlanID { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        [ForeignKey("PlanningCriteriaID")]
        public virtual PlanningCriteria PlanningCriteria { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual PlanStatus Status { get; set; }
        public virtual IList<Order> PendentOrders { get; set; }
        public virtual IList<Order> FinalizedOrders { get; set; }
        public abstract void DoPlanning();
        public abstract void FinalizeNextOrder();
        public abstract void Cancel();
        public static readonly Plan NULL = new NullPlan();
         
        public int PlanningCriteriaID { get; set; }

        [NotMapped]
        private class NullPlan : Plan
        {
            public override string Name
            {
                get { return String.Empty; }
                set { }
            }

            public override string Description
            {
                get { return String.Empty; }
                set { }
            }

            public override PlanningCriteria PlanningCriteria
            {
                get { return null; }
                set { }
            }

            public override DateTime StartDate
            {
                get { return DateTime.Now; }
                set { }
            }

            public override PlanStatus Status
            {
                get { return PlanStatus.Runnning; }
                set { }
            }

            public override IList<Order> PendentOrders
            {
                get { return new List<Order>(); }
                set { }
            }

            public override IList<Order> FinalizedOrders
            {
                get { return new List<Order>(); }
                set { }
            }

            public override void DoPlanning()
            {
            }

            public override void FinalizeNextOrder()
            {
            }

            public override void Cancel()
            {
            }
        }
    }
}
