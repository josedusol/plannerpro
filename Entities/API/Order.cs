﻿using PlannerPro.Entities.EntitiesImp;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace PlannerPro.Entities.API 
{
    public abstract class Order
    {
        [Browsable(false)]
        public int OrderID { get; set; }
        [ForeignKey("ProductID")]
        public virtual Product Product { get; set; }
        [ForeignKey("ClientID")]
        public virtual Client Client { get; set; }
        public virtual int Quantity { get; set; }
        public virtual DateTime DeliveryDate { get; set; }
        public virtual DateTime FinalizeDate { get; set; }
        public virtual OrderStatus Status { get; set; }
        public virtual DeliveryStatus DeliveryStatus { get; set; }
        public abstract float CalculateMarginProfit();
        public static readonly Order NULL = new NullOrder();

        public int ProductID { get; set; }
        public int ClientID { get; set; }

        [NotMapped]
        private class NullOrder : Order
        {
            public override Client Client
            {
                get { return new RealClient(String.Empty, 1); }
                set { }
            }

            public override Product Product
            {
                get { return new RealProduct(String.Empty, 1, 1, 1); }
                set { }
            }

            public override int Quantity
            {
                get { return 0; }
                set { }
            }

            public override DateTime DeliveryDate
            {
                get { return DateTime.Now; }
                set { }
            }

            public override DateTime FinalizeDate
            {
                get { return DateTime.Now; }
                set { }
            }

            public override OrderStatus Status
            {
                get { return OrderStatus.New; }
                set { }
            }

            public override DeliveryStatus DeliveryStatus
            {
                get { return DeliveryStatus.Punctual; }
                set { }
            }

            public override float CalculateMarginProfit()
            {
                return 0;
            }
        }
    }
}
