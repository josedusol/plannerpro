﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace PlannerPro.Entities.API
{
    public abstract class Product
    {
        [Browsable(false)]
        public int ProductID { get; set; }
        public virtual string Name { get; set; }
        public virtual int ManufacturingTime { get; set; }
        public virtual float SalePrice { get; set; }
        public virtual float ProductionCost { get; set; }
        public static readonly Product NULL = new NullProduct();

        [NotMapped]
        private class NullProduct : Product
        {
            public override string Name
            {
                get { return String.Empty; }
                set { }
            }

            public override int ManufacturingTime
            {
                get { return 0; }
                set { }
            }

            public override float SalePrice
            {
                get { return 0; }
                set { }
            }

            public override float ProductionCost
            {
                get { return 0; }
                set { }
            }
        }
    }
}
