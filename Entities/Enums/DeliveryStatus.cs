﻿using System.ComponentModel;

namespace PlannerPro.Entities.API
{
    public enum DeliveryStatus
    {
        [Description("A tiempo")]
        Punctual,
        [Description("Retraso")]
        Delayed,
    }
}
