﻿using System.ComponentModel;

namespace PlannerPro.Entities.API
{
    public enum PlanStatus
    {
        [Description("En Ejecución")]
        Runnning,
        [Description("Cancelado")]
        Cancelled,
        [Description("Culminado")]
        Culminated,
    }
}
