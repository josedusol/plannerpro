﻿using System.ComponentModel;

namespace PlannerPro.Entities.API
{
    public enum OrderStatus
    {
        [Description("Nuevo")]
        New,
        [Description("Pendiente")]
        Pending,
        [Description("Finalizado")]
        Finalized,
    }
}
