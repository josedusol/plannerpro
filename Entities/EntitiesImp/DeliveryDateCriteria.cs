﻿using PlannerPro.Entities.API;
using System.Collections.Generic;
using System.Linq;

namespace PlannerPro.Entities.EntitiesImp
{
    public class DeliveryDateCriteria : PlanningCriteria
    {
        public override IList<Order> Planning(IEnumerable<Order> orders)
        {
            IList<Order> planification = 
                orders.OrderBy(order => order.DeliveryDate).ToList();

            return planification;
        }

        public override string ToString()
        {
            return "Fecha de entrega";
        }
    }
}
