﻿using PlannerPro.Entities.API;
using PlannerPro.Entities.Exceptions;

namespace PlannerPro.Entities.EntitiesImp
{
    public class RealProduct : Product
    {
        private string name;
        private int manufacturingTime;
        private float productionCost;
        private float salePrice;

        public RealProduct()
        {
        }

        public RealProduct(string name)
        {
            this.Name = name;
            this.manufacturingTime = 1;
            this.salePrice = 1;
            this.productionCost = 1;
        }

        public RealProduct(string name, int manufacturingTime, float salePrice, float productionCost)
        {
            this.Name = name;
            this.ManufacturingTime = manufacturingTime;
            this.SalePrice = salePrice;
            this.ProductionCost = productionCost;
        }

        public override string Name
        {
            get { return name; }
            set {
                if (value.Length == 0)
                    throw new ProductException("El nombre del producto no puede ser vacío.");

                name = value;
            }
        }

        public override int ManufacturingTime
        {
            get { return manufacturingTime; }
            set {
                if (value < 1)
                    throw new ProductException("El tiempo de fabricación no puede ser menor a 1.");

                manufacturingTime = value;
            }
        }

        public override float SalePrice
        {
            get { return salePrice; }
            set {
                if (value < 0 )
                    throw new ProductException("El precio de venta no puede ser menor a 0.");

                salePrice = value;
            }
        }

        public override float ProductionCost
        {
            get { return productionCost; }
            set {
                if (value < 0)
                    throw new ProductException("El costo de producción no puede ser menor a 0.");

                productionCost = value;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
                return false;

            string productName = ((Product) obj).Name;

            return object.Equals(productName, Name);
        }

        public override string ToString()
        {
            return this.name;
        }
    }
}
