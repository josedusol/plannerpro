﻿using PlannerPro.Entities.API;
using PlannerPro.Entities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PlannerPro.Entities.EntitiesImp
{
    public class RealPlan : Plan
    {
        private string name;
        private string description;
        private PlanningCriteria planningCriteria;
        private DateTime startDate;
        private PlanStatus status;
        private IList<Order> pendentOrders;
        private IList<Order> finalizedOrders;

        public RealPlan() {
        }

        public RealPlan(string name)
        {
            Name = name;
            Description = "";
            PlanningCriteria = new DeliveryDateCriteria();
            StartDate = DateTime.Now;
            Status = PlanStatus.Runnning;
            pendentOrders = new List<Order>();
            finalizedOrders = new List<Order>();
        }

        public RealPlan(string name, string description, PlanningCriteria planningCriteria)
        {
            Name = name;
            Description = description;
            PlanningCriteria = planningCriteria;
            StartDate = DateTime.Now;
            Status = PlanStatus.Runnning;
            pendentOrders = new List<Order>();
            finalizedOrders = new List<Order>();
        }

        public override string Name
        {
            get { return name; }
            set {
                if (value.Length == 0)
                    throw new PlanException("El nombre del plan no puede ser vacío.");

                name = value;
            }
        }

        public override string Description
        {
            get { return description; }
            set { description = value; }
        }

        public override PlanningCriteria PlanningCriteria
        {
            get { return planningCriteria; }
            set { planningCriteria = value; }
        }

        public override DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }

        public override PlanStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        public override IList<Order> PendentOrders
        {
            get { return pendentOrders; }
            set { pendentOrders = value; }
        }

        public override void DoPlanning() 
        {
            pendentOrders = planningCriteria.Planning(pendentOrders);
            if (pendentOrders.Count == 0)
                throw new PlanException("No hay pedidos que statisfagan el criterio de producción establecido.");

            MarkOrdersAsPendent();
            CalculateDelays();
        }

        private void MarkOrdersAsPendent()
        {
            foreach (Order o in pendentOrders)
                o.Status = OrderStatus.Pending;
        }

        private void CalculateDelays()
        {
            DateTime startDate = DateTime.Now;

            foreach (Order o in pendentOrders) {
                startDate = startDate.AddMinutes(o.Product.ManufacturingTime * o.Quantity);
                if (startDate > o.DeliveryDate)
                    o.DeliveryStatus = DeliveryStatus.Delayed;
            }
        }

        public override IList<Order> FinalizedOrders
        {
            get { return finalizedOrders; }
            set { finalizedOrders = value; }
        }

        public override void FinalizeNextOrder()
        {
            if (Status == PlanStatus.Runnning) {
                Order firstOrder = pendentOrders.First();
                firstOrder.FinalizeDate = DateTime.Now;
                firstOrder.Status = OrderStatus.Finalized;
                pendentOrders.RemoveAt(0);
                finalizedOrders.Add(firstOrder);

                if (pendentOrders.Count == 0)
                    Status = PlanStatus.Culminated;
            }
        }

        public override void Cancel()
        {
            Status = PlanStatus.Cancelled;
            MarkPendentOrdersAsNew(pendentOrders);
            pendentOrders.Clear();
        }

        private void MarkPendentOrdersAsNew(IEnumerable<Order> orders)
        {
            foreach (Order o in orders) {
                o.Status = OrderStatus.New;
                o.DeliveryStatus = DeliveryStatus.Punctual;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
                return false;

            string planName = ((Plan)obj).Name;

            return object.Equals(planName, Name);
        }

        public override string ToString()
        {
            return this.name;
        }
    }
}
