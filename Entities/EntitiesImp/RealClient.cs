﻿using PlannerPro.Entities.API;
using PlannerPro.Entities.Exceptions;

namespace PlannerPro.Entities.EntitiesImp
{
    public class RealClient : Client
    {
        private string name;
        private int preference;

        public RealClient()
        {
        }

        public RealClient(string name)
        {
            this.Name = name;
            this.Preference = 1;
        }

        public RealClient(string name, int preference) 
        {
            this.Name = name;
            this.Preference = preference;
        }
        
        public override string Name {
            get { return name; }
            set {            
                if (value.Length == 0)
                    throw new ClientException("El nombre del cliente no puede ser vacío.");

                name = value;
            }
        }

        public override int Preference {
            get { return preference; }
            set {
                if (value < 1 || value > 10)
                    throw new ClientException("La preferencia debe ser un valor entero entre 1 y 10.");

                preference = value;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
                return false;

            Client client = ((Client) obj);

            return this.Name.Equals(client.Name) 
                && this.Preference == client.Preference;
        }

        public override string ToString()
        {
            return this.name;
        }
    }
}
