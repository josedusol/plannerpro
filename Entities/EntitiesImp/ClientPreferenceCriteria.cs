﻿using PlannerPro.Entities.API;
using System.Collections.Generic;
using System.Linq;

namespace PlannerPro.Entities.EntitiesImp
{
    public class ClientPreferenceCriteria : PlanningCriteria
    {
        public override IList<Order> Planning(IEnumerable<Order> orders)
        {
            IList<Order> planification = 
                orders.OrderByDescending(order => order.Client.Preference).ToList();

            return planification;
        }

        public override string ToString()
        {
            return "Preferencia de cliente";
        }
    }
}
