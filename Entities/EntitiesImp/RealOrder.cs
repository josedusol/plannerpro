﻿using PlannerPro.Entities.API;
using PlannerPro.Entities.Exceptions;
using System;

namespace PlannerPro.Entities.EntitiesImp
{
    public class RealOrder : Order
    {
        private Client client;
        private Product product;
        private int quantity;
        private DateTime deliveryDate;
        private DateTime finishDate;
        private OrderStatus status;
        private DeliveryStatus deliveryStatus;

        public RealOrder() { }

        public RealOrder(Client client, Product product, int quantity, DateTime deliveryDate) 
        {
            this.Client = client;
            this.Product = product;
            this.Quantity = quantity;
            this.DeliveryDate = deliveryDate;
            this.finishDate = DateTime.MaxValue;
            this.Status = OrderStatus.New;
            this.DeliveryStatus = DeliveryStatus.Punctual;
            this.FinalizeDate = DateTime.MaxValue;
        }

        public override Client Client
        {
            get { return client; }
            set {
               if (value == Client.NULL)
                    throw new OrderException("El cliente no puede ser nulo.");

                client = value;
            }
        }

        public override Product Product
        {
            get { return product; }
            set {
                if (value == Product.NULL)
                    throw new OrderException("El producto no puede ser nulo.");

                product = value;
            }
        }

        public override int Quantity
        {
            get { return quantity; }
            set {
                if (value < 1)
                    throw new OrderException("La cantidad a producir no puede ser menor a 1.");

                quantity = value;
            }
        }

        public override DateTime DeliveryDate
        {
            get { return deliveryDate; }
            set {
                if (value <= DateTime.Now)
                    throw new OrderException("La fecha de entrega deber ser mayor a la fecha actual.");

                deliveryDate = value;
            }
        }

        public override DateTime FinalizeDate
        {
            get { return finishDate; }
            set { finishDate = value; }
        }

        public override OrderStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        public override DeliveryStatus DeliveryStatus
        {
            get { return deliveryStatus; }
            set { deliveryStatus = value; }
        }

        public override float CalculateMarginProfit()
        {
            if (product == null)
                return 0;

            return (product.SalePrice - product.ProductionCost) * quantity;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            Order otherOrder = ((Order)obj);

            return this.client.Equals(otherOrder.Client) &&
                   this.product.Equals(otherOrder.Product) &&
                   this.quantity == otherOrder.Quantity &&
                   otherOrder.DeliveryDate.Date == this.deliveryDate.Date;
        }
    }
}
