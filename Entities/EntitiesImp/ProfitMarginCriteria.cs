﻿using PlannerPro.Entities.API;
using System.Collections.Generic;
using System.Linq;

namespace PlannerPro.Entities.EntitiesImp
{
    public class ProfitMarginCriteria : PlanningCriteria
    {
        public override IList<Order> Planning(IEnumerable<Order> orders)
        {
            IList<Order> planification = 
                orders.OrderByDescending(order => order.CalculateMarginProfit()).ToList();

            return planification;
        }

        public override string ToString()
        {
            return "Margen de ganancia";
        }
    }
}
