﻿using PlannerPro.Entities.API;
using System.Collections.Generic;
using System.Linq;

namespace PlannerPro.Entities.EntitiesImp
{
    public class FilteredDeliveryDateCriteria : PlanningCriteria
    {
        public override IList<Order> Planning(IEnumerable<Order> orders)
        {
            IEnumerable<Order> filteredOrders =
                orders.Where(order => order.Quantity > 1000 && order.Client.Preference >= 5);

            IList<Order> planification = 
                filteredOrders.OrderBy(order => order.DeliveryDate).ToList();

            return planification;
        }

        public override string ToString()
        {
            return "Fecha de entrega con filtro";
        }
    }
}
