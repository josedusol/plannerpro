﻿using System;

namespace PlannerPro.Entities.Exceptions
{
    [SerializableAttribute] 
    public class ProductException : Exception
    {
        public ProductException()
            : base()
        {
        }

        public ProductException(String message)
            : base(message)
        {
        }

        public ProductException(String message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
