﻿using System;

namespace PlannerPro.Entities.Exceptions
{
    [SerializableAttribute] 
    public class ClientException : Exception
    {
        public ClientException()
            : base()
        {
        }

        public ClientException(String message)
            : base(message)
        {
        }

        public ClientException(String message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
