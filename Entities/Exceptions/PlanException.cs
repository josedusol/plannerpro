﻿using System;

namespace PlannerPro.Entities.Exceptions
{
    [SerializableAttribute] 
    public class PlanException : Exception
    {
        public PlanException()
            : base()
        {
        }

        public PlanException(String message)
            : base(message)
        {
        }

        public PlanException(String message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
