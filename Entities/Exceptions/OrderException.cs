﻿using System;

namespace PlannerPro.Entities.Exceptions
{
    [SerializableAttribute] 
    public class OrderException : Exception
    {
        public OrderException()
            : base()
        {
        }

        public OrderException(String message)
            : base(message)
        {
        }

        public OrderException(String message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
